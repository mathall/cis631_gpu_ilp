#!/bin/bash

#
# Runs different *_revised_simplex implementations against test problems
#

IMPLS=(
#	"serial"
#	"omp"
	"cuda"
	"cublas"
)

PROB_DIR="./data"
PROBS=(
# 	"ADLITTLE.SIF"
#	"AFIRO.SIF"
#	"BLEND.SIF"
#	"LOTFI.SIF"
#	"SC105.SIF"
#	"SC205.SIF"
#	"SC50A.SIF"
#	"SC50B.SIF"
#	"SCAGR7.SIF"
#	"SHARE2B.SIF"
	"SHIP04S.SIF"
	"SHIP04L.SIF"
)	

NUM_RUNS=3

OUT_FILE=results_`date +%Y%m%d%H%M%S`.out

echo "\"problem\" \"backend\" \"time\" \"optimal\"" > $OUT_FILE

for prob in ${PROBS[@]}
do
	for imp in ${IMPLS[@]}
	do
		echo "Running ${imp} on ${prob}. . ."
		for i in `seq 1 ${NUM_RUNS}`
		do
			echo "  round $i"
			RES=`./${imp}_revised_simplex ${PROB_DIR}/${prob}`
			TIME=`echo "$RES" | grep time: | sed -E "s/.* time: ([0-9\.]+)/\1/"`
			OPT=`echo "$RES" | grep Optimal | sed -E "s/.* objective value is (\-?[0-9\.]+)/\1/"`
			if [ -z "$TIME" ]
			then
				TIME=0
			fi
			if [ -z "$OPT" ]
			then
				OPT=0
			fi
			echo ${prob/%.SIF/} ${imp} ${TIME} ${OPT} >> $OUT_FILE
		done
	done
done

echo Done.
