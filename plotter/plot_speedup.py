import numpy as np
from sys import argv
from box_plot import boxplot

def main(argv):    
    files = []
    for arg in argv[1:]:
        files.append(arg)
    
    print(files)
    data = {} # test_type: metrics
    data["serial"] = {} # metric: value
    data["openMP"] = {}
    data["cuda"] = {}
    data["cublas"] = {}

    for f in files:
        if "cuda" in f:
            test_type = 'cuda'
        elif "openMP" in f:
            test_type = "openMP"
        elif "serial" in f:
            test_type = "serial"
        elif "cublas" in f:
            test_type = "cublas"
        else:
            print('unknown test given by file: {}'.format(f))
            exit(-1)

        with open(f, 'r') as fob:
            line = fob.readline()
            while line:
                line = line.strip().strip(',')
                if ":" in line:
                    metric = line.split(":")[0]
                    data[test_type][metric] = []
                    # print("metric: {}".format(metric))
                elif ", " in line:
                    data_list = line.split(",")
                    for i in range(len(data_list)):
                        try:
                            val = float(data_list[i].strip())
                        except:
                            print("error reading metric {}, line: \n\t{}\n\tCannot handle this string:'{}'".format(metric, line, data_list[i]))
                            exit()
                        data[test_type][metric].append(val)
                    # print("value: {}".format(line))
                line = fob.readline()

    for test_type in data:
        for metric in data[test_type]:
            data[test_type][metric] = np.array(np.percentile(data[test_type][metric], [0,25,50,75,100]))

    speedup = {} # metric -> x[]/serial[]
    
    for metric in data["serial"]:
        serial = data["serial"][metric]
        speedup[metric] = {}
        if data["openMP"]:
            openMP = data["openMP"][metric]
            omp_speedup = serial / openMP
            speedup[metric]["openMP"] = omp_speedup

        if data["cuda"]:
            cuda = data["cuda"][metric]
            cuda_speedup = serial / cuda
            speedup[metric]["cuda"] = cuda_speedup
        
        if data["cublas"]:
            cuda = data["cublas"][metric]
            cuda_speedup = serial / cuda
            speedup[metric]["cublas"] = cuda_speedup

    # print(speedup['matrix_inverse']["openMP"])        
    # print(speedup.keys())    
    omp = {}
    for metric in speedup:
        omp[metric] = speedup[metric]["cuda"]
        boxplot(omp, "cuda_500")

    # print(speedup['matrix_inverse']["openMP"])        
    # print(speedup.keys())    
    omp = {}
    for metric in speedup:
        omp[metric] = speedup[metric]["cublas"]
        boxplot(omp, "cublas_500")

    # print(speedup['matrix_inverse']["openMP"])        
    # print(speedup.keys())    
    omp = {}
    for metric in speedup:
        omp[metric] = speedup[metric]["openMP"]
        boxplot(omp, "openMP_500")

    # print(omp)

main(argv)