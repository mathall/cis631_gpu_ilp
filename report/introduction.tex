Linear Programming (LP) is a widely used tool for solving optimization problems. Some common use cases include scheduling, supply chain management, production planning, network flow, and operations research in general. It is commonly applied in different research domains, including networked systems. Originally developed in the 1800s, it has blossomed with the development of computing, as faster computers, larger memory capacity, and multi-processing have enabled LP to be applied to larger and larger problems. For instance, Google and Microsoft, both rely on linear programming as the core component of their traffic engineering systems, which continuously serve content to billions of people daily~\cite{hong2013achieving, jain2013b4}.

The simplex method for linear programming is one of the most commonly deployed optimization algorithms. Although it has worst-case exponential time complexity (for the number of constraints), simplex often finds an optimal solution on polynomial time~\cite{spielman2004smoothed}. Although polynomial-time algorithms are more attractive than exponential, they can still prove to be intractable for large problems.

Parallel to the growth of problem size and complexity, computer architectures have advanced as well.
Multi-core processors are now ubiquitous in commodity and high-performance computers. Graphics processors, with thousands of cores, are pervasive as well. The paradigm shift towards many-core architectures compels developers to rewrite old single-core algorithms for these new multi-core systems if they expect their programs to speed up year after year and to solve larger and more complex problems.

Given that linear programs, although powerful, have their limits of computational tractability, and that more focus must be given to the parallel programming paradigm to make the best use of the computational resources available today, we aim to develop parallel programming implementations of the revised simplex method, thereby solving optimization problems more efficiently on modern computing systems.

In particular, we explore two avenues of parallelization: multi-core, and GPU. We relate our experience with writing these algorithms in this paper and highlight the performance boosts that we observed in the single-core (serial) version of the algorithm as well as the multi-core (OpenMP) and GPU (CUDA) implementations. We also compare our CUDA implementation with a high-performance library, \emph{cuBLAS}. 
The key result we present is that {\em problem size has a critical effect on the ability of parallelization to achieve better performance}, regardless of the chosen avenue of parallelization.
In general, we note that problems with hundreds of variables and constraints are able to benefit from parallel implementations, while smaller problems are more efficient to solve using serial code.

The remainder of this report is organized as follows:
In \S \ref{sec:background} we discuss the simplex and revised simplex algorithms; in \S~\ref{sec:implementation} we discuss our implementation of those algorithms and our generic backend interface. \S~\ref{sec:results} details the performance results of our OpenMP and CUDA backends on microbenchmarks and selected Netlib problems. 
\S~\ref{sec:related} touches on related work. Finally, we give concluding remarks in \S~\ref{sec:conclusion}.

\noindent
{\bf Aside:}
In our original project proposal, we planned to develop parallel implementations of an Integer Programming optimization solver.
Through deeper study, we realized that LP optimization solvers were required at the core of most common algorithms for the IP problem.
Due to time constraints, we decided to focus the current effort instead on developing an LP implementation.
Our current implementation is highly flexible and performant on large problems and lays the groundwork for future IP and Mixed-IP optimization.