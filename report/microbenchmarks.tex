As noted in section~\ref{sec:implementation}, we chose to implement the revised simplex method in such a way that it can use an arbitrary set of implementations, serial or parallel. Thus, our parallel implementations of the algorithm are made by parallelizing the respective operations required by revised simplex. 
In this section, we present the performance of these operations in isolation on a custom-built benchmarking suit. 
Our benchmark suit creates an arbitrary set of input data for the set of operations, and accepts several parameters including the number of iterations in which to run each operation and the input data set size. 

\noindent
{\bf Operations:} The operations that we target for our microbenchmarks can be divided into memory management routines and compute routines. The set of operations for memory management is: 
\begin{verbatim}
matrix_get_col()
matrix_set_col()
vec_get()
vec_set()
matrix_extract_vec()
vec_extract_vec()
\end{verbatim}{}
Of these {\tt matrix\_extract\_vec()} returns a new matrix formed with the columns of the old matrix indicated by the given vector.
The {\tt vec\_extract\_vec()} operation has a similar semantic on vectors.
These operations are required in both phases of the revised simplex method to extract the current basis matrix $B = A_{\BS}$, the residual matrix $A_{\RS}$, and the entries of the cost function $c_{\BS}$.
Note that while we implement more getters and setters as part of our abstract interface, they are not called from the main revised simplex loop and we, therefore, do not judge their performance to be directly relevant.

The compute routines are:

\begin{verbatim}
vec_min()
vec_subtract()
vec_dot_prod()
vec_matrix_mult()
scalar_vec_mult()
matrix_matrix_mult()
matrix_inverse()
matrix_vec_mult()
matrix_colsum_ltz()
phase1_min_ratio()
phase2_min_ratio()
\end{verbatim}{}
As discussed in \S \ref{ssec:implmotivation}, the last three of these compute routines are combined operations defined specifically to satisfy different steps of the revised simplex algorithm.
The other compute routines are common linear algebra operations as the names suggest.

\noindent
{\bf Architecture:} We collect the following results while running our code on Talapas, the University of Oregon's supercomputer\footnote{\url{https://hpcf.uoregon.edu/content/talapas}}. The operating system is Red Hat Enterprise Linux, version 7.6. We use one node on the {\tt gpu} partition, with a dual-socket Intel(R) Xeon(R) CPU E5-2690 v4 with with 14 cores per socket, for a total of 28 independent threads running at 2.60~GHz each. The GPU is a Tesla K80, with 24~GB of memory. The programs serial and OpenMP programs are written in C, and the CUDA kernels are written in CUDA/C++. We use the gcc/g++ 7.3.0 and NVCC 9.2 for CUDA/C++. 

\noindent
{\bf Evaluation Parameters:} For these microbenchmarks, we evaluate our implementations with randomly generated n-vectors and n-square matrices, where n = 500. The values in each cell are double precision floating points between -100 and 100. For each benchmark, we run 10 iterations, where new sets of random numbers are used in each iteration. The number of iterations is limited due to the time required by the serial baseline, which can extend upwards of a few minutes for operations such as matrix inverse. Although the number of iterations is low, the variance in time for each set of benchmarks is also low, as seen by the box an whisker plots.

\begin{figure*}
    \centering
    \includegraphics[width=0.8\textwidth]{openMP_500.png}
    \caption{OpenMP Operation Speedup vs Serial, $log_{10}$ scale}
    \label{fig:micoMP}
\end{figure*}

\noindent
{\bf OpenMP:} Figure~\ref{fig:micoMP} shows the speedup for the test set of operations between our OpenMP implementation and the serial baseline. We observe the best performance boost for the matrix inverse operation, where the average serial time is 5.5~seconds and the average parallel time is 0.3~seconds, approximately 18~$\times$ faster. Other operations, such as \texttt{matrix\_extract\_vec} and \texttt{matrix\_vec\_mult} have more modest performance benefits, below 10~$\times$. Some operations, including \texttt{col\_sum\_ltz}, and \texttt{phase1/phase2\_min\_ratio} have no performance change because they are not parallelized. We attempted to parallelize these functions, however, the operations required custom reduction operations whose implementation was still giving race conditions at the time of submission. Some operations, such as \texttt{matrix\_get\_col} appear to have a wider variance because the absolute time for these operations was on the order of $10^{-6}$~s to $10^{-5}$~s.
%with the parallel \texttt{get\_col} operations being out performed by the serialized version.

\begin{figure*}
    \centering
    \includegraphics[width=0.8\textwidth]{cuda_500.png}
    \caption{CUDA Operation Speedup vs Serial. $log_{10}$ scale}
    \label{fig:micoCuda}
\end{figure*}

\noindent
{\bf Cuda:} Figure~\ref{fig:micoCuda} shows the performance increase for the CUDA implementation of our microbenchmarks. As we can see, we have greatly increased performance with respect to the serial code, with operations such as \texttt{matrix\_set\_col}, \texttt{matrix\_inverse}, \texttt{vec\_dot\_prod}, \texttt{matrix\_vec\_mult} and \texttt{matrix\_matrix\_mult} all performing 10 to 100~$\times$ faster than the serial version. Other random-access memory operations are drastically slower with CUDA. For example, \texttt{vec\_get} and \texttt{vec\_set}, which read or write a randomly chosen element in an array, are more than 100~$\times$ slower against the serial code. Other Vector operations are also slower with respect to the serial operations such as \texttt{scalar\_vec\_mult}, and \texttt{vec\_dot\_prod}. This is likely because the size of the arrays, 500 elements, is not large enough to show the performance boost in the GPU.

\begin{figure*}
    \centering
    \includegraphics[width=0.8\textwidth]{cublas_500.png}
    \caption{CuBLAS Operation Speedup vs Serial, $log_{10}$ scale}
    \label{fig:micoCuBlas}
\end{figure*}

\noindent
{\bf cuBLAS: }
Our CUDA implementations are admittedly not highly optimized and likely have room for improvement.
In order to gauge our code's potential for improvement, we investigate how our code compares with a highly-optimized state-of-the-art linear-programming library, cuBLAS~\cite{cuBLAS}. Figure~\ref{fig:micoCuBlas} shows the results using the same microbenchmarks considered in this section. Here, we see that performance gains are pushed even further with cuBLAS, upwards of 100 times faster. 
Note that, because cuBLAS only supports the common BLAS linear algebra operations, some methods in the cuBLAS backend, such as \texttt{phase1/phase2\_min\_ratio\_test}, fall back on our CUDA kernels.
Thus the performance of these is, as expected, the same as our CUDA implementation.
Surprisingly enough, the vector operations all took a performance hit from the cuBLAS operations as well. However, this is again, likely due to the relatively small size of the vectors. 
We leave extending these microbenchmarks to a comparison between parallel implementations over larger problems sizes for future work.