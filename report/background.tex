An LP optimization problem consists of three main parts: (1) the objective functions, expressed as a linear combination of a set of variables; (2) the optimization sense of the objective function, expressed as minimize or maximize; and (3) a set of constraints on the variables.
Often, LP problems arise in practice with tens to hundreds of variables and complex, heterogeneous sets of constraints.
For example, Gardner et al.~\cite{lotfi1990} cast an audit staff planning problem as an LP with 153 constraints, including 95 equalities, 16 less-thans, and 42 greater-thans, on 308 variables.
To simplify the task of specifying LPs, the common MPS format~\cite{mps1, mps2} allows the specification of equalities and inequalities over the variables, as well as bounds on each individual variable and ranges in which to apply the given equalities and/or inequalities.
LP solvers can easily convert such heterogeneous inputs into the less relaxed forms required by LP optimization algorithms.

To simplify the mathematics, most LP optimization algorithms assume the input is given in a concise mathematical description called standard form. Following loosely the notation of Ralphs~\cite{ralphs2018parallel}, the standard form for expressing an LP problem is as follows:
\begin{align}
& \min_{x \in \mathcal{F}} \Trans{c} x \\
\mathrm{s.t.}\ &\mathcal{F} = \{ x \in \mathbb{R}^{n}\ |\ Ax \leq b\ ,\ x \geq 0\ \}
\end{align}
where $A\in\mathbb{R}^{m \mathrm{x} n}$ and $b\in\mathbb{R}^{m}$ describe the constraints on the variables $x_{1},\dots,x_{n}$
and $c\in\mathbb{R}^{n}$ specifies the objective function.
Clearly, the heterogeneous types of constraints found in the wild can easily be transformed into standard form by replacing equalities with two inequality constraints, negating greater-than constraints, and possibly negating the objective function.
In the last cast, the computed optimal objective value must also be negated before reporting.
A final caveat is that unbounded $x$ values must be replaced with a difference between two, new positive variables.
While these transformations are mathematically simple, they can result in a significant expansion of constraints and variables.
For example, the audit staff planning LP mentioned above actually has twice as many constraints in standard form.
Finally, transformations are required to remove redundant or pairwise linearly dependent constraints or variables as will become apparent in section \ref{ssec:revisedsimplex}.

In addition to standard form, several other key terms have developed to facilitate the discussion of LP optimization algorithms.
A setting of $x$ which satisfies $x \in \mathcal{F}$ is known as a {\em feasible} solution and a setting of $x$ which minimizes $\Trans{c}x$ is known as an {\em optimal} solution.
The goal of an LP optimization algorithm is then to find the optimal solution given the objective function and constraints.
Additionally, LP optimization algorithms must be able to identify two conditions where such an optimal solution does not exist.
First, it could be the case that the objective functions can be decreased without bound, in which case the LP is said to be {\em unbounded}.
Finally, the constraints could be given such that no feasible solution exists (\eg $x \geq 3$ and $x \leq 2$) in which case, the LP is said to be {\em infeasible}.

Most performant LP optimization solutions are built on Dantzig's original simplex method~\cite{dantzig1955generalized}.
At a high level, this method identifies that any optimal solution will fall on the edge of the $n$-dimensional polyhedron defined by $\mathcal{F}$.
The method iteratively explores the convex hull of $\mathcal{F}$, going from point to point, until the optimal solution is found.
The complexity of this algorithm is typically measured in the number of iterations taken for a given problem as a function of problem dimensionality.
While the worst-case time complexity of the simplex method is known to be exponential, for a wide range of practical optimization problems, it executes in near-linear time~\cite{shamir1987survey}.
Probabilistic analysis~\cite{smale1983average,borgwardt2012simplex} demonstrates the average case time complexity is, in fact, linear.
Several other theoretic results support the simplex method's utility when the dimension is fixed~\cite{megiddo1984linear,dyer1989randomized}.

Another key feature of Dantzig's method is its ready interpretation in terms of common linear algebra operations, often known as the {\em revised simplex method}.
The textbook formulation of the original simplex method is given in terms of updates to constraints and the objective function.
In each iteration, these equations are rearranged such that particular {\em basic} variables are isolated in terms of the other {\em nonbasic} variables.
This naive form requires updating each coefficient (each entry in $A$) in each iteration which leads to slow memory-bound implementations in practice.
The key insight behind the revised simplex method is that the coefficients of the basic variables at any iteration form a basis for the right-hand-side of the constraints (\ie $b$).
The iterative selection of basic variables, then, can be made by simply replacing columns of this basis and computing its inverse. These operations are much more efficient on modern computer architectures due to their higher arithmetic intensity.
We use the word simplex to refer to the revised simplex method, and not Dantzig's original algorithm in the remainder of this paper.

\subsection{The Revised Simplex Method}
\label{ssec:revisedsimplex}

To execute the revised simplex method, the LP must first be converted from standard form to slack form.
This essentially converts all the inequalities to equalities by adding non-negative ``slack'' variables $s_{i}$ such that
$$
\sum_{j=1}^{n}a_{ij}x_{j} + s_{i} = b_{i}\ ,\ \forall i\in \{1,\dots,m\}.
$$
For simplicity of notation, the following discussion assumes the LP has already been converted into slack form by replacing $A$ with $AI$, where $I$ is the $m$-element identity matrix, and $n$ with $m +n$.
The feasible region $\mathcal{F}$ then also becomes $\{ x \in \mathbb{R}^{n}\ |\ Ax = b\ ,\ x \geq 0\ \}$.
Since $n > m$ and we assume the columns of $A$ are pairwise linear independent, this system of equalities clearly has infinite solutions.

As in the original simplex method, the revised simplex method considers a sequence of solutions to this system of equalities, moving from less optimal to more optimal solutions based on a set of heuristics.
To limit the infinite solution space, the revised simplex method only considers {\em basic} solutions.
A basic solution is formed by extracting a basis from the columns of $A$ and setting the variables that correspond to the other, nonbasic columns to zero.
The system can then be solved for the values of the basic variables by inversion and the value of the objective function can be determined.
These solutions correspond to points of the $n$-dimensional polyhedron considered in the geometric interpretation of the simplex method.

Due to the nonnegativity constraint on the $x_{i}$s and the fact than the $b_{i}$s may still be negative, {\em basic} solutions do not always correspond to {\em feasible} solutions.
Therefore, implementations of the simplex method must proceed in two phases: first to find a {\em basic feasible} solution, then to refine the basic feasible solution into an {\em optimal} solution.
The mechanism traditionally used to accomplish this is to introduce $m$ new nonnegative {\em artificial} variables with negative coefficients in the constraint matrix $A$ (\eg augment $A$ with $-I$).
The {\em phase one} objective function then minimizes the sum of these artificial variables.
If the sum can be driven to 0, then a basic feasible solution has been found. Otherwise, the LP is determined to be infeasible.
While the mathematical correctness of this technique follows easily, the introduction of new variables again increases the problem complexity.

\noindent
{\bf Phase One Algorithm:}
The current work leverages more recent theoretical developments which provide an {\em artificial-free} method for finding the {\em basic feasible} solution~\cite{khan2013new}.
As the name suggests, this method does not require any additional variables beyond the slack form.
The key insight is that only the rows of $A$ where the corresponding entry in $b$ are negative need to be considered in the phase one objective function.
The same semantics around artificial variables apply to these constraints: if they can all be driven to zero, a {\em basic feasible} solution has been found, otherwise, the LP is infeasible.

Algorithm~\ref{alg:revisedsimplex1} shows pseudocode for the phase-one simplex method. We adopt the convention that $A_{\mathcal{S}}$ represents the columns of matrix $A$ specified by the index set $S$ and $c_{\mathcal{S}}$ represents the entries of vector $c$ specified by the index set $S$.
In this notation, $\beta$ represents the basic solution gathered by setting all nonbasic variables to 0 and the columns of $\alpha$ hold the coefficients of the nonbasic variables with respect to the current choice of basis $B$, sometimes referred to as the tableau of $A$.
The key steps are in lines \ref{rs1:updateq} and \ref{rs1:updatep}.
In line \ref{rs1:updateq}, the nonbasic variable with maximal impact on the phase one objective function is chosen to be pivoted into the basis only taking into account the constraints that violate solution feasibility (where $b_{i} < 0$).
Next, in line \ref{rs1:updatep}, a column is chosen to be removed from the basis using a variation of the classic minimum ratio test.
In particular, columns are only removed when the basic solution is zero if the corresponding tableau entry is positive.

\begin{algorithm}
\caption{Revised Simplex Method Phase 1}\label{alg:revisedsimplex1}
\begin{algorithmic}[1]
\Require $A, b, m, n$
\State $A \gets [A I]$ \Comment{Augment $A$ with slack variables}
\State $\BS \gets \{n + 1,\dots,n+m+1\}\ ,\ B \gets A_{\BS}$ \Comment{Choose initial basis}
\State $\RS \gets \{1,\dots,n\}$ \Comment{Remaining variables are nonbasic}
\State $\beta \gets B^{-1}b\ ,\ \alpha \gets B^{-1}A_{\RS}$ \label{rs1:start}
\If{all $\beta_{i} \geq 0$}
    \State {\bf continue} to phase two \Comment{Found {\em basic feasible} solution}
\EndIf
\State $w_{j} \gets \sum_{i}\alpha_{i,j} \suchthat \beta_{i} < 0\ \ \forall j \in \RS$ \label{rs1:colsum}
\If {all $w_{j} \geq 0$}
    \State {\bf return} \Comment{LP is {\em unfeasible}}
\EndIf
\State $q \gets \mathrm{argmin}\{w_{j}\}$\label{rs1:updateq}
\State $p \gets \mathrm{argmin}\{ \beta_{i}/\alpha_{i,q}\ |\ (\beta_{i}<0\ \mathrm{and}\ \alpha_{i,q}<0)\ \mathrm{or}\ (\beta_{i}\geq0\ \mathrm{and}\ \alpha_{i,q}>0)  \}$\label{rs1:updatep}
\State Pivot $q$ into $\BS$ and $p$ into $\RS$, update $B \gets A_{\BS}$
\State {\bf goto} line \ref{rs1:start}
\end{algorithmic}
\end{algorithm}

\noindent
{\bf Phase Two Algorithm:}
The second phase of the revised simplex method, as shown in algorithm \ref{alg:revisedsimplex2}, starts from a basic feasible solution and iteratively refines the value of the original objective function by pivoting basic and nonbasic variables.
Each iteration chooses a nonbasic variable to enter the basis with maximal cost improvement as computed in line \ref{rs2:costimp}.
If none of the nonbasic variables can improve the cost, the current basic solution is optimal and the algorithm terminates.
The proof of this is well known and outside the scope of this work (see, \eg chapter 29 of~\cite{cormen2009}).

Next, in line \ref{rs2:alphaq}, this phase calculates the coefficients of the selected column with respect to the current choice of basis.
These coefficients show how much increasing the $q$th variable will decrease the variables in the current basis.
If increasing the $q$th variable actually {\em increases} the current basic variables, \ie its coefficients are all nonpositive, the LP is unbounded.
Otherwise, in line \ref{rs2:theta}, the algorithm chooses the basic variable which reaches zero first (as the $q$th nonbasic variable is increased) to pivot out of the basis.
As in the phase-one algorithm, the last steps (lines \ref{rs2:pivot} and \ref{rs2:updatecost}) update the basic and nonbasic variables with a minimum number of operations.

\begin{algorithm}
\caption{Revised Simplex Method Phase 2}\label{alg:revisedsimplex2}
\begin{algorithmic}[1]
\Require $A, b, c, m, n, \BS$ \Comment{Assuming $A_{\BS}x=b$ has a solution where all $x_{i} \geq 0$}
\State $\beta \gets B^{-1}b\ ,\ z \gets \Trans{c_{\BS}}\beta\ ,\ \Trans{\pi} \gets \Trans{c_{\BS}}B^{-1}$ \label{rs2:start}
\State $w \gets c_{\RS} - \Trans{\pi}A_{\RS}$\Comment{Compute the cost improvement for nonbasic variables}\label{rs2:costimp}
\If {all $w_{j} \geq 0$}
    \State {\bf return} \Comment{No $w_{j}$ can improve the cost so the current basic solution is optimal}
\EndIf
\State $q \gets \mathrm{argmin} \{w_{j}\}$
\State $\alpha_{\cdot,q} \gets B^{-1}A_{\cdot, q}$ \Comment{Calculate the $q$th column of the tableau}\label{rs2:alphaq}
\If {all $\alpha_{i,q} \leq 0$}
    \State {\bf return} \Comment{The cost can be reduced indefinitely by increasing $x_{q}$}
\EndIf
\State $\theta \gets \mathrm{min}\{\beta_{i}/\alpha_{i,q}\ |\ \alpha_{i,q} > 0\},\ \mathrm{choose}\ p\ \suchthat \theta=\beta_{p}/\alpha_{p,q}$\label{rs2:theta}
\State Pivot $q$ into $\BS$ and p into $\RS$, update $B$ by replacing the $p$-th column with $A_{\cdot,q}$, and update $\beta_{p}\gets\theta, \beta_{i}\gets\beta_{i} - \theta\alpha_{i,q}\ \mathrm{for}\ i \neq p$\label{rs2:pivot}
\State $z \gets z - \theta w_{q}$ \Comment{Update the current objective function value}\label{rs2:updatecost}
\State {\bf goto} line \ref{rs2:start}
\end{algorithmic}
\end{algorithm}

\noindent
{\bf Parallel Implementation:}
In addition to the simplicity of update operations, the revised simplex algorithm also lends itself somewhat to parallelization.
The iterations of the algorithms must be executed in sequence with data synchronization between each iteration.
However, the individual operations that must be executed in a single iteration mirror common linear algebra functions which have well-known parallel implementations.
In particular several prior efforts explore revised simplex implementation on multi-core~\cite{alon1990parallel, luby1993parallel} and GPU~\cite{gahrouei2018effective,bieling2010efficient,spampinato2009linear} systems.
The key challenges through these efforts are in finding efficient techniques for data movement and synchronization between operations.
Additionally, the revised simplex method is highly sensitive to the numeric stability of each operation.
In particular, the reordering of operations inherent in many parallel reduction implementations (\eg for the min operations), can have a significant impact on the ability of the algorithm to terminate properly.