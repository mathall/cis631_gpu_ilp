To understand the impact of our three different backends on the performance of the revised simplex algorithm, we solve a selected set of LP problems from the Netlib problem set~\cite{netlib}.
Our selection of problems is limited by our minimal implementation of the MPS format (see \S \ref{ssec:impldetails}) and the fact that several of the Netlib problems contain tricky conditions which cause our revised simplex implementation to cycle.
Table~\ref{tab:LPs} shows our selection and the characteristics of these problems. 

\begin{table}[]
    \centering
    \begin{tabular}{|l|r|r|r|}
    \hline
    Name    &    Rows     &   Columns  &   Non-Zeros \\\hline
    ADLITTLE  &    57  &    97        &   465          \\
    AFIRO     &    28  &    32        &    88        \\
    BLEND     &    75  &    83        &   521       \\
    ISRAEL    &   175  &   142        &  2358        \\
    LOTFI     &   154  &   308        &  1086        \\
    SC105     &   106  &   103        &   281        \\
    SC205     &   206  &   203        &   552        \\
    SC50A     &    51  &    48        &   131        \\
    SHARE2B   &    97  &    79        &   730        \\
    SC50B     &    51  &    48        &   119        \\
    SCAGR7    &   130  &   140        &   553        \\
    SHIP04S   &   403  &  1458        &  5810       \\
    SHIP04L   &   403  &  2118        & 8450       \\
    \hline
    \end{tabular}
    \vspace{0.3cm}
    \caption{NETLIB Test Problems and Their Sizes.}
    \label{tab:LPs}
\end{table}

Our serial and OpenMP implementations are able to solve each of the problems listed in table~\ref{tab:LPs}. With the exception of the last two problems in the table, we run each solver on each one 30 times. In the case of SHIP04S and SHIP04L, we only run the solver 3 times, and only with CUDA and cuBLAS back-ends. This is because even our CUDA back-end took several minutes to complete these problems.
A single test of the serial backend on SHIP04S ran for over an hour before terminating.
In the following results, we report absolute time and speed up based on the time taken by all iterations of the main revised simplex loop, not accounting for the time spent setting up variables and moving data.
This decision is motivated in part by our original impetus to employ this LP optimization as part of a larger IP optimization, in which case, the data would already be in place.

\begin{figure}
    \centering
    \includegraphics[width=0.8\columnwidth]{lp_eval/orig_lp_eval_mean.pdf}
    \caption{Execution time for 10 NetLib problems}
    \label{fig:absolute_time_omp_v_cuda}
    \centering
    \includegraphics[width=0.8\columnwidth]{lp_eval/orig_lp_eval_mean_speedup.pdf}
    \caption{Speedup for 10 NetLib problems}
    \label{fig:speedup_omp_v_cuda}
\end{figure}

Figure~\ref{fig:absolute_time_omp_v_cuda} shows the absolute time taken to complete each of the linear programs for our serial, OpenMP, and CUDA backends. In this figure, we see that for small problems, the serial implementation outperforms both parallel implementations. However, as the size of the problem increases, the parallel algorithms increase in efficiency and are able to outpace the serial version, which grows dramatically in runtime. This is consistent with our prior experience using off-the-shelf LP optimization tools which may take hours to solve problems beyond a certain size.

To further illustrate the effect of problem size, figure~\ref{fig:speedup_omp_v_cuda} shows the speedup of our OpenMP and CUDA backends against the serial version. We see that the best speedup from CUDA is on SCAGR7, a problem with 130 constraints and 140 variables, where it gives a solution 3.5~$\times$ faster than the serial version. The best problem for OpenMP is SC205, with 206 constraints in 203 variables, where it performs $\sim$~2.2~$\times$ faster than serial, and surprisingly also outperforms CUDA.
These results demonstrate that, while problem size is a good indicator for the potential for parallel implementations to achieve speed up, it is not the only indicator---other factors, such as numeric precision and memory latency, also play a key role in the revised simplex method's ability to arrive quickly at the optimal solution.