In order to support diverse parallel implementations on a common algorithm, we developed an abstract interface for the required data structures and operations.
In \S \ref{ssec:implmotivation} we describe some of the particular choices made in breaking up the algorithms described in \S \ref{sec:background}
and \S \ref{ssec:impldetails} we describe technical details of our implementation including references to source files of interest.

\subsection{Motivation}
\label{ssec:implmotivation}

Many of the steps required by the revised simplex algorithm translate directly into common linear algebra operations, however, some steps involve more complex conditional and reduction logic.
Our abstract interface reflects this diversity of operations.
Rather than attempting to create a general linear algebra solution, we chose to implement only those operations required by the revised simplex algorithms.
We additionally identified several key operations that can benefit from parallel implementation but do not fit neatly into traditional linear algebra formulations.
As discussed below, these operations correspond to lines \ref{rs1:colsum} and \ref{rs1:updatep} of algorithm \ref{alg:revisedsimplex1} and line \ref{rs2:theta} of algorithm \ref{alg:revisedsimplex2}.
We also implemented a combined {\tt min} and {\tt argmin} operation which returns the minimum element and its index as required at several points in both phases.

\noindent
{\bf colsum\_ltz:} This operation, required by line \ref{rs1:colsum} of algorithm \ref{alg:revisedsimplex1}, computes the sum of the columns of the given matrix, including only the rows where an auxiliary vector (in this case $\beta$) is less than zero.
We could have implemented this operation by extracting the rows of $A$ corresponding to the nonzero entries in $\beta$, transposing the resulting matrix, and using a matrix-vector multiplication with the all-ones vector.
However, we anticipated the overheads of two extra function calls as well as the temporary storage and data movement would incur significant overhead.
Moreover, this operation lends itself well to implementation as a single parallel pass over the data as each thread can inspect the value of $\beta_{i}$ for their particular element independently before embarking on the reduction operation between threads.
For example, in the CUDA implementation, this operation is implemented as a single kernel invocation.

\noindent
{\bf phase1\_min\_ratio:} This operation, required by line \ref{rs1:updatep} of algorithm \ref{alg:revisedsimplex1}, simultaneously computes an element-wise division between two vectors and finds the minimum element of the result.
The trick is that only elements where $\beta_{i}$ and $\alpha_{i,q}$ are less than zero or elements where $\beta_{i}$ is greater than or equal to zero and $\alpha_{i,q}$ is greater than zero should be considered for the minimum.
At first glance, it may seem like all elements where the quotient is nonnegative could be considered for the minimum, in which case this could be implemented as three operations: divide, remove negative values, and compute the minimum.
However, this would wrongly include elements where $\beta_{i}$ is zero but $\alpha_{i,q}$ is {\em less than zero}.
These difficulties along with the fact that these combined operations are easily parallelizable lead us to implement \texttt{phase1\_min\_ratio} as a single operation.
In particular, threads can perform the division and contribute to the minimum reduction {\em only if} the predicate on $\alpha$ and $\beta$ is met for each particular element.
This also reduces the number of expensive division operations performed and may lead to increased performance in hyper-scalar architectures.

\noindent
{\bf phase2\_min\_ration:} This operation, required by line \ref{rs2:theta} or algorithm \ref{alg:revisedsimplex2}, is closely related to the phase one minimum ratio test described above. However, only entries where $\alpha_{i,q}$ is strictly greater than zero should be considered.
While this could be achieved by extracting the positive elements of $\alpha_{\cdot,q}$, extracting the corresponding elements of $\beta$, performing the element-wise division, and taking the minimum, we again reasoned that the overheads, especially in terms of extra copies, would merit the development of a single operation.
It might also be conceivable to implement a generic predicated divide and minimum operation which could be shared between both phase's minimum ratio tests, but we felt the overheads of designing and implementing a generic predicate expression language were too great compared with the lesser burden of maintaining two similar operations across different backends.

Another advantage of encapsulating the minimum ratio test for both phases in a single operation is that multiple variants of the artificial-free phase 1 and phase 2 revised simplex methods can be explored with minimal modification.
Typically these variants differ only in the heuristics for how to choose the entering and leaving variables which can be (partly) controlled through the minimum ratio test.
We hope to explore these different possibilities in future work.

\subsection{Implementation Details}
\label{ssec:impldetails}

As mentioned previously, our implementation strategy follows strict encapsulation of the operations and data structures required for the revised simplex method (see the specification in {\tt include/la.h}).
These operations and data structures are implemented and compiled in independently-linked object files allowing for independence between libraries.
For example, if the proprietary CUDA libraries are not installed on a particular system, the rest of the code may still be used out of the box.
The core revised simplex method is implemented in the {\tt Revised\_Simplex()} procedure (see {\tt src/revised\_simplex.c}) and makes no assumptions about the underlying data types or implementations, beyond what is defined in the specification.
We also developed simple unit tests for sanity (see {\tt src/la\_test.c}) and a more performance-oriented benchmarking system (see {\tt src/benchmark.c}) which both rely on this generic interface to apply common tests to each backend developed.

Backends are implemented in single C source files and follow the naming convention {\tt src/la\_*}, where the {\tt*} represents the name of the backend (\eg the OpenMP implementation is in {\tt src/la\_omp.c}).
Each backend implementation populates a structure of function pointers through the {\tt *\_set\_ops()} procedure.
While the use of function pointers may have some negative performance implications, we contend that the number of operations issued by the generic revised simplex code is small compared to the number of operations issues by each backend implementation, hence partially amortizing the cost of these pointers.
In our case, the flexibility to easily develop and test new implementations wins out over the marginal performance benefits of a more tightly integrated implementation structure.
For example, the CUBLAS backend was developed in several hours by simple modifications to a copy of the CUDA backend.

In addition to the core revised simplex abstraction and implementation, we also developed code to read the common MPS file format for describing LP problems~\cite{mps1,mps2} (see {\tt src/read\_mps.c}).
MPS allows for sparse representation of constraints and the objective function using a key-value based approach, but also allows for heterogeneous constraint types and several auxiliary specifications as mentioned in section \ref{sec:background}.
For simplicity, and due to time constraints, we chose to only implement the core features required to read several of the Netlib test problems~\cite{netlib} (see section \ref{sec:LP_Eval}).
In particular, we left the implementation of the {\tt RANGES} and {\tt BOUNDS} sections of MPS for future work as these sections are no more than different ways of augmenting the main constraints. Therefore they raise no new mathematical or algorithmic issues.
Our MPS implementation executes several simple sanity checks on the incoming LPs to eliminate any all-zero rows and columns, to transform equalities into two inequalities, to homogenize the directions of constraints, and to standardize the direction of the objective function. All of the aforementioned pre-processing routines proved useful in reading one or more of the Netlib problems.
