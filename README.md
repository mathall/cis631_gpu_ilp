# Parallel Linear Programming Solver

This project implements the revised simplex method with parallel backends using OpenMP and CUDA.
Our code reads the common MPS format for expressing linear programs and outputs the optimal value of the objective function, that the problem is unbounded, of that the problem is infeasible.

The main report can be found in `report/CIS631Final.pdf`.
To see progress of the objective function define the macro `DEBUG_PROGRESS` in `src/revised_simplex.c`

# Implementation

We abstract the linear algebra operations required by revised simplex using the definitions in `include/la.h` which include some classic linear algebra, as well as some particular kernels (e.g., the minimum ratio tests).
The actual revised simplex implementation is in `src/revised_simplex.c`, while the OpenMP and CUDA backends are in `src/la_omp.c` and `src/la_cuda.c` respectively.
The make system generates several test files as well as executables for each backend: `cuda_revised_simplex`, `omp_revised_simplex`, and, for reference, `serial_revised_simplex`.


# Testing

We selected problems from the netlib linear programming test problem set [http://www.numerical.rl.ac.uk/cute/netlib.html](http://www.numerical.rl.ac.uk/cute/netlib.html).
In particular, our serial implementation can solve the following problems, copies of which can be found in `data/` for convenience.

```
Name         Rows    Cols     Non-zeros           Optimal Value
ADLITTLE      57      97           465        2.2549496316E+05
AFIRO         28      32            88       -4.6475314286E+02
BLEND         75      83           521       -3.0812149846E+01
ISRAEL       175     142          2358       -8.9664482186E+05
LOTFI        154     308          1086       -2.5264706062E+01
SC105        106     103           281       -5.2202061212E+01
SC205        206     203           552       -5.2202061212E+01
SC50A         51      48           131       -6.4575077059E+01
SC50B         51      48           119       -7.0000000000E+01
SCAGR7       130     140           553       -2.3313892548E+06
SHARE2B       97      79           730       -4.1573224074E+02
```

Of these, the CUDA implementation currently fails on:

```
ISRAEL:   singular matrix
SHARE2B:  cycle
```

and the CUBLAS implementation fails on:

```
ADLITTLE: cycle
ISRAEL:   cycle
LOTFI:    cycle
SC205:    cycle
SCAGR7:   cycle
SHARE2B:  cycle
```

The CUDA implementation is able to solve the following problems onwhich the serial and omp version fail:
```
BEACONFD   (140 iterations)
SHIP04L    (816 iterations)
SHIP04S    (806 iterations)
SHIP08S    (1168 iterations, 1931.46 sec)
```

Implementing linear programming solvers is a challenge in numeric stability.
Due to the scope of these problems, it is hard to give an exact cause for failure.

# OpenMP Results

|Name      |OpenMP_Solvable(+)| OpenMP_Time (*)| OpenMP Iterations  |  Serial time (\*)| Speedup|  
|----------|----------------|----------------|----------------      |--------           |--------|       
|ADLITTLE  |       YES      |   1.129s      | 468                   |      2.144        | 1.89   |
|AFIRO     |       YES      |   1.056s      | 21                    |     1.014         | 0.96   | 
|BLEND     |No: Degenerate B|   4.345s      | 204                   |     UnInvertible  |  NA    |  
|ISRAEL    |NO: Cyclic      |   $\infty$    | $\infty$              |    Cyclic         |  NA    |  
|LOTFI     |     YES        | 53.895        | 1373                  |     48.226        | 0.89   |  
|SC105     |     YES        |   2.670s      | 107                   |     1.736         | 0.65   | 
|SC205     |     YES        |   9.913s      | 268                   |     13.721        | 1.38   | 
|SC50A     |    Yes         |    1.375s     | 49                    |     1.079        |  0.78   |  
|SC50B     |    YES         |  1.434s       | 59                    |     1.116        |  0.79   |  
|SCAGR7    |     YES        |    24.611s    | 361                   |    20.658        |  0.84   |   
|SHARE2B   |     Yes        |       4.251s  | 183                   |      2.927       |  0.69   |  

(*)64 core Intel(R) Xeon(R) Gold 5218 CPU @ 2.30GHz
(Matt ran over his Quota Capacity for Talapas at 7pm the night of the deadline, and thus evaluated the performance on Mitra, a 64 core machine of the Oregon Networking Research Group).
(+) Solvable with code base from **663d8972c8b0de1d76b862859700c7c16b832ec6**.


# Credits

While this project was highly collaborative in nature, we stuck to a general division of labor as follows.
Matt wrote the OpenMP backend, the original (non-revised) simplex implementation, and the benchmarking system.
Chris wrote the MPS-reading functions, the generic revise simplex implementation, and the CUDA backend.

