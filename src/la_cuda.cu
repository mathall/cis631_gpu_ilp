/*
 * CIS 631, Fall 2019
 *
 * Naive CUDA implementation of linear algebra operations and data structures
 *
 * Bounds checking is removed for speed in many functions, assuming the underlying algorithm has already been verified through lass_* or equiv
 *
 * Authors: Chris Misa
 */

#include <stdint.h>

#include <common.h>
#include <la_cuda.h>

#define THREADS_PER_BLOCK 128
#define ELEMS_PER_THREAD 4
#define ELEMS_PER_BLOCK (THREADS_PER_BLOCK * ELEMS_PER_THREAD)

/* Different dimensions for 2-D case. */
#define THREADS_PER_BLOCK_X 16
#define THREADS_PER_BLOCK_Y 16
#define ELEMS_PER_BLOCK_Y (THREADS_PER_BLOCK_Y * ELEMS_PER_THREAD)

/* For dumping matrices. . . */
#define MAX_COL_WIDTH 20

/* Divide and round up */
#define DIV_UP(x, y) (((x) + (y) - 1) / (y))

/* Encapsulate matrix with pointer to device memory */
typedef struct lacu_matrix_s {
    double *data;
    int m;
    int n;
} lacu_matrix_t;

/* Evaluates to a pointer to the entry in the i-th row, j-th column */
#define LACU_MATRIX_IDX(mat, i, j) ((mat)->data + (j) * (mat)->m + (i))

/* Encapsulate vector with pointer to device memory */
typedef struct lacu_vec_s {
    double *data;
    int m;
} lacu_vec_t;

/* Evaluates to a pointer to the i-th entry */
#define LACU_VEC_IDX(vec, i) ((vec)->data + (i))


#define SMALLEST_NONZERO_DEV 0.00000001
__device__ inline double
zapsmall_dev(double d)
{
    if (d < SMALLEST_NONZERO_DEV && d > -SMALLEST_NONZERO_DEV) {
        return 0.0;
    } else {
        return d;
    }
}

/* Device-side temporary buffers */
static double *dev_buff_n = NULL;
static double *dev_buff_m = NULL;
static double *dev_buff_mm = NULL;

/* Host-side temporary buffers */
static double *host_buff_n = NULL;
static double *host_buff_m = NULL;

/* Allocate temp space on device if not allocated already, returns non-zero if anything went wrong. */
int
lacu_ensure_temps(int m, int n) {

    cudaError_t err;
    int size;

    if (dev_buff_n == NULL) {
        size = (m > n ? m : n);
        err = cudaMalloc((void **)&dev_buff_n, sizeof(double) * size);
        if (err != cudaSuccess) {
            fprintf(stderr, "Failed to allocate temp buffer on device (%s)\n",
                    cudaGetErrorString(err));
            return -1;
        }
        err = cudaMalloc((void **)&dev_buff_m, sizeof(double) * size);
        if (err != cudaSuccess) {
            fprintf(stderr, "Failed to allocate temp buffer on device (%s)\n",
                    cudaGetErrorString(err));
            return -1;
        }
        err = cudaMalloc((void **)&dev_buff_mm, sizeof(double) * m * m);
        if (err != cudaSuccess) {
            fprintf(stderr, "Failed to allocate temp buffer on device (%s)\n",
                    cudaGetErrorString(err));
            return -1;
        }

        host_buff_n = (double *)malloc(sizeof(double) * size);
        host_buff_m = (double *)malloc(sizeof(double) * size);
    }
    return 0;
}

/* Allocate a new matrix with m rows and n columns. Sets all entries to 0.0 */
void *
lacu_matrix_alloc(int m, int n)
{
    cudaError_t err;
    lacu_matrix_t *mat = NULL;
    
    mat = (lacu_matrix_t *)malloc(sizeof(lacu_matrix_t));
    if (mat == NULL) {
        fprintf(stderr, "No memory for cuda matrix host-side alloc\n");
        goto error_out;
    }
    mat->data = NULL;
    mat->m = m;
    mat->n = n;

    err = cudaMalloc((void **)&mat->data, sizeof(double) * m * n);
    if (err != cudaSuccess) {
        fprintf(stderr, "cudaMalloc failed in lacu_matrix_alloc (%s)\n",
                cudaGetErrorString(err));
        goto error_out;
    }
    err = cudaMemset(mat->data, 0.0, sizeof(double) * m * n);
    if (err != cudaSuccess) {
        fprintf(stderr, "cudaMemset failed in lacu_matrix_alloc (%s)\n",
                cudaGetErrorString(err));
        goto error_out;
    }

    return mat;

error_out:
    if (mat != NULL) {
        if (mat->data != NULL) {
            cudaFree(mat->data);
        }
        free(mat);
    }
    return NULL;
}

/* Initialize the given, allocated matrix with values from a normal c-style two-dimensional array, returns 0 on success, otherwise non-zero */
int
lacu_matrix_init(void *matrix, double **array, int m, int n)
{
    lacu_matrix_t *mat;
    int i;
    int j;
    cudaError_t err;

    mat = (lacu_matrix_t *)matrix;
    if (mat->m != m || mat->n != n) {
        fprintf(stderr, "Bad dimentions in lacu_matrix_init\n");
        return -1;
    }

    for (i = 0; i < m; i++) {
        for (j = 0; j < n; j++) {
            err = cudaMemcpy(LACU_MATRIX_IDX(mat, i, j), &(array[i][j]), sizeof(double), cudaMemcpyHostToDevice);
            if (err != cudaSuccess) {
                fprintf(stderr, "Failed to copy to device in lacu_matrix_init (%s)\n",
                        cudaGetErrorString(err));
                return -1;
            }
        }
    }
    
    return 0;
}

/* Free memory held by matrix */
void
lacu_matrix_free(void *matrix)
{
    lacu_matrix_t *mat;
    
    mat = (lacu_matrix_t *)matrix;
    if (mat != NULL) {
        if (mat->data != NULL) {
            cudaFree(mat->data);
        }
        free(mat);
    }
}

/* Returns the entry at row i, column j */
double
lacu_matrix_get(void *matrix, int i, int j)
{
    lacu_matrix_t *mat;
    cudaError_t err;
    double res;

    mat = (lacu_matrix_t *)matrix;

    err = cudaMemcpy(&res, LACU_MATRIX_IDX(mat, i, j), sizeof(double), cudaMemcpyDeviceToHost);
    if (err != cudaSuccess) {
        fprintf(stderr, "Failed to copy from device in lacu_matrix_get (%s)\n",
                cudaGetErrorString(err));
        return 0.0;
    }
    return res;
}

/* Returns a vector representation of column j, assuming result is an already-allocated vector of the correct length. Returns 0 on success, otherwise non-zero */
int
lacu_matrix_get_col(void *result, void *matrix, int j)
{
    lacu_matrix_t *mat;
    lacu_vec_t *vec;
    cudaError_t err;

    vec = (lacu_vec_t *)result;
    mat = (lacu_matrix_t *)matrix;

    err = cudaMemcpy(LACU_VEC_IDX(vec, 0), LACU_MATRIX_IDX(mat, 0, j), sizeof(double) * mat->m, cudaMemcpyDeviceToDevice);
    if (err != cudaSuccess) {
        fprintf(stderr, "Failed to copy within device in lacu_matrix_get_col (%s)\n",
                cudaGetErrorString(err));
        return -1;
    }
    return 0;
}


/* Sets the value at row i, column j to value. Returns 0 on success, non-zero on error */
int
lacu_matrix_set(void *matrix, int i, int j, double value)
{
    lacu_matrix_t *mat;
    cudaError_t err;

    mat = (lacu_matrix_t *)matrix;

    err = cudaMemcpy(LACU_MATRIX_IDX(mat, i, j), &value, sizeof(double), cudaMemcpyHostToDevice);
    if (err != cudaSuccess) {
        fprintf(stderr, "Failed to copy to device in lacu_matrix_set (%s)\n",
                cudaGetErrorString(err));
        return -1;
    }
    return 0;
}

/* Sets the entries in the column j to the values given in vector */
int lacu_matrix_set_col(void *matrix, int j, void *vector)
{
    lacu_matrix_t *mat;
    lacu_vec_t *vec;
    cudaError_t err;

    vec = (lacu_vec_t *)vector;
    mat = (lacu_matrix_t *)matrix;

    err = cudaMemcpy(LACU_MATRIX_IDX(mat, 0, j), LACU_VEC_IDX(vec, 0), sizeof(double) * mat->m, cudaMemcpyDeviceToDevice);
    if (err != cudaSuccess) {
        fprintf(stderr, "Failed to copy within device in lacu_matrix_set_col (%s)\n",
                cudaGetErrorString(err));
        return -1;
    }
    return 0;
}

/* dumps a textual representation of the matrix on the given FILE * */
void
lacu_matrix_dump(FILE *fp, void *matrix)
{
    lacu_matrix_t *mat;
    double *temp;
    cudaError_t err;
    int i, j, col_offset;
    int rows, cols;

    mat = (lacu_matrix_t *)matrix;
    temp = (double *)malloc(sizeof(double) * mat->m * mat->n);
    if (temp == NULL) {
        fprintf(stderr, "No memory to copy back matrix in lacu_matrix_dump\n");
        goto done;
    }

    rows = mat->m;
    cols = mat->n;

    err = cudaMemcpy(temp, mat->data, sizeof(double) * rows * cols, cudaMemcpyDeviceToHost);
    if (err != cudaSuccess) {
        fprintf(stderr, "Failed to copy back matrix in lacu_matrix_dump (%s)\n",
                cudaGetErrorString(err));
        goto done;
    }

    for (col_offset = 0; col_offset < cols; col_offset += MAX_COL_WIDTH) {
        fprintf(fp, "Columns %d through %d\n",
                col_offset,
                col_offset + MAX_COL_WIDTH < cols ? col_offset + MAX_COL_WIDTH - 1 : cols - 1);
        for(i = 0; i < rows; i++){
            fprintf(fp, "[%d] ", i);
            for(j = col_offset; j < cols && j < col_offset + MAX_COL_WIDTH; j++){
                fprintf(fp, "% f ", *(temp + j * rows + i));
            }
            fprintf(fp, "\n");
        }
    }

done:
    if (temp != NULL) {
        free(temp);
    }
}


/* Receives a matrix, and assigns random values to all entries. */
int
lacu_matrix_randomize(void *matrix, int min, int max)
{
    lacu_matrix_t *mat;
    uint64_t seed;
    double *temp = NULL;
    cudaError_t err;
    int rv = 0;

    mat = (lacu_matrix_t *)matrix;
    temp = (double *)malloc(sizeof(double) * mat->m * mat->n);
    if (temp == NULL) {
        fprintf(stderr, "Failed to alloc temporary memory in lacu_matrix_randomize\n");
        rv = -1;
        goto done;
    }

    seed = ReadTSC();
    srand(seed);

    for(int i = 0; i < mat->m * mat->n; i++){
            temp[i] = (rand() % (max - min + 1)) + min;
    }

    err = cudaMemcpy(mat->data, temp, sizeof(double) * mat->m * mat->n, cudaMemcpyHostToDevice);
    if (err != cudaSuccess) {
        fprintf(stderr, "Failed to copy to device in lacu_matrix_randomize (%s)\n",
                cudaGetErrorString(err));
        rv = -1;
        goto done;
    }
    
done:
    if (temp != NULL) {
        free(temp);
    }
    return rv;
}

/* Computes the inverse matrix, storing the result in an already-allocated matrix of the correct size. Returns 0 on success, otherwise non-zero if e.g., the matrix is not invertible */

/* Set res to the identity matrix */
__global__ void
lacu_matrix_inverse_init_kernel(double *res, int m, int n)
{
    int x = blockIdx.x * blockDim.x + threadIdx.x;
    int y = blockIdx.y * ELEMS_PER_BLOCK_Y + threadIdx.y;
    int bottom = (blockIdx.y + 1) * ELEMS_PER_BLOCK_Y;

    if (x < n) {
        for (; y < bottom && y < m; y += blockDim.y) {
            res[x * m + y] = (x == y ? 1 : 0);
        }
    }
}

/* Find the first non-zero entry in the i-th column, returned in integer value of res */
__global__ void
lacu_matrix_inverse_find_nz_kernel(double *res, double *mat, int m, int i)
{
    int j;
    if (threadIdx.x == 0) {
        for (j = i; j < m; j++) {
            if (mat[i * m + j] != 0.0) {
                break;
            }
        }
        *res = (double)j;
    }
}

/* Swap rows i and j in both matrices */
__global__ void
lacu_matrix_inverse_swap_rows_kernel(double *res, double *mat, int m, int i, int j) {
    int x = blockIdx.x * ELEMS_PER_BLOCK + threadIdx.x;
    int end = (blockIdx.x + 1) * ELEMS_PER_BLOCK;
    double temp;

    for (; x < end && x < m; x += blockDim.x) {
        temp = res[x * m + i];
        res[x * m + i] = res[x * m + j];
        res[x * m + j] = temp;

        temp = mat[x * m + i];
        mat[x * m + i] = mat[x * m + j];
        mat[x * m + j] = temp;
    }
}

/* Normalize all entries in row i by the value at i,i */
__global__ void
lacu_matrix_inverse_normalize_row_kernel(double *res, double *mat, int m, int i)
{
    int x = blockIdx.x * ELEMS_PER_BLOCK + threadIdx.x;
    int end = (blockIdx.x + 1) * ELEMS_PER_BLOCK;
    __shared__ double scalar;

    if (threadIdx.x == 0) {
        scalar = 1.0 / mat[i * m + i];
        mat[i * m + i] = 1.0;
    }
    __syncthreads();

    for (; x < end && x < m; x += blockDim.x) {
        if (x != i) {
            mat[x * m + i] = zapsmall_dev(scalar * mat[x * m + i]);
        }
        res[x * m + i] = zapsmall_dev(scalar * res[x * m + i]);
    }
}

/* Eliminate all other entries in column i */
__global__ void
lacu_matrix_inverse_eliminate_col_kernel(double *res, double *mat, double *mat_col_i, int m, int i)
{
    int x = blockIdx.x * blockDim.x + threadIdx.x;
    int y = blockIdx.y * ELEMS_PER_BLOCK_Y + threadIdx.y;
    int bottom = (blockIdx.y + 1) * ELEMS_PER_BLOCK_Y;
    
    extern __shared__ double local[];

    double *res_row_i;
    double *mat_row_i;
    double *scales;

    /* Get pointers into shared memory */
    res_row_i = local;
    mat_row_i = local + blockDim.x;
    scales = local + 2 * blockDim.x;
    
    __syncthreads();

    /* Load row i into shared memory */
    if (threadIdx.y == 0 && x < m) {
        res_row_i[threadIdx.x] = res[x * m + i];
        mat_row_i[threadIdx.x] = mat[x * m + i];
    }

    __syncthreads();

    /* Eliminate */
    for (; y < bottom && y < m; y += blockDim.y) {
    
        /* Load the coefficient of row y, col i into shared memory */
        if (threadIdx.x == 0) {
            scales[threadIdx.y] = mat_col_i[y];
        }
        __syncthreads();

        if (y != i && x < m) {
            mat[x * m + y] = zapsmall_dev(mat[x * m + y] - (mat_row_i[threadIdx.x] * scales[threadIdx.y]));
            res[x * m + y] = zapsmall_dev(res[x * m + y] - (res_row_i[threadIdx.x] * scales[threadIdx.y]));
        }
        __syncthreads();
    }
}

int
lacu_matrix_inverse(void *result, void *matrix)
{
    lacu_matrix_t *res;
    lacu_matrix_t *mat;
    cudaError_t err;
    int m;
    dim3 grid;
    dim3 threads_per_block;
    int i;
    double temp_d;
    int temp_i;
    int shared_size;


    res = (lacu_matrix_t *)result;
    mat = (lacu_matrix_t *)matrix;
    
    m = mat->m;

    err = cudaMemcpy(dev_buff_mm, mat->data, sizeof(double) * m * m, cudaMemcpyDeviceToDevice);
    if (err != cudaSuccess) {
        fprintf(stderr, "Failed to copy into temp memory in lacu_matrix_inverse (%s)\n",
                cudaGetErrorString(err));
        return -1;
    }

    /* Start result with identity matrix */
    grid.x = DIV_UP(m, THREADS_PER_BLOCK_X);
    grid.y = DIV_UP(m, ELEMS_PER_BLOCK_Y);
    grid.z = 1;
    threads_per_block.x = THREADS_PER_BLOCK_X;
    threads_per_block.y = THREADS_PER_BLOCK_Y;
    threads_per_block.z = 1;
    lacu_matrix_inverse_init_kernel<<<grid, threads_per_block>>>(res->data, m, m);
    err = cudaDeviceSynchronize();
    if (err != cudaSuccess) {
        fprintf(stderr, "lacu_matrix_inverse_init kernel failure (%s)\n",
                cudaGetErrorString(err));
        return -1;
    }

    for (i = 0; i < m; i++) {

        /* Find first non-zero entry and swap rows if needed */
        lacu_matrix_inverse_find_nz_kernel<<<1, 1>>>(dev_buff_m, dev_buff_mm, m, i);
        err = cudaDeviceSynchronize();
        if (err != cudaSuccess) {
            fprintf(stderr, "lacu_matrix_inverse find nz kernel failure (%s)\n",
                    cudaGetErrorString(err));
            return -1;
        }
        err = cudaMemcpy(&temp_d, dev_buff_m, sizeof(double), cudaMemcpyDeviceToHost);
        if (err != cudaSuccess) {
            fprintf(stderr, "lacu_matrix_inverse faild to copy back non-zero row index (%s)\n",
                    cudaGetErrorString(err));
            return -1;
        }
        temp_i = (int)temp_d;
        if (temp_i == m) {
            fprintf(stderr, "Matrix un-invertible!\n");
            return -1;
        }
        if (temp_i != i) {
            grid.x = DIV_UP(m, ELEMS_PER_BLOCK);
            grid.y = 1;
            grid.z = 1;
            threads_per_block.x = THREADS_PER_BLOCK;
            threads_per_block.y = 1;
            threads_per_block.z = 1;
            lacu_matrix_inverse_swap_rows_kernel<<<grid, threads_per_block>>>(res->data, dev_buff_mm, m, i, temp_i);
            err = cudaDeviceSynchronize();
            if (err != cudaSuccess) {
                fprintf(stderr, "lacu_matrix_inverse swap rows kernel failure (%s)\n",
                    cudaGetErrorString(err));
                return -1;
            }
        }

        /* Normalize row i */
        grid.x = DIV_UP(m, ELEMS_PER_BLOCK);
        grid.y = 1;
        grid.z = 1;
        threads_per_block.x = THREADS_PER_BLOCK;
        threads_per_block.y = 1;
        threads_per_block.z = 1;
        lacu_matrix_inverse_normalize_row_kernel<<<grid, threads_per_block>>>(res->data, dev_buff_mm, m, i);
        err = cudaDeviceSynchronize();
        if (err != cudaSuccess) {
            fprintf(stderr, "lacu_matrix_inverse normalize row kernel failure (%s)\n",
                    cudaGetErrorString(err));
            return -1;
        }

        /* Extract i-th column */
        err = cudaMemcpy(dev_buff_m, dev_buff_mm + m * i, sizeof(double) * m, cudaMemcpyDeviceToDevice);
        if (err != cudaSuccess) {
            fprintf(stderr, "Failed to extract i-th column in lacu_matrix_inverse (%s)\n",
                    cudaGetErrorString(err));
            return -1;
        }

        /* Eliminate other entries */
        grid.x = DIV_UP(m, THREADS_PER_BLOCK_X);
        grid.y = DIV_UP(m, ELEMS_PER_BLOCK_Y);
        grid.z = 1;
        threads_per_block.x = THREADS_PER_BLOCK_X;
        threads_per_block.y = THREADS_PER_BLOCK_Y;
        threads_per_block.z = 1;
        shared_size = 2 * THREADS_PER_BLOCK_X + THREADS_PER_BLOCK_Y;
        lacu_matrix_inverse_eliminate_col_kernel<<<grid, threads_per_block, shared_size * sizeof(double)>>>
                (res->data, dev_buff_mm, dev_buff_m, m, i);
        err = cudaDeviceSynchronize();
        if (err != cudaSuccess) {
            fprintf(stderr, "lacu_matrix_inverse eliminate column kernel failure (%s)\n",
                    cudaGetErrorString(err));
            return -1;
        }

    }

    return 0;
}

/* Extract the colums specified by the index vector idx_vec */

/* Each block gets a column */
__global__ void
lacu_matrix_extract_vec_kernel(double *res, double *mat, double *idx, int m)
{
    int i;
    
    for (i = threadIdx.x; i < m; i += blockDim.x) {
        res[blockIdx.x * m + i] = mat[((int)idx[blockIdx.x]) * m + i];
    }
}

int
lacu_matrix_extract_vec(void *result, void *matrix, void *idx_vec)
{
    lacu_matrix_t *res;
    lacu_matrix_t *mat;
    lacu_vec_t *v;
    cudaError_t err;

    res = (lacu_matrix_t *)result;
    mat = (lacu_matrix_t *)matrix;
    v = (lacu_vec_t *)idx_vec;

    lacu_matrix_extract_vec_kernel<<<v->m, THREADS_PER_BLOCK>>>(res->data, mat->data, v->data, mat->m);
    err = cudaDeviceSynchronize();
    if (err != cudaSuccess) {
        fprintf(stderr, "lacu_matrix_extract_vec kernel failure (%s)\n",
                cudaGetErrorString(err));
        return -1;
    }
    
    return 0;
}


/*
 * Vector functions
 */

/* Allocate a new vector with m entries */
void *
lacu_vec_alloc(int m)
{
    cudaError_t err;
    lacu_vec_t *vec = NULL;

    vec = (lacu_vec_t *)malloc(sizeof(lacu_vec_t));
    if (vec == NULL) {
        fprintf(stderr, "No memory for host side vec in lacu_vec_alloc\n");
        goto error_out;
    }
    vec->data = NULL;
    vec->m = m;

    err = cudaMalloc((void **)&vec->data, sizeof(double) * m);
    if (err != cudaSuccess) {
        fprintf(stderr, "cudaMalloc failed in lacu_vec_alloc (%s)\n",
                cudaGetErrorString(err));
        goto error_out;
    }
    err = cudaMemset(vec->data, 0.0, sizeof(double) * m);
    if (err != cudaSuccess) {
        fprintf(stderr, "cudaMemset failed in lacu_vec_alloc (%s)\n",
                cudaGetErrorString(err));
        goto error_out;
    }

    return vec;

error_out:
    if (vec != NULL) {
        if (vec->data != NULL) {
            cudaFree(vec->data);
        }
        free(vec);
    }

    return NULL;
}

/* Initialize the given vector from a normal c-style array */
int
lacu_vec_init(void *vec, double *array, int m)
{
    lacu_vec_t *v;
    cudaError_t err;

    v = (lacu_vec_t *)vec;
    if (v->m != m) {
        fprintf(stderr, "Bad dimensions in lacu_vec_init\n");
        return -1;
    }

    err = cudaMemcpy(v->data, array, sizeof(double) * m, cudaMemcpyHostToDevice);
    if (err != cudaSuccess) {
        fprintf(stderr, "Failed to copy to device in lacu_vec_init (%s)\n",
                cudaGetErrorString(err));
        return -1;
    }

    return 0;
}

/* Free the given vector */
void
lacu_vec_free(void *vec)
{
    lacu_vec_t *v;

    v = (lacu_vec_t *)vec;

    if (v != NULL) {
        if (v->data != NULL) {
            cudaFree(v->data);
        }
        free(v);
    }
}

/* Return the i-th entry of the given vector */
double
lacu_vec_get(void *vec, int i)
{
    lacu_vec_t *v;
    cudaError_t err;
    double res;

    v = (lacu_vec_t *)vec;
    err = cudaMemcpy(&res, v->data + i, sizeof(double), cudaMemcpyDeviceToHost);
    if (err != cudaSuccess) {
        fprintf(stderr, "Failed to copy from device in lacu_vec_get (%s)\n",
                cudaGetErrorString(err));
        return 0.0;
    }

    return res;
}

/* Sets the i-th entry of the given vector to value */
int
lacu_vec_set(void *vec, int i, double value)
{
    lacu_vec_t *v;
    cudaError_t err;

    v = (lacu_vec_t *)vec;
    err = cudaMemcpy(v->data + i, &value, sizeof(double), cudaMemcpyHostToDevice);
    if (err != cudaSuccess) {
        fprintf(stderr, "Failed to copy to device in lacu_vec_set (%s)\n",
                cudaGetErrorString(err));
        return -1;
    }

    return 0;
}

/* Dumps a textual representation of the vector on the given FILE * */
void
lacu_vec_dump(FILE *fp, void *vec)
{
    double *temp = NULL;
    lacu_vec_t *v;
    cudaError_t err;
    int i;

    v = (lacu_vec_t *)vec;
    temp = (double *)malloc(sizeof(double) * v->m);
    if (temp == NULL) {
        fprintf(stderr, "Failed to allocate temporary memory in lacu_vec_dump\n");
        goto done;
    }
    
    err = cudaMemcpy(temp, v->data, sizeof(double) * v->m, cudaMemcpyDeviceToHost);
    if (err != cudaSuccess) {
        fprintf(stderr, "Failed to copy from device in lacu_vec_dump (%s)\n",
                cudaGetErrorString(err));
        goto done;
    }

    for (i = 0; i < v->m; i++) {
        fprintf(fp, "[%d] %f\n", i, temp[i]);
    }
    
done:
    if (temp != NULL) {
        free(temp);
    }
}

/* Returns a vector which contains the elements specified by the index vector idx_vec */

__global__ void
lacu_vec_extract_vec_kernel(double *dst, double *src, double *idx, int m)
{
    int x = blockIdx.x * ELEMS_PER_BLOCK + threadIdx.x;
    int max_x = (blockIdx.x + 1) * ELEMS_PER_BLOCK;
    int i;
    
    for (i = x; i < max_x && i < m; i += blockDim.x) {
        dst[i] = src[(int)idx[i]];
    }
}

int
lacu_vec_extract_vec(void *result, void *vec, void *idx_vec)
{
    lacu_vec_t *res;
    lacu_vec_t *v;
    lacu_vec_t *idx;
    int num_blocks;
    cudaError_t err;

    res = (lacu_vec_t *)result;
    v = (lacu_vec_t *)vec;
    idx = (lacu_vec_t *)idx_vec;

    num_blocks = DIV_UP(idx->m, ELEMS_PER_BLOCK);
    lacu_vec_extract_vec_kernel<<<num_blocks, THREADS_PER_BLOCK>>>(res->data, v->data, idx->data, idx->m);
    err = cudaDeviceSynchronize();
    if (err != cudaSuccess) {
        fprintf(stderr, "lacu_vec_extract_vec kernel failure (%s)\n",
                cudaGetErrorString(err));
        return -1;
    }

    return 0;
}

/* Receives a vector, and assigns random values to all entries. */
/* Note: assuming this is not performance sensitive . . . */
int lacu_vec_randomize(void *vec, int min, int max)
{
    lacu_vec_t *v;
    uint64_t seed;
    double *temp = NULL;
    cudaError_t err;
    int rv = 0;

    v = (lacu_vec_t *)vec;
    temp = (double *)malloc(sizeof(double) * v->m);
    if (temp == NULL) {
        fprintf(stderr, "Failed to alloc temporary memory in lacu_vec_randomize\n");
        rv = -1;
        goto done;
    }

    seed = ReadTSC();
    srand(seed);

    for(int i = 0; i < v->m; i++){
            temp[i] = (rand() % (max - min + 1)) + min;
    }

    err = cudaMemcpy(v->data, temp, sizeof(double) * v->m, cudaMemcpyHostToDevice);
    if (err != cudaSuccess) {
        fprintf(stderr, "Failed to copy to device in lacu_vec_randomize (%s)\n",
                cudaGetErrorString(err));
        rv = -1;
        goto done;
    }
    
done:
    if (temp != NULL) {
        free(temp);
    }
    return rv;
}

/*
 * Operations
 */

/* Scalar value times a vector */

__global__ void
lacu_scalar_vec_mult_kernel(double *dst, double *src, int m, double value)
{
    int x = blockIdx.x * ELEMS_PER_BLOCK + threadIdx.x;
    int max_x = (blockIdx.x + 1) * ELEMS_PER_BLOCK;
    int i;
    
    for (i = x; i < max_x && i < m; i += blockDim.x) {
        dst[i] = value * src[i];
    }
}


int
lacu_scalar_vec_mult(void *result, double value, void *vec)
{
    lacu_vec_t *res;
    lacu_vec_t *v;
    int num_blocks;
    cudaError_t err;

    res = (lacu_vec_t *)result;
    v = (lacu_vec_t *)vec;
    
    num_blocks = DIV_UP(v->m, ELEMS_PER_BLOCK);
    lacu_scalar_vec_mult_kernel<<<num_blocks, THREADS_PER_BLOCK>>>(res->data, v->data, v->m, value);
    err = cudaDeviceSynchronize();
    if (err != cudaSuccess) {
        fprintf(stderr, "lacu_scalar_vec_mult kernel failure (%s)\n",
                cudaGetErrorString(err));
        return -1;
    }
    return 0;
}

/* Element-wise subtraction */

__global__ void
lacu_vec_vec_subtract_kernel(double *dst, double *src1, double *src2, int m)
{
    int x = blockIdx.x * ELEMS_PER_BLOCK + threadIdx.x;
    int max_x = (blockIdx.x + 1) * ELEMS_PER_BLOCK;
    int i;
    
    for (i = x; i < max_x && i < m; i += blockDim.x) {
        dst[i] = zapsmall_dev(src1[i] - src2[i]);
    }
}

int
lacu_vec_vec_subtract(void *result, void *vec1, void *vec2)
{
    lacu_vec_t *res;
    lacu_vec_t *v1;
    lacu_vec_t *v2;
    int num_blocks;
    cudaError_t err;

    res = (lacu_vec_t *)result;
    v1 = (lacu_vec_t *)vec1;
    v2 = (lacu_vec_t *)vec2;
    
    num_blocks = DIV_UP(res->m, ELEMS_PER_BLOCK);
    lacu_vec_vec_subtract_kernel<<<num_blocks, THREADS_PER_BLOCK>>>(res->data, v1->data, v2->data, res->m);
    err = cudaDeviceSynchronize();
    if (err != cudaSuccess) {
        fprintf(stderr, "lacu_vec_vec_subtract kernel failure (%s)\n",
                cudaGetErrorString(err));
        return -1;
    }
    return 0;
}

/* Dot-product of two vectors */

/* Block-reduction kernel: each block generates an entry in dst */
__global__ void
lacu_vec_dot_prod_kernel(double *dst, double *src1, double *src2, int m)
{
    extern __shared__ double local[];
    int x = blockIdx.x * ELEMS_PER_BLOCK + threadIdx.x;
    int block_max = (blockIdx.x + 1) * ELEMS_PER_BLOCK;
    int i;
    double acc;
    
    /* Accumulate for all elements assigned to this thread */
    acc = 0.0;
    for (i = x; i < block_max && i < m; i += blockDim.x) {
        acc += src1[i] * src2[i];
    }
    local[threadIdx.x] = acc;

    __syncthreads();

    /* Accumulate between threads in this block */
    for (i = blockDim.x >> 1; i > 0; i >>= 1) {
        if (threadIdx.x < i) {
            local[threadIdx.x] += local[threadIdx.x + i];
        }
        __syncthreads();
    }

    /* Write final result for this block */
    if (threadIdx.x == 0) {
        dst[blockIdx.x] = local[0];
    }
}

int
lacu_vec_dot_prod(double *result, void *vec1, void *vec2)
{
    lacu_vec_t *v1;
    lacu_vec_t *v2;
    int num_blocks;
    cudaError_t err;
    int i;
    double acc;

    v1 = (lacu_vec_t *)vec1;
    v2 = (lacu_vec_t *)vec2;
    
    num_blocks = DIV_UP(v1->m, ELEMS_PER_BLOCK);
    lacu_vec_dot_prod_kernel<<<num_blocks, THREADS_PER_BLOCK, sizeof(double) * THREADS_PER_BLOCK>>>(dev_buff_n, v1->data, v2->data, v2->m);
    err = cudaDeviceSynchronize();
    if (err != cudaSuccess) {
        fprintf(stderr, "lacu_vec_dot_prod kernel failure (%s)\n",
                cudaGetErrorString(err));
        return -1;
    }

    err = cudaMemcpy(host_buff_n, dev_buff_n, sizeof(double) * num_blocks, cudaMemcpyDeviceToHost);
    if (err != cudaSuccess) {
        fprintf(stderr, "lacu_vec_dot_prod failed to copy result back from device (%s)\n",
                cudaGetErrorString(err));
        return -1;
    }

    acc = 0.0;
    for (i = 0; i < num_blocks; i++) {
        acc += host_buff_n[i];
    }
    *result = acc;

    return 0;
}

/* Matrix-vector multiplication */

/* Naively assign block to each result entry, reduction in each block */
__global__ void
lacu_matrix_vec_mult_kernel(double *res, double *mat, double *vec, int m, int n)
{
    extern __shared__ double local[];
    int i;
    double acc;

    acc = 0.0;
    for (i = threadIdx.x; i < n; i += blockDim.x) {
        acc += vec[i] * mat[i * m + blockIdx.x];
    }
    local[threadIdx.x] = acc;

    __syncthreads();

    for (i = blockDim.x >> 1; i > 0; i >>= 1) {
        if (threadIdx.x < i) {
            local[threadIdx.x] += local[threadIdx.x + i];
        }
        __syncthreads();
    }

    if (threadIdx.x == 0) {
        res[blockIdx.x] = local[0];
    }
}

int
lacu_matrix_vec_mult(void *result, void *matrix, void *vector)
{
    lacu_vec_t *res;
    lacu_matrix_t *mat;
    lacu_vec_t *v;
    cudaError_t err;

    res = (lacu_vec_t *)result;
    mat = (lacu_matrix_t *)matrix;
    v = (lacu_vec_t *)vector;

    lacu_matrix_vec_mult_kernel<<<res->m, THREADS_PER_BLOCK, sizeof(double) * THREADS_PER_BLOCK>>>
            (res->data, mat->data, v->data, mat->m, mat->n);
    err = cudaDeviceSynchronize();
    if (err != cudaSuccess) {
        fprintf(stderr, "lacu_matrix_vec_mult kernel failure (%s)\n",
                cudaGetErrorString(err));
        return -1;
    }
    return 0;
}

/* Vector-matrix multiplication */

/* Naively assign block to each result entry, reduction in each block */
__global__ void
lacu_vec_matrix_mult_kernel(double *res, double *vec, double *mat, int m, int n)
{
    extern __shared__ double local[];
    int i;
    double acc;

    acc = 0.0;
    for (i = threadIdx.x; i < m; i += blockDim.x) {
        acc += vec[i] * mat[blockIdx.x * m + i];
    }
    local[threadIdx.x] = acc;

    __syncthreads();

    for (i = blockDim.x >> 1; i > 0; i >>= 1) {
        if (threadIdx.x < i) {
            local[threadIdx.x] += local[threadIdx.x + i];
        }
        __syncthreads();
    }

    if (threadIdx.x == 0) {
        res[blockIdx.x] = local[0];
    }
}

int
lacu_vec_matrix_mult(void *result, void *vec, void *matrix)
{
    lacu_vec_t *res;
    lacu_matrix_t *mat;
    lacu_vec_t *v;
    cudaError_t err;

    res = (lacu_vec_t *)result;
    mat = (lacu_matrix_t *)matrix;
    v = (lacu_vec_t *)vec;

    lacu_vec_matrix_mult_kernel<<<res->m, THREADS_PER_BLOCK, sizeof(double) * THREADS_PER_BLOCK>>>
            (res->data, v->data, mat->data, mat->m, mat->n);
    err = cudaDeviceSynchronize();
    if (err != cudaSuccess) {
        fprintf(stderr, "lacu_vec_matrix_mult kernel failure (%s)\n",
                cudaGetErrorString(err));
        return -1;
    }
    return 0;
}

/* Matrix-matrix multiplication */

/* Naive block-per-element assuming in1 is l by m, in2 is m by n, and res is l by n */
__global__ void
lacu_matrix_matrix_mult_kernel(double *res, double *in1, double *in2, int l, int m, int n)
{
    extern __shared__ double local[];
    int i;
    double acc;

    acc = 0.0;
    for (i = threadIdx.x; i < m; i += blockDim.x) {
        acc += in1[i * l + blockIdx.y] * in2[blockIdx.x * m + i];
    }
    
    local[threadIdx.x] = acc;

    __syncthreads();

    for (i = blockDim.x >> 1; i > 0; i >>= 1) {
        if (threadIdx.x < i) {
            local[threadIdx.x] += local[threadIdx.x + i];
        }
        __syncthreads();
    }

    if (threadIdx.x == 0) {
        res[blockIdx.x * l + blockIdx.y] = local[0];
    }
}

int lacu_matrix_matrix_mult(void *result, void *matrix1, void *matrix2)
{
    lacu_matrix_t *res;
    lacu_matrix_t *mat1;
    lacu_matrix_t *mat2;
    cudaError_t err;
    dim3 grid;

    res = (lacu_matrix_t *)result;
    mat1 = (lacu_matrix_t *)matrix1;
    mat2 = (lacu_matrix_t *)matrix2;

    grid.x = mat2->n;
    grid.y = mat1->m;
    grid.z = 1;
    lacu_matrix_matrix_mult_kernel<<<grid, THREADS_PER_BLOCK, sizeof(double) * THREADS_PER_BLOCK>>>
            (res->data, mat1->data, mat2->data, mat1->m, mat2->m, mat2->n);
    err = cudaDeviceSynchronize();
    if (err != cudaSuccess) {
        fprintf(stderr, "lacu_matrix_matrix_mult kernel failure (%s)\n",
                cudaGetErrorString(err));
        return -1;
    }
    return 0;
}


/* Matrix column sum if less than zero: each entry in result_vec is the sum of the corresponding column in matrix only taking into consideration rows where index_vec is less than zero */


__global__ void
lacu_matrix_colsum_ltz_kernel(double *res, double *mat, double *vec, int m, int n)
{
    extern __shared__ double local[];
    int i;
    double acc;

    acc = 0.0;
    for (i = threadIdx.x; i < m; i += blockDim.x) {
        if (vec[i] < 0) {
            acc += mat[blockIdx.x * m + i];
        }
    }
    local[threadIdx.x] = acc;

    __syncthreads();

    for (i = blockDim.x >> 1; i > 0; i >>= 1) {
        if (threadIdx.x < i) {
            local[threadIdx.x] += local[threadIdx.x + i];
        }
        __syncthreads();
    }

    if (threadIdx.x == 0) {
        res[blockIdx.x] = local[0];
    }
}

int lacu_matrix_colsum_ltz(void *result_vec, void *matrix, void *index_vec)
{
    lacu_vec_t *res;
    lacu_matrix_t *mat;
    lacu_vec_t *v;
    cudaError_t err;

    res = (lacu_vec_t *)result_vec;
    mat = (lacu_matrix_t *)matrix;
    v = (lacu_vec_t *)index_vec;

    lacu_matrix_colsum_ltz_kernel<<<mat->n, THREADS_PER_BLOCK, sizeof(double) * THREADS_PER_BLOCK>>>
            (res->data, mat->data, v->data, mat->m, mat->n);
    err = cudaDeviceSynchronize();
    if (err != cudaSuccess) {
        fprintf(stderr, "lacu_matrix_colsum_ltz kernel failure (%s)\n",
                cudaGetErrorString(err));
        return -1;
    }
    return 0;
}

/* Returns both value and index of the minimum element of the given vector */

__global__ void
lacu_vec_min_kernel(double *res, double *res_idx, double *vec, int m)
{
    extern __shared__ double local[];
    double *mins;
    int *min_idxs;
    int i = blockIdx.x * ELEMS_PER_BLOCK + threadIdx.x;
    int block_max = (blockIdx.x + 1) * ELEMS_PER_BLOCK;
    double min;
    int min_idx;

    mins = local;
    min_idxs = (int *)(local + blockDim.x);

    min = INFINITY;
    for (; i < block_max && i < m; i += blockDim.x) {
        if (vec[i] < min) {
            min = vec[i];
            min_idx = i;
        }
    }
    mins[threadIdx.x] = min;
    min_idxs[threadIdx.x] = min_idx;
    
    __syncthreads();

    for (i = blockDim.x >> 1; i > 0; i >>= 1) {
        if (threadIdx.x < i) {
            if (mins[threadIdx.x + i] < mins[threadIdx.x]) {
                mins[threadIdx.x] = mins[threadIdx.x + i];
                min_idxs[threadIdx.x] = min_idxs[threadIdx.x + i];
            }
        }
        __syncthreads();
    }

    if (threadIdx.x == 0) {
        res[blockIdx.x] = mins[0];
        res_idx[blockIdx.x] = (double)min_idxs[0];
    }
}

void lacu_vec_min(double *min_val, int *min_idx, void *vector)
{
    lacu_vec_t *vec;
    int num_blocks;
    int i;
    double tmp_min;
    int tmp_idx;
    cudaError_t err;
    int shared_size;
    
    vec = (lacu_vec_t *)vector;
    num_blocks = DIV_UP(vec->m, ELEMS_PER_BLOCK);
    shared_size = (sizeof(double) + sizeof(int)) * THREADS_PER_BLOCK;
    lacu_vec_min_kernel<<<num_blocks, THREADS_PER_BLOCK, shared_size>>>(dev_buff_n, dev_buff_m, vec->data, vec->m);
    err = cudaDeviceSynchronize();
    if (err != cudaSuccess) {
        fprintf(stderr, "lacu_vec_min kernel failure (%s)\n",
                cudaGetErrorString(err));
        return;
    }

    err = cudaMemcpy(host_buff_n, dev_buff_n, sizeof(double) * num_blocks, cudaMemcpyDeviceToHost);
    if (err != cudaSuccess) {
        fprintf(stderr, "lacu_vec_dot_prod failed to copy result back from device (%s)\n",
                cudaGetErrorString(err));
        return;
    }
    err = cudaMemcpy(host_buff_m, dev_buff_m, sizeof(double) * num_blocks, cudaMemcpyDeviceToHost);
    if (err != cudaSuccess) {
        fprintf(stderr, "lacu_vec_dot_prod failed to copy result back from device (%s)\n",
                cudaGetErrorString(err));
        return;
    }

    tmp_min = INFINITY;
    tmp_idx = -1;
    for (i = 0; i < num_blocks; i++) {
        if (host_buff_n[i] < tmp_min) {
            tmp_min = host_buff_n[i];
            tmp_idx = host_buff_m[i];
        }
    }

    if (min_val != NULL) {
        *min_val = tmp_min;
    }
    if (min_idx != NULL) {
        *min_idx = (int)tmp_idx;
    }
}

/* Minimum ratio test for phase 1: return min {beta_i / alphaq_i such that (beta_i < 0 and alphaq_i < 0) or (beta_i >= 0 && alphaq_i > 0)} */

__global__ void
lacu_phase1_min_ratio_kernel(double *theta, double *p, double *beta, double *alphaq, int m)
{
    extern __shared__ double local[];
    double *mins;
    int *min_idxs;

    int i = threadIdx.x;
    double min;
    int min_idx;
    double betai;
    double alphaqi;
    double quot;

    mins = local;
    min_idxs = (int *)(local + blockDim.x);

    min = INFINITY;
    min_idx = -1;
    for (; i < m; i += blockDim.x) {
        betai = zapsmall_dev(beta[i]);
        alphaqi = zapsmall_dev(alphaq[i]);
        if ((betai < 0 && alphaqi < 0) || (betai >= 0 && alphaqi > 0)) {
            quot = betai / alphaqi;
            if (quot < min) {
                min = quot;
                min_idx = i;
            }
        }
    }
    mins[threadIdx.x] = min;
    min_idxs[threadIdx.x] = min_idx;
    
    __syncthreads();

    for (i = blockDim.x >> 1; i > 0; i >>= 1) {
        if (threadIdx.x < i) {
            if (mins[threadIdx.x + i] < mins[threadIdx.x]) {
                mins[threadIdx.x] = mins[threadIdx.x + i];
                min_idxs[threadIdx.x] = min_idxs[threadIdx.x + i];
            }
        }
        __syncthreads();
    }

    if (threadIdx.x == 0) {
        *theta = mins[0];
        *p = (double)min_idxs[0];
    }
}

int lacu_phase1_min_ratio(double *theta, int *p, void *beta, void *alphaq)
{
    lacu_vec_t *b;
    lacu_vec_t *a;
    cudaError_t err;
    double temp_theta;
    double temp_p;
    int shared_size;

    b = (lacu_vec_t *)beta;
    a = (lacu_vec_t *)alphaq;

    shared_size = (sizeof(double) + sizeof(int)) * THREADS_PER_BLOCK;
    lacu_phase1_min_ratio_kernel<<<1, THREADS_PER_BLOCK, shared_size>>>(dev_buff_m, dev_buff_n, b->data, a->data, a->m);
    err = cudaDeviceSynchronize();
    if (err != cudaSuccess) {
        fprintf(stderr, "lacu_phase1_min_ratio kernel failure (%s)\n",
                cudaGetErrorString(err));
        return -1;
    }

    err = cudaMemcpy(&temp_theta, dev_buff_m, sizeof(double), cudaMemcpyDeviceToHost);
    if (err != cudaSuccess) {
        fprintf(stderr, "lacu_phase1_min_ratio failed to copy back result (%s)\n",
                cudaGetErrorString(err));
        return -1;
    }

    err = cudaMemcpy(&temp_p, dev_buff_n, sizeof(double), cudaMemcpyDeviceToHost);
    if (err != cudaSuccess) {
        fprintf(stderr, "lacu_phase1_min_ratio failed to copy back result (%s)\n",
                cudaGetErrorString(err));
        return -1;
    }

    *theta = temp_theta;
    *p = (int)temp_p;

    return 0;
}

/* Minimum ratio test for phase 2: return min {beta_i / alphaq_i such that alphaq_i > 0} */

__global__ void
lacu_phase2_min_ration_kernel(double *theta, double *p, double *beta, double *alphaq, int m)
{
    extern __shared__ double local[];
    double *mins;
    int *min_idxs;

    int i = threadIdx.x;
    double min;
    int min_idx;
    double betai;
    double alphaqi;
    double quot;

    mins = local;
    min_idxs = (int *)(local + blockDim.x);

    min = INFINITY;
    min_idx = -1;
    for (; i < m; i += blockDim.x) {
        betai = zapsmall_dev(beta[i]);
        alphaqi = zapsmall_dev(alphaq[i]);
        if (alphaqi > 0) {
            quot = betai / alphaqi;
            if (quot < min) {
                min = quot;
                min_idx = i;
            }
        }
    }
    mins[threadIdx.x] = min;
    min_idxs[threadIdx.x] = min_idx;
    
    __syncthreads();

    for (i = blockDim.x >> 1; i > 0; i >>= 1) {
        if (threadIdx.x < i) {
            if (mins[threadIdx.x + i] < mins[threadIdx.x]) {
                mins[threadIdx.x] = mins[threadIdx.x + i];
                min_idxs[threadIdx.x] = min_idxs[threadIdx.x + i];
            }
        }
        __syncthreads();
    }

    if (threadIdx.x == 0) {
        *theta = mins[0];
        *p = (double)min_idxs[0];
    }
}

int lacu_phase2_min_ratio(double *theta, int *p, void *beta, void *alphaq)
{
    lacu_vec_t *b;
    lacu_vec_t *a;
    cudaError_t err;
    double temp_theta;
    double temp_p;
    int shared_size;

    b = (lacu_vec_t *)beta;
    a = (lacu_vec_t *)alphaq;

    shared_size = (sizeof(double) + sizeof(int)) * THREADS_PER_BLOCK;
    lacu_phase2_min_ration_kernel<<<1, THREADS_PER_BLOCK, shared_size>>>(dev_buff_m, dev_buff_n, b->data, a->data, a->m);
    err = cudaDeviceSynchronize();
    if (err != cudaSuccess) {
        fprintf(stderr, "lacu_phase2_min_ratio kernel failure (%s)\n",
                cudaGetErrorString(err));
        return -1;
    }

    err = cudaMemcpy(&temp_theta, dev_buff_m, sizeof(double), cudaMemcpyDeviceToHost);
    if (err != cudaSuccess) {
        fprintf(stderr, "lacu_phase2_min_ratio failed to copy back result (%s)\n",
                cudaGetErrorString(err));
        return -1;
    }

    err = cudaMemcpy(&temp_p, dev_buff_n, sizeof(double), cudaMemcpyDeviceToHost);
    if (err != cudaSuccess) {
        fprintf(stderr, "lacu_phase2_min_ratio failed to copy back result (%s)\n",
                cudaGetErrorString(err));
        return -1;
    }

    *theta = temp_theta;
    *p = (int)temp_p;

    return 0;
}


#ifdef __cplusplus /* Make sure we can link to this function from normal C code */
extern "C"
{
#endif

/* Populates the pointers in the ops struct to use these matrix and vector functions */
void
lacu_set_ops(struct la_ops *ops)
{
    ops->ensure_temps = lacu_ensure_temps;
    
    ops->matrix_alloc = lacu_matrix_alloc;
    ops->matrix_init = lacu_matrix_init;
    ops->matrix_free = lacu_matrix_free;
    ops->matrix_get = lacu_matrix_get;
    ops->matrix_get_col = lacu_matrix_get_col;
    ops->matrix_set = lacu_matrix_set;
    ops->matrix_set_col = lacu_matrix_set_col;
    ops->matrix_dump = lacu_matrix_dump;
    ops->matrix_randomize = lacu_matrix_randomize;
    ops->matrix_inverse = lacu_matrix_inverse;
    ops->matrix_extract_vec = lacu_matrix_extract_vec;

    ops->vec_alloc = lacu_vec_alloc;
    ops->vec_init = lacu_vec_init;
    ops->vec_free = lacu_vec_free;
    ops->vec_get = lacu_vec_get;
    ops->vec_set = lacu_vec_set;
    ops->vec_dump = lacu_vec_dump;
    ops->vec_extract_vec = lacu_vec_extract_vec; 
    ops->vec_randomize = lacu_vec_randomize;

    ops->scalar_vec_mult = lacu_scalar_vec_mult;
    ops->vec_vec_subtract = lacu_vec_vec_subtract;
    ops->vec_dot_prod = lacu_vec_dot_prod;
    ops->matrix_vec_mult = lacu_matrix_vec_mult;
    ops->vec_matrix_mult = lacu_vec_matrix_mult;
    ops->matrix_matrix_mult = lacu_matrix_matrix_mult;

    ops->matrix_colsum_ltz = lacu_matrix_colsum_ltz;
    ops->vec_min = lacu_vec_min;
    ops->phase1_min_ratio = lacu_phase1_min_ratio;
    ops->phase2_min_ratio = lacu_phase2_min_ratio;
}

#ifdef __cplusplus
} // extern "C"
#endif
