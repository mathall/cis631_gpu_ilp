/*
 * CIS 631, Fall 2019
 *
 * Implementation of set-management functions on bit maps.
 *
 * This file is un-used in the current implementation. kept for reference only. DO NOT USE THESE FUNCTIONS!
 *
 * Authors: Chris Misa
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <bm_set.h>

/* Populates the required pointers in the ops struct to use these set functions */
void
bm_set_set_ops(struct la_ops *ops)
{
    ops->set_alloc = bm_set_alloc;
    ops->set_free = bm_set_free;
    ops->set_add = bm_set_add;
    ops->set_remove = bm_set_remove;
    ops->set_test = bm_set_test;
    ops->set_get = bm_set_get;
    ops->set_inverse = bm_set_inverse;
    ops->set_dump = bm_set_dump;
}

/* Allocate a set to keep track of m distinct indices */
void *
bm_set_alloc(int m)
{
    struct bm_set_t *new;
    int size_bytes;

    new = (struct bm_set_t *)malloc(sizeof(struct bm_set_t));
    if (new == NULL) {
        return NULL;
    }
    new->size = m;
    new->num_elems = 0;
    size_bytes = (m >> 3) + 1; /* Just add an extra byte in case 8 !| size */
    new->data = (unsigned char *)malloc(size_bytes);
    if (new->data == NULL) {
        free(new);
        return NULL;
    }

    memset(new->data, 0, size_bytes);

    return new;
}

/* Free a set */
void
bm_set_free(void *set)
{
    struct bm_set_t *bm_set = (struct bm_set_t *)set;

    free(bm_set->data);
    free(bm_set);
}

/* Adds the i-th index to the set */
void
bm_set_add(void *set, int i)
{
    struct bm_set_t *bm_set = (struct bm_set_t *)set;

    if (i <= bm_set->size) {

        /* Only increment num_elems if the bit was not previously set */
        if ((bm_set->data[i >> 3] & (0x1 << (i & 0x7))) == 0) {
            bm_set->num_elems++;
        }
        bm_set->data[i >> 3] |= (0x1 << (i & 0x7));
    } else {
        fprintf(stderr, "Can't set bit %d in set of size %d\n",
                i, bm_set->size);
    }
}

/* Removes the i-th index from the set */
void
bm_set_remove(void *set, int i)
{
    struct bm_set_t *bm_set = (struct bm_set_t *)set;

    if (i <= bm_set->size) {
        bm_set->data[i >> 3] &= ~(0x1 << (i & 0x7));
        bm_set->num_elems--;
    } else {
        fprintf(stderr, "Can't set bit %d in set of size %d\n",
                i, bm_set->size);
    }
}

/* Returns non-zero if the i-th elemet is in the set, 0 otherwise */
int
bm_set_test(void *set, int i)
{
    struct bm_set_t *bm_set = (struct bm_set_t *)set;

    if (i <= bm_set->size) {
        return (bm_set->data[i >> 3] & (0x1 << (i & 0x7))) != 0;
    } else {
        fprintf(stderr, "Can't test bit %d in set of size %d\n",
                i, bm_set->size);
    }
}

/* Returns the index of the i-th element in the set */
int
bm_set_get(void *set, int i)
{
    struct bm_set_t *bm_set = (struct bm_set_t *)set;
    int j, k;

    k = 0;
    for (j = 0; j < bm_set->size; j++) {
        if (bm_set_test(set, j)) {
            if (k == i) {
                return j;
            }
            k++;
        }
    }
}

/* returns the inverse of the given set, assuming result is an already-allocated set of the same length */
void
bm_set_inverse(void *result, void *set)
{
    struct bm_set_t *orig, *new;
    int i;
    int n;

    new = (struct bm_set_t *)result;
    orig = (struct bm_set_t *)set;
    n = (orig->size >> 3) + 1;
    for (i = 0; i < n; i++) {
       new->data[i] = ~(orig->data[i]);
    }
    new->num_elems = orig->size - orig->num_elems;
}

/* Dumps a textual representation of the set on the given FILE * */
void
bm_set_dump(FILE *fp, void *set)
{
    int i;
    struct bm_set_t *set_ptr;
    int size_bytes;

    set_ptr = (struct bm_set_t *)set;
    fprintf(fp, "Set at <0x%p> with %d elements:", set, set_ptr->num_elems);
    for (i = 0; i < set_ptr->size; i++) {
        if ((set_ptr->data[i >> 3] & (0x1 << (i & 0x7))) != 0) {
            fprintf(fp, " %d", i);
        }
    }
    fprintf(fp, "\n");
}
