/* 
CIS 631: Parallel Programming.
This program is an implementation of the Simplex Algorithm for Linear Programming.
Written by: Matt Hall, Chris Misa
Simplex algoritm implemented with help from these slides:
https://www.cs.princeton.edu/~rs/AlgsDS07/22LinearProgramming.pdf
*/

#include <stdlib.h>
#include <stdio.h>

#include <data.h>
#include <common.h>
#include <read_mps.h>

/*
    Tablau
                        n vars           m *slack vars*        b - resource limit
                    __/          \_ | _/                   \_ |
                / (0,0) ... (0,n-1), (0,n), ... , (0,n+m-1),  (0,n+m)   
        m rows /    .   .               .   .                   .
(constraints)  \    .       .           .       .               .
                \   .           .       .           .           .
    1 solution  { (m, 0),   ... ,    (m,n)           ...      (m, m+n) }


Logic for constructing tablaue based on problem type and constraint (LT or GT)
    were written with help from these notes:
    https://www.ms.uky.edu/~cru227/MA162/LectureNotes/SimplexMethod1_07312014.pdf
    https://www.ms.uky.edu/~cru227/MA162/LectureNotes/LinearProgramming2_08042014

    Returns tableau and dimensions be reference.
    
    Returns 0 on success, otherwise, non-zero
*/
int
Simplex(double ** A,
        double b[],
        double c[],
        size_t b_len,
        size_t c_len,
        enum problem_type prob_type,
        enum constraints constraints,
        double ***tableau,
        size_t *tab_m,
        size_t *tab_n) {
    size_t M;
    size_t N;
    double **a;

    M = b_len;
    N = c_len; 

    // Construct Tablau 
    a = mallocMatrix(M+1, M+N+1);
    if (a == NULL) {
        fprintf(stderr, "No memory for allocating tableau\n");
        return -1;
    }

    // Copy A int tablau a
    for(int i = 0; i < M; i++){
        for(int j = 0; j < N; j++){
            if( prob_type == MAXIMIZE || constraints == LT )
                a[i][j] = A[i][j];
            else // store transpose of constraint matrix for Minimization Problems
                a[i][j] = A[j][i];
        }
    }

    // Copy Identiy matrix for slack variables
    for(int j = N; j < M + N; j++){
        a[j-N][j] = 1.0;
    }

    // Copy in the initial feasible solution
    for(int j = 0; j < N; j++){
        if ( prob_type == MAXIMIZE)
            a[M][j] = c[j];
        else if ( prob_type == MINIMIZE && constraints == LT)
            a[M][j] = -1 * c[j]; 
        else if ( prob_type == MINIMIZE && constraints == GT)
            a[M][j] = -1 * b[j];
            
    }
    
    // Copy in the resource limits
    for(int i = 0; i < M; i++){
        if (prob_type == MAXIMIZE || constraints == LT)
            a[i][M+N] = b[i];
        else if (prob_type == MINIMIZE && constraints == GT)
            a[i][M+N] = c[i];
    }

    *tableau = a;
    *tab_m = M;
    *tab_n = N;

    return 0;
}

/*
  pivot performs elemetary row operation on the tablau, a, by the element in cell (p, q)
*/
void
pivot(double **a, size_t M, size_t N, int p, int q)
{
    /* 
      Scale all elements by row p, column q
    */ 
    for(int i = 0; i <= M; i++){
        for (int j = 0; j <= M+N; j++){
            if(i != p && j != q){
                a[i][j] -= a[p][j] * a[i][q] / a[p][q];
            }
        }
    }
    /*
      Zero out column q
    */
    for(int i = 0; i <= M; i++){
        if(i != p){
            a[i][q] = 0.0;
        }
    }
    /*
      Scale row p
    */
    for(int j = 0; j <= M+N; j++){
        if (j != q){
            a[p][j] /= a[p][q];
        }
    }
    a[p][q] = 1.0;
}

void solve(double **a, size_t M, size_t N, int max_iterations, int prob_type, int constraints){
    int iteration = 0;
    while(iteration++ < max_iterations){
        int p, q;
        printf("iteration %d: \n", iteration);
        /*
          Walk accross values of the objective function (Row M)
          q becomes the column number of the *First Positive Cell*
        */
        printf("Walking row of test condition\n");
        for(q = 0; q < M+N; q++){
            printf("\tq: %d, \n\ta[M][%d]: %f\n\n", q, q, a[M][q]);
            if (a[M][q] > 0 && prob_type == MAXIMIZE)
                break;
            
            else if (a[M][q] < 0 && prob_type == MINIMIZE)
                break;
        }
        // If all cells from objective function row are negative, we're done. 
        if( q >= M+N){
            printf("q >= M+N; supposedly optimal solution was found!\n");
            printf("%d >= %lu+%lu; supposedly optimal solution was found!\n", q, M, N);
            
            break;
        }

        /* To Find pivot row *p* firstly, find the first positive cell in the 
            chosen column q...
        */
        for(p = 0; p < M; p++){
            if(a[p][q] > 0){
                break;
            }
        } 
        /*
            Then, choose the row whose ratio to the resource limit is lowest.
        */
        for(int i = p+1; i < M; i++){
            if(a[i][q] > 0){
                if(a[i][M+N] / a[i][q] < a[p][N+M] / a[p][q]){
                    p = i;
                }
            }
        }
        
        pivot(a, M, N, p, q);
    }
}

int
main(int argc, char *argv[])
{
    double ** A;
    double *b;
    double *c;
    int m;
    int n;
    enum problem_type type;
    enum constraints cons;
    int rv;

    double **Tableau;
    size_t M;
    size_t N;

    if (argc != 2) {
        printf("Usage: %s <MPS file>\n", argv[0]);
    }

    rv = read_mps(argv[1], &A, &b, &c, &m, &n, &type);
    if (rv != 0) {
        fprintf(stderr, "Failed to read mps file at \"%s\"\n", argv[1]);
        return 1;
    }

    if (type == MINIMIZE) {
        cons = GT;
    } else {
        cons = LT;
    }

    Simplex(A, b, c, m, n, type, cons, &Tableau, &M, &N);

    solve(Tableau, M, N, 500, type, cons);

    printf("Solved, the tableau is:\n");
    printMatrix(Tableau, M+1, M+N+1);

    return 0;
}

// int main(int argc, char* argv[]){
//     printf("Hello World!\n");
//     printf("argc: %d\n", argc);
//     printf("argv: ");
//     for(int i = 0; i < argc; i++)
//     {
//         printf("\t%s\n", argv[i]);
//     }
// 
//     int n = 2;
//     int m = 2;
//     double * b;
//     double * c;
//     double ** A = mallocMatrix(m, n); 
//     A[0][0] = 1;
//     A[0][1] = 2;
//     A[1][0] = 3;
//     A[1][1] = 2;
// 
//     b = calloc(m, sizeof(double));
//     b[0] = 6;
//     b[1] = 6;
// 
//     c = calloc(n, sizeof(double));
//     c[0] = 2;
//     c[1] = 5;
// 
//     int prob_type = MINIMIZE;
//     int constraints = GT; 
//     printMatrix(A, m, n);
// 
//     
//     // b = calloc(m, sizeof(double)); 
//     // c = calloc(n, sizeof(double)); 
//     printf("calling simplex\n");
//     Simplex(A, b, c, m, n, prob_type, constraints);
//     // printf("printing matrix");
//     // printMatrix(a, M+1, M+N+1);
//     solve(500, prob_type, constraints);
// 
//     printf("solved\n");
//     printMatrix(a, M+1, M+N+1);
//     return 0;
// }

