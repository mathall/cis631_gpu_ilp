/*
 * CIS 631, Fall 2019
 *
 * Simple, serial linear algebra operations using C-style storage
 * 
 * Authors: Chris Misa, Matt Hall
 *
 * NOTES: This implementation is meant to be extra verbose so we can debug the algorithms that use
 * these functions. Other algorithms may skip certain steps (e.g., bounds checking) in favor of performance.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#include <la_simple_serial.h>
#include <common.h>

#define MAX_COL_WIDTH 20
#define SINGULAR_MATRIX_DUMP_FILE "singular_matrix"

/* Allocate a new matrix with m rows and n columns. Sets all entries to 0.0 */
void *
lass_matrix_alloc(int m, int n)
{
    lass_matrix_t *new;

    new = (lass_matrix_t *)malloc(sizeof(lass_matrix_t));
    if (new == NULL) {
        return NULL;
    }

    new->m = m;
    new->n = n;
    new->data = (double *)malloc(sizeof(double) * m * n);
    if (new->data == NULL) {
        free(new);
        return NULL;
    }
    memset(new->data, 0, sizeof(double) * m * n);

    return new;
}

/* Initialize the given, allocated matrix with values from a normal c-style two-dimensional array, returns 0 on success, otherwise non-zero */
int
lass_matrix_init(void *matrix, double **array, int m, int n)
{
    int i;
    lass_matrix_t *mat_ptr;
    mat_ptr = (lass_matrix_t *)matrix;

    if (mat_ptr->m != m || mat_ptr->n != n) {
        fprintf(stderr, "Bad dimensions in lass_matrix_init\n");
        return -1;
    }
    for (i = 0; i < m; i++) {
        memcpy(mat_ptr->data + (i * n), array[i], n * sizeof(double));
    }
    return 0;
}

/* Free memory held by matrix */
void
lass_matrix_free(void *matrix)
{
    lass_matrix_t *mat_ptr;
    mat_ptr = (lass_matrix_t *)matrix;

    free(mat_ptr->data);
    free(mat_ptr);
}

/* Returns the entry at row i, column j */
double
lass_matrix_get(void *matrix, int i, int j)
{
    lass_matrix_t *mat_ptr;
    mat_ptr = (lass_matrix_t *)matrix;

    if (i < 0 || i >= mat_ptr->m) {
        fprintf(stderr, "Bad row index %d in lass_matrix_get\n", i);
        return 0.0;
    }
    if (j < 0 || j >= mat_ptr->n) {
        fprintf(stderr, "Bad column index %d in lass_matrix_get\n", j);
        return 0.0;
    }
    
    return mat_ptr->data[(i * mat_ptr->n) + j];
}

/* Returns a vector representation of column j owned by the caller, assuming result is an already-allocated vector of the correct length. Returns 0 on success, otherwise non-zero */
int
lass_matrix_get_col(void *result, void *matrix, int j)
{
    lass_matrix_t *mat_ptr;
    lass_vec_t *vec_ptr;
    int i;

    mat_ptr = (lass_matrix_t *)matrix;
    vec_ptr = (lass_vec_t *)result;

    if (vec_ptr->m != mat_ptr->m) {
        fprintf(stderr, "Bad dimensions in lass_matrix_get_col\n");
        return -1;
    }

    for (i = 0; i < mat_ptr->m; i++) {
        vec_ptr->data[i] = lass_matrix_get(matrix, i, j);
    }

    return 0;
}

/* Sets the value at row i, column j to value. Returns 0 on success, non-zero on error */
int
lass_matrix_set(void *matrix, int i, int j, double value)
{
    lass_matrix_t *mat_ptr;
    mat_ptr = (lass_matrix_t *)matrix;

    if (i < 0 || i >= mat_ptr->m) {
        fprintf(stderr, "Bad row index %d in lass_matrix_set\n", i);
        return -1;
    }
    if (j < 0 || j >= mat_ptr->n) {
        fprintf(stderr, "Bad column index %d in lass_matrix_set\n", j);
        return -1;
    }
    
    mat_ptr->data[(i * mat_ptr->n) + j] = value;
    return 0;
}

/* Sets the entries in the column j to the values given in vector */
int lass_matrix_set_col(void *matrix, int j, void *vector)
{
    lass_matrix_t *mat;
    lass_vec_t *vec;
    int i;

    mat = (lass_matrix_t *)matrix;
    vec = (lass_vec_t *)vector;
    
    if (vec->m != mat->m) {
        fprintf(stderr, "Bad dimentions in lass_matrix_set_col\n");
        return -1;
    }

    for (i = 0; i < vec->m; i++) {
        lass_matrix_set(matrix, i, j, vec->data[i]);
    }

    return 0;
}

/* dumps a textual representation of the matrix on the given file * */
void
lass_matrix_dump(FILE *fp, void *matrix)
{
    int i, j, col_offset;
    lass_matrix_t *mat_ptr;
    int cols, rows;

    mat_ptr = (lass_matrix_t *)matrix;
    cols = mat_ptr->n;
    rows = mat_ptr->m;

    for (col_offset = 0; col_offset < cols; col_offset += MAX_COL_WIDTH) {
        fprintf(fp, "Columns %d through %d\n",
                col_offset,
                col_offset + MAX_COL_WIDTH < cols ? col_offset + MAX_COL_WIDTH - 1 : cols - 1);
        for(i = 0; i < rows; i++){
            fprintf(fp, "[%d] ", i);
            for(j = col_offset; j < cols && j < col_offset + MAX_COL_WIDTH; j++){
                fprintf(fp, "% f ", lass_matrix_get(matrix, i, j));
            }
            fprintf(fp, "\n");
        }
    }
}

/* Computes the inverse matrix, storing the result in an already-allocated matrix of the correct size. Returns 0 on success, otherwise non-zero if e.g., the matrix is not invertible */
int
lass_matrix_inverse(void *result, void *matrix)
{
    lass_matrix_t *mat;
    lass_matrix_t *orig;
    lass_matrix_t *inv;
    int i, j, k;
    int m, n;
    double temp, temp2;
    double res;

    mat = (lass_matrix_t *)matrix;
    inv = (lass_matrix_t *)result;

    if (mat->m != mat->n || mat->m != inv->m || mat->n != inv->n) {
        fprintf(stderr, "Matrix dimension error in lass_matrix_inverse\n");
        return -1;
    }

    m = mat->m;
    n = mat->n;

    /*
     * NOTE:
     * Using Gauss-Jordan elimination, but implicitly forming
     * the augmented matrix between a copy of the given matrix and
     * the all-ready allocated result.
     */

    /* Copy original matrix */
    orig = lass_matrix_alloc(m, n);
    if (orig == NULL) {
        fprintf(stderr, "No memory for computing inverse\n");
        return -1;
    }
    orig->m = m;
    orig->n = n;
    memcpy(orig->data, mat->data, sizeof(double) * m * n);

    /* Start the result at the identity */
    memset(inv->data, 0, sizeof(double) * m * n);
    for (i = 0; i < m; i++) {
        lass_matrix_set(result, i, i, 1.0);
    }

    /* Normalize and eliminate along diagonal of copied original */
    for (i = 0; i < m; i++) {
        
        /* Find non-zero row on or below the diag */
        for (j = i; j < m; j++) {
            temp = lass_matrix_get(orig, j, i);
            if (temp != 0.0) {
                temp = 1.0 / temp;
                break;
            }
        }
        if (j == m) {
            fprintf(stderr, "Probably can't invert this matrix: column %d is zero below diag. Dumping the matrix to %s\n", i, SINGULAR_MATRIX_DUMP_FILE);
            FILE *fp = fopen(SINGULAR_MATRIX_DUMP_FILE, "w");
            for (i = 0; i < m; i++) {
                for (j = 0; j < m; j++) {
                    fprintf(fp, "%g ", lass_matrix_get(matrix, i, j));
                }
                fprintf(fp, "\n");
            }
            fclose(fp);
            return(-1);
        }

        /* Swap rows if entry i,i wasn't zero */
        if (j != i) {
            for (k = 0; k < n; k++) {
                temp2 = lass_matrix_get(orig, i, k);
                lass_matrix_set(orig, i, k, lass_matrix_get(orig, j, k));
                lass_matrix_set(orig, j, k, temp2);

                temp2 = lass_matrix_get(inv, i, k);
                lass_matrix_set(inv, i, k, lass_matrix_get(inv, j, k));
                lass_matrix_set(inv, j, k, temp2);
            }
        }

        /* Normalize row i */
        for (k = 0; k < n; k++) {
            if (k == i) {
                lass_matrix_set(orig, i, k, 1.0);
                res = zapsmall(lass_matrix_get(inv, i, k) * temp);
                lass_matrix_set(inv, i, k, res);
            } else {
                res = zapsmall(lass_matrix_get(orig, i, k) * temp);
                lass_matrix_set(orig, i, k, res);
                res = zapsmall(lass_matrix_get(inv, i, k) * temp);
                lass_matrix_set(inv, i, k, res);
            }
        }

        /* Eliminate other rows in column i */
        for (k = 0; k < m; k++) {
            temp = lass_matrix_get(orig, k, i);
            if (k != i && temp != 0.0) {
                for (j = 0; j < n; j++) {
                    res = zapsmall(lass_matrix_get(orig, k, j) - (temp * lass_matrix_get(orig, i, j)));
                    lass_matrix_set(orig, k, j, res);
                    res = zapsmall(lass_matrix_get(inv, k, j) - (temp * lass_matrix_get(inv, i, j)));
                    lass_matrix_set(inv, k, j, res);
                }
            }
        }
    }

    lass_matrix_free(orig);

    return 0;
}

/* Extract the colums specified by the index vector idx_vec */
int
lass_matrix_extract_vec(void *result, void *matrix, void *idx_vec)
{
    lass_matrix_t *res;
    lass_matrix_t *mat;
    lass_vec_t *idx;
    int i, j;

    res = (lass_matrix_t *)result;
    mat = (lass_matrix_t *)matrix;
    idx = (lass_vec_t *)idx_vec;

    if (res->n != idx->m || res->m != mat->m) {
        fprintf(stderr, "Bad dimensions in lass_matrix_extract_vec\n");
        return -1;
    }

    /* Make sure the elements of idx_vec are valid indices into the columns of matrix */
    for (i = 0; i < idx->m; i++) {
        if (idx->data[i] < 0 || idx->data[i] >= mat->n || floor(idx->data[i]) != idx->data[i]) {
            fprintf(stderr, "Bad entry in index vector in lass_matrix_extract_vec\n");
            return -1;
        }
    }

    /* Copy in the specified columns */
    for (i = 0; i < idx->m; i++) {
        for (j = 0; j < res->m; j++) {
            lass_matrix_set(result, j, i, lass_matrix_get(matrix, j, (int)idx->data[i]));
        }
    }

    return 0;
}


/*
 * Functions for dealing with vectors
 */

/* Allocate a new vector with m entries */
void *
lass_vec_alloc(int m)
{
    lass_vec_t *new;
    
    new = (lass_vec_t *)malloc(sizeof(lass_vec_t));
    if (new == NULL) {
        return NULL;
    }
    new->m = m;
    new->data = (double *)malloc(sizeof(double) * m);
    if (new->data == NULL) {
        free(new);
        return NULL;
    }
    memset(new->data, 0, sizeof(double) * m);

    return new;
}

/* Receives a matrix, and assigns random values to all entries. */
int
lass_mat_randomize(void *matrix, int min, int max){
    lass_matrix_t *mat;
    mat = (lass_matrix_t *) matrix;
    int m, n; 
    m = mat->m; 
    n = mat->n;
    uint64_t seed = ReadTSC();
    srand(seed);

    for(int i = 0; i < m; i++){
        for(int j = 0; j < n; j++){
            double num = (rand() % (max - min + 1)) + min;
            lass_matrix_set(mat, i, j, num);
        }
    }

    return 0;
}

// void
// lass_vec_randomize(void *vec, int min, int max){
//     return 0;
// }

/* Initialize the given vector from a normal c-style array */
int
lass_vec_init(void *vec, double *array, int m)
{
    lass_vec_t *vp;
    vp = (lass_vec_t *)vec;
    
    if (vp->m != m) {
        fprintf(stderr, "Bad dimensions in lass_vec_init\n");
        return -1;
    }

    memcpy(vp->data, array, sizeof(double) * m);

    return 0;
}

/* Free the given vector */
void
lass_vec_free(void *vec)
{
    lass_vec_t *vp;
    vp = (lass_vec_t *)vec;

    free(vp->data);
    free(vp);
}

/* Return the i-th entry of the given vector */
double
lass_vec_get(void *vec, int i)
{
    lass_vec_t *vp;
    vp = (lass_vec_t *)vec;

    if (i < 0 || i >= vp->m) {
        fprintf(stderr, "Bad index %d into vector of %d elements in lass_vec_get\n",
                i, vp->m);
        return 0.0;
    }
    return vp->data[i];
}

/* Sets the i-th entry of the given vector to value */
int
lass_vec_set(void *vec, int i, double value)
{
    lass_vec_t *vp;
    vp = (lass_vec_t *)vec;

    if (i < 0 || i >= vp->m) {
        fprintf(stderr, "Bad index %d into vector of %d elements in lass_vec_set\n",
                i, vp->m);
        return 0.0;
    }
    vp->data[i] = value;
}

/* Receives a matrix, and assigns random values to all entries. */
int
lass_vec_randomize(void *vec, int min, int max){
    lass_vec_t *vp;
    vp = (lass_vec_t *)vec;
    int m; 
    m = vp->m;
    uint64_t seed = ReadTSC();
    srand(seed);

    for(int i = 0; i < m; i++){
            double num = (rand() % (max - min + 1)) + min;
            lass_vec_set(vp, i, num);
    }

    return 0;
}

/* Dumps a textual representation of the vector on the given FILE * */
void
lass_vec_dump(FILE *fp, void *vec)
{
    lass_vec_t *vp;
    int i;

    vp = (lass_vec_t *)vec;

    for (i = 0; i < vp->m; i++) {
        fprintf(fp, "[%d] %f\n", i, vp->data[i]);
    }
}

/* Returns a vector which contains the elements specified by the index vector idx_vec */
int
lass_vec_extract_vec(void *result, void *vec, void *idx_vec)
{
    lass_vec_t *res;
    lass_vec_t *v;
    lass_vec_t *idx;
    int i;

    res = (lass_vec_t *)result;
    v = (lass_vec_t *)vec;
    idx = (lass_vec_t *)idx_vec;

    if (res->m != idx->m) {
        fprintf(stderr, "Bad dimensions in lass_vec_extract_vec\n");
        return -1;
    }

    /* Verify all entries of idx_vec are indices into vec */
    for (i = 0; i < idx->m; i++) {
        if (idx->data[i] < 0 || idx->data[i] >= v->m || floor(idx->data[i]) != idx->data[i]) {
            fprintf(stderr, "Bad entry in index vector in lass_vec_extract_vec\n"); 
            return -1;
        }
    }

    /* Copy entries */
    for (i = 0; i < idx->m; i++) {
        res->data[i] = v->data[(int)idx->data[i]];
    }

    return 0;
}


/*
 * Operations
 * NOTE: these functions should all return 0 if the dimensions of their arguments make sense,
 * otherwise they should return non-zero to indicate an error.
 *
 * NOTE: result is assumed to be a matrix or vector allocated with the correct dimensions.
 */

/* Scalar value times a vector */
int
lass_scalar_vec_mult(void *result, double value, void *vec)
{
    lass_vec_t *vp;
    lass_vec_t *res;
    int i;

    vp = (lass_vec_t *)vec;
    res = (lass_vec_t *)result;
    if (vp->m != res->m) {
        fprintf(stderr, "Bad dimensions in lass_scalar_vec_mult\n");
        return -1;
    }

    for (i = 0; i < vp->m; i++) {
        res->data[i] = vp->data[i] * value;
    }
    return 0;
}

/* Element-wise subtraction */
int
lass_vec_vec_subtract(void *result, void *vec1, void *vec2)
{
    lass_vec_t *v1;
    lass_vec_t *v2;
    lass_vec_t *res;
    int i;

    v1 = (lass_vec_t *)vec1;
    v2 = (lass_vec_t *)vec2;
    res = (lass_vec_t *)result;
    if (v1->m != v2->m || v2->m != res->m) {
        fprintf(stderr, "Bad dimensions in lass_vec_vec_subtract\n");
        return -1;
    }

    for (i = 0; i < v1->m; i++) {
        res->data[i] = zapsmall(v1->data[i] - v2->data[i]);
    }
    return 0;
}

/* Dot-product of two vectors */
int
lass_vec_dot_prod(double *result, void *vec1, void *vec2)
{
    lass_vec_t *v1;
    lass_vec_t *v2;
    double acc;
    int i;

    v1 = (lass_vec_t *)vec1;
    v2 = (lass_vec_t *)vec2;
    acc = 0.0;
    if (v1->m != v2->m) {
        fprintf(stderr, "Bad dimensions in lass_vec_dot_prod\n");
        return -1;
    }

    for (i = 0; i < v1->m; i++) {
        acc += v1->data[i] * v2->data[i];
    }

    *result = acc;
    return 0;
}



/* Matrix-vector multiplication */
int
lass_matrix_vec_mult(void *result, void *matrix, void *vector)
{
    lass_vec_t *v;
    lass_matrix_t *mat;
    lass_matrix_t *res;
    double acc;
    int i, j;

    v = (lass_vec_t *)vector;
    mat = (lass_matrix_t *)matrix;
    res = (lass_matrix_t *)result;
    if (mat->n != v->m || mat->m != res->m) {
        fprintf(stderr, "Bad dimensions in lass_matrix_vec_mult\n");
        return -1;
    }

    for (i = 0; i < mat->m; i++) {
        acc = 0;
        for (j = 0; j < mat->n; j++) {
            acc += v->data[j] * lass_matrix_get(matrix, i, j);
        }
        res->data[i] = acc;
    }
    return 0;
}

/* Vector-matrix multiplication */
int
lass_vec_matrix_mult(void *result, void *vec, void *matrix)
{
    lass_vec_t *v;
    lass_matrix_t *mat;
    lass_matrix_t *res;
    double acc;
    int i, j;

    v = (lass_vec_t *)vec;
    mat = (lass_matrix_t *)matrix;
    res = (lass_matrix_t *)result;
    if (mat->m != v->m || mat->n != res->m) {
        fprintf(stderr, "Bad dimensions in lass_vec_matrix_mult\n");
        return -1;
    }

    for (i = 0; i < mat->n; i++) {
        acc = 0;
        for (j = 0; j < mat->m; j++) {
            acc += v->data[j] * lass_matrix_get(matrix, j, i);
        }
        res->data[i] = acc;
    }
    return 0;
}

/* Matrix-matrix multiplication */
int
lass_matrix_matrix_mult(void *result, void *matrix1, void *matrix2)
{
    lass_matrix_t *res;
    lass_matrix_t *mat1;
    lass_matrix_t *mat2;
    int i, j, k;
    double acc;

    res = (lass_matrix_t *)result;
    mat1 = (lass_matrix_t *)matrix1;
    mat2 = (lass_matrix_t *)matrix2;

    if (mat1->n != mat2->m || res->m != mat1->m || res->n != mat2->n) {
        fprintf(stderr, "Bad dimensions in lass_matrix_matrix_mult\n");
        return -1;
    }

    for (i = 0; i < mat2->n; i++) {
        for (j = 0; j < mat1->m; j++) {
            acc = 0.0;
            for (k = 0; k < mat2->m; k++) {
                acc += lass_matrix_get(mat1, j, k) * lass_matrix_get(mat2, k, i);
            }
            lass_matrix_set(res, j, i, acc);
        }
    }
    return 0;
}

/*
 * Extra linear algebra operations needed for revised simplex
 */

/* Matrix column sum if less than zero: each entry in result_vec is the sum of the corresponding column in matrix only taking into consideration rows where the corresponding entry in index_vec is less than zero */
int lass_matrix_colsum_ltz(void *result_vec, void *matrix, void *index_vec)
{
    lass_vec_t *res;
    lass_matrix_t *mat;
    lass_vec_t *vec;
    double sum;
    int i, j;

    res = (lass_vec_t *)result_vec;
    mat = (lass_matrix_t *)matrix;
    vec = (lass_vec_t *)index_vec;

    if (mat->m != vec->m || res->m != mat->n) {
        fprintf(stderr, "Bad dimensions in lass_matrix_colsum_ltz\n");
        return -1;
    }

    /* For each column */
    for (i = 0; i < mat->n; i++) {
        sum = 0.0;
        for (j = 0; j < mat->m; j++) {
            if (vec->data[j] < 0.0) {
                sum += lass_matrix_get(matrix, j, i);
            }
        }
        res->data[i] = sum;
    }

    return 0;
}

/* Returns both value and index of the minimum element of the given vector. Pass null if you don't care about min value or index in particular. */
void lass_vec_min(double *min_val, int *min_idx, void *vector)
{
    lass_vec_t *v;
    double min;
    int min_i;
    int i;

    v = (lass_vec_t *)vector;
    min = INFINITY;
    min_i = -1;
    for (i = 0; i < v->m; i++) {
        if (v->data[i] < min) {
            min = v->data[i];
            min_i = i;
        }
    }

    if (min_val != NULL) {
        *min_val = min;
    }
    if (min_idx != NULL) {
        *min_idx = min_i;
    }
}

/* Minimum ratio test for phase 1: return min {beta_i / alphaq_i such that (beta_i < 0 and alphaq_i < 0) or (beta_i >= 0 && alphaq_i > 0)} */
int
lass_phase1_min_ratio(double *theta, int *p, void *beta, void *alphaq)
{
    lass_vec_t *v1;
    lass_vec_t *v2;
    double betai;
    double alphaqi;
    double quot;
    double min;
    int min_i;
    int i;

    v1 = (lass_vec_t *)beta;
    v2 = (lass_vec_t *)alphaq;
    if (v1->m != v2->m) {
        fprintf(stderr, "Bad dimensions in lass_phase1_min_ratio\n");
        return -1;
    }
    
    min = INFINITY;
    min_i = -1;
    for (i = 0; i < v1->m; i++) {
        betai = zapsmall(v1->data[i]);
        alphaqi = zapsmall(v2->data[i]);
        if ((betai < 0 && alphaqi < 0) || (betai >= 0 && alphaqi > 0)) {
            quot = betai / alphaqi;
            if (quot < min) {
                min = quot;
                min_i = i;
            }
        }
    }
    *theta = min;
    *p = min_i;
}

/* Minimum ratio test for phase 2: return min {beta_i / alphaq_i such that alphaq_i > 0} */
int
lass_phase2_min_ratio(double *theta, int *p, void *beta, void *alphaq)
{
    lass_vec_t *v1;
    lass_vec_t *v2;
    double betai;
    double alphaqi;
    double quot;
    double min;
    int min_i;
    int i;

    v1 = (lass_vec_t *)beta;
    v2 = (lass_vec_t *)alphaq;
    if (v1->m != v2->m) {
        fprintf(stderr, "Bad dimensions in lass_phase1_min_ratio\n");
        return -1;
    }
    
    min = INFINITY;
    min_i = -1;
    for (i = 0; i < v1->m; i++) {
        betai = zapsmall(v1->data[i]);
        alphaqi = zapsmall(v2->data[i]);
        if (alphaqi > 0) {
            quot = betai / alphaqi;
            if (quot < min) {
                min = quot;
                min_i = i;
            }
        }
    }
    *theta = min;
    *p = min_i;
}

/* Populates the pointers in the ops struct to use these matrix and vector functions */
void
lass_set_ops(struct la_ops *ops)
{
    ops->ensure_temps = NULL;
    
    ops->matrix_alloc = lass_matrix_alloc;
    ops->matrix_init = lass_matrix_init;
    ops->matrix_free = lass_matrix_free;
    ops->matrix_get = lass_matrix_get;
    ops->matrix_get_col = lass_matrix_get_col;
    ops->matrix_set = lass_matrix_set;
    ops->matrix_set_col = lass_matrix_set_col;
    ops->matrix_inverse = lass_matrix_inverse;
    ops->matrix_extract_vec = lass_matrix_extract_vec;
    ops->matrix_dump = lass_matrix_dump;
    ops->matrix_randomize = lass_mat_randomize; 

    ops->vec_alloc = lass_vec_alloc;
    ops->vec_init = lass_vec_init;
    ops->vec_free = lass_vec_free;
    ops->vec_get = lass_vec_get;
    ops->vec_set = lass_vec_set;
    ops->vec_dump = lass_vec_dump;
    ops->vec_extract_vec = lass_vec_extract_vec; 
    ops->vec_randomize = lass_vec_randomize;

    ops->scalar_vec_mult = lass_scalar_vec_mult;
    ops->vec_vec_subtract = lass_vec_vec_subtract;
    ops->vec_dot_prod = lass_vec_dot_prod;
    ops->matrix_vec_mult = lass_matrix_vec_mult;
    ops->vec_matrix_mult = lass_vec_matrix_mult;
    ops->matrix_matrix_mult = lass_matrix_matrix_mult;

    ops->matrix_colsum_ltz = lass_matrix_colsum_ltz;
    ops->vec_min = lass_vec_min;
    ops->phase1_min_ratio = lass_phase1_min_ratio;
    ops->phase2_min_ratio = lass_phase2_min_ratio;
}
