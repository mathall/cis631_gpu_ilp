/*
 * CIS 631, Fall 2019
 *
 * Test executable for MPS reading function
 *
 * Authors: Chris Misa
 */

#include <stdio.h>

#include <data.h>
#include <read_mps.h>
#include <common.h>

int
main(int argc, char **argv)
{
    int rv;
    double **A;
    double *b;
    double *c;
    int m;
    int n;
    enum problem_type prob_type;

    if (argc != 2) {
        fprintf(stderr, "Usage: %s <file name>\n", argv[0]);
        return 1;
    }

    rv = read_mps(argv[1], &A, &b, &c, &m, &n, &prob_type);

    printf("Read LP with %d variables and %d constraints\n", n, m);

    printf("============ Constraint matrix: ============\n");
    printMatrix(A, m, n);

    printf("============ RHS vector: ===================\n");
    printArray(b, m);

    printf("============ Objective function: ===========\n");
    printArray(c, n);

    return 0;
}
