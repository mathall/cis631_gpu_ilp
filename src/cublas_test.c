/*
 * CIS 631, Fall 2019
 *
 * Run sanity test on the cuda ops
 *
 * Authors: Chris Misa
 */

#include <la.h>
#include <la_cublas.h>
#include <la_test.h>

int
main(void)
{
    struct la_ops ops;
    
    lacb_set_ops(&ops);

    printf("* * * * * * * * * * Starting la_cublas sanity check * * * * * * * * * *\n");
    la_test(&ops);
    printf("* * * * * * * * * * Finished la_cublas sanity check * * * * * * * * * *\n");
    
    return 0;
}
