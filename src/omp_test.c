/*
 * CIS 631, Fall 2019
 *
 * Run sanity test on the openMP ops
 *
 * Authors: Matt Hall, Chris Misa
 */

#include <la.h>
#include <la_openMP.h>
#include <la_test.h>
#include <omp.h>

int
main(void)
{
    struct la_ops ops;
    
    laomp_set_ops(&ops);

    printf("* * * * * * * * * * Starting la_openMP sanity check * * * * * * * * * *\n");
    la_test(&ops);
    printf("* * * * * * * * * * Finished la_openMP sanity check * * * * * * * * * *\n");

    // omp_set_num_threads(16);
    printf("Num Threads: %d\n", omp_get_num_threads());
    int omp_get_thread_num();
    # pragma omp parallel
    {
    printf("Thread rank: %d\n", omp_get_thread_num());
    }
    return 0;
}
