/*
 * CIS 631, Fall 2019
 *
 * OpenMP linear algebra operations using C-style storage
 * 
 * Authors: Chris Misa, Matt Hall
 *
 * NOTES: This implementation is meant to be extra verbose so we can debug the algorithms that use
 * these functions. Other algorithms may skip certain steps (e.g., bounds checking) in favor of performance.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <omp.h>

#include <la_openMP.h>
#include <common.h>

#define MAX_COL_WIDTH 20
#define SINGULAR_MATRIX_DUMP_FILE "singular_matrix"

/* Allocate a new matrix with m rows and n columns. Sets all entries to 0.0 */
void *
laomp_matrix_alloc(int m, int n)
{
    laomp_matrix_t *new;

    new = (laomp_matrix_t *)AlignedMalloc(sizeof(laomp_matrix_t));
    if (new == NULL) {
        return NULL;
    }

    new->m = m;
    new->n = n;
    new->data = (double *)AlignedMalloc(sizeof(double) * m * n);
    if (new->data == NULL) {
        free(new);
        return NULL;
    }
    memset(new->data, 0, sizeof(double) * m * n);

    return new;
}

/* Initialize the given, allocated matrix with values from a normal c-style two-dimensional array, returns 0 on success, otherwise non-zero */
int
laomp_matrix_init(void *matrix, double **array, int m, int n)
{
    int i;
    laomp_matrix_t *mat_ptr;
    mat_ptr = (laomp_matrix_t *)matrix;

    if (mat_ptr->m != m || mat_ptr->n != n) {
        fprintf(stderr, "Bad dimensions in laomp_matrix_init\n");
        return -1;
    }
    for (i = 0; i < m; i++) {
        memcpy(mat_ptr->data + (i * n), array[i], n * sizeof(double));
    }
    return 0;
}

/* Free memory held by matrix */
void
laomp_matrix_free(void *matrix)
{
    laomp_matrix_t *mat_ptr;
    mat_ptr = (laomp_matrix_t *)matrix;

    free(mat_ptr->data);
    free(mat_ptr);
}

/* Returns the entry at row i, column j */
double
laomp_matrix_get(void *matrix, int i, int j)
{
    laomp_matrix_t *mat_ptr;
    mat_ptr = (laomp_matrix_t *)matrix;

    if (i < 0 || i >= mat_ptr->m) {
        fprintf(stderr, "Bad row index %d in laomp_matrix_get\n", i);
        return INFINITY;
    }
    if (j < 0 || j >= mat_ptr->n) {
        fprintf(stderr, "Bad column index %d in laomp_matrix_get\n", j);
        return INFINITY;
    }
    
    return mat_ptr->data[(i * mat_ptr->n) + j];
}

/*
#define laomp_matrix_get(...) laomp_matrix_get_special(__func__, __VA_ARGS__)
double 
laomp_matrix_get_special(char const * caller_name, void *matrix, int i, int j)
{
    double val = laomp_matrix_get(matrix, i, j);
    if(val < INFINITY )
        return val; 
    else    
        printf("laomp_matrix_get was called from %s\n", caller_name);
        exit(-1); 
}
*/

/* Returns a vector representation of column j owned by the caller, assuming result is an already-allocated vector of the correct length. Returns 0 on success, otherwise non-zero */
int
laomp_matrix_get_col(void *result, void *matrix, int j)
{
    laomp_matrix_t *mat_ptr;
    laomp_vec_t *vec_ptr;

    mat_ptr = (laomp_matrix_t *)matrix;
    vec_ptr = (laomp_vec_t *)result;

    if (vec_ptr->m != mat_ptr->m) {
        fprintf(stderr, "Bad dimensions in laomp_matrix_get_col\n");
        return -1;
    }

    #pragma omp parallel for default(none) shared(vec_ptr, mat_ptr, j) 
    for (int i = 0; i < mat_ptr->m; i++) {
        vec_ptr->data[i] = mat_ptr->data[(i * mat_ptr->n) + j];
    }

    return 0;
}

/* Sets the value at row i, column j to value. Returns 0 on success, non-zero on error */
int
laomp_matrix_set(void *matrix, int i, int j, double value)
{
    laomp_matrix_t *mat_ptr;
    mat_ptr = (laomp_matrix_t *)matrix;

    if (i < 0 || i >= mat_ptr->m) {
        fprintf(stderr, "Bad row index %d in laomp_matrix_set, called by %s\n", i);
        return (-1);
    }
    if (j < 0 || j >= mat_ptr->n) {
        fprintf(stderr, "Bad column index %d in laomp_matrix_set called by %s\n", i);
        return (-1);
    }
    
    mat_ptr->data[(i * mat_ptr->n) + j] = value;
    return 0;
}





/*
int laomp_matrix_set_special(char const * caller_name, void *matrix, int i, int j, double value )
{
    if (laomp_matrix_set(matrix, i, j, value) != 0){
        printf( "laomp_matrix_set was called from %s\n", caller_name );
        printf("all hope is lost.");
        exit(-1);
    }
    else return 0;
}

#define laomp_matrix_set(...) laomp_matrix_set_special(__func__, __VA_ARGS__)
*/

/* Sets the entries in the column j to the values given in vector */
int laomp_matrix_set_col(void *matrix, int j, void *vector)
{
    laomp_matrix_t *mat;
    laomp_vec_t *vec;
    int i;
    
    mat = (laomp_matrix_t *)matrix;
    vec = (laomp_vec_t *)vector;
    
    if (vec->m != mat->m) {
        fprintf(stderr, "Bad dimentions in laomp_matrix_set_col\n");
        return -1;
    }
    #pragma omp parallel for default(none) shared(vec, mat, j) 
    for (i = 0; i < vec->m; i++) {
        mat->data[(i * mat->n) + j] = vec->data[i];
    }

    return 0;
}

/* dumps a textual representation of the matrix on the given file * */
void
laomp_matrix_dump(FILE *fp, void *matrix)
{
    int i, j, col_offset;
    laomp_matrix_t *mat_ptr;
    int cols, rows;

    mat_ptr = (laomp_matrix_t *)matrix;
    cols = mat_ptr->n;
    rows = mat_ptr->m;

    for (col_offset = 0; col_offset < cols; col_offset += MAX_COL_WIDTH) {
        fprintf(fp, "Columns %d through %d\n",
                col_offset,
                col_offset + MAX_COL_WIDTH < cols ? col_offset + MAX_COL_WIDTH - 1 : cols - 1);
        for(i = 0; i < rows; i++){
            fprintf(fp, "[%d] ", i);
            for(j = col_offset; j < cols && j < col_offset + MAX_COL_WIDTH; j++){
                fprintf(fp, "% f ", laomp_matrix_get(matrix, i, j));
            }
            fprintf(fp, "\n");
        }
    }
}

/* Computes the inverse matrix, storing the result in an already-allocated matrix of the correct size. Returns 0 on success, otherwise non-zero if e.g., the matrix is not invertible */
int
laomp_matrix_inverse(void *result, void *matrix)
{
    laomp_matrix_t *mat;
    laomp_matrix_t *orig;
    laomp_matrix_t *inv;
    int i, j, k;
    int m, n;
    double temp, temp2;
    double res;
    int bad_call = 0;
    mat = (laomp_matrix_t *)matrix;
    inv = (laomp_matrix_t *)result;

    if (mat->m != mat->n || mat->m != inv->m || mat->n != inv->n) {
        fprintf(stderr, "Matrix dimension error in laomp_matrix_inverse\n");
        return -1;
    }

    m = mat->m;
    n = mat->n;

    /*
     * NOTE:
     * Using Gauss-Jordan elimination, but implicitly forming
     * the augmented matrix between a copy of the given matrix and
     * the all-ready allocated result.
     */

    /* Copy original matrix */
    orig = laomp_matrix_alloc(m, n);
    if (orig == NULL) {
        fprintf(stderr, "No memory for computing inverse\n");
        return -1;
    }
    orig->m = m;
    orig->n = n;
    memcpy(orig->data, mat->data, sizeof(double) * m * n);

    /* Start the result at the identity */
    memset(inv->data, 0, sizeof(double) * m * n);
    #pragma omp parallel for default(none) shared(inv, m) private(i) 
    for (i = 0; i < m; i++) {
        inv->data[(i * inv->n) + i] = 1.0; 
        // laomp_matrix_set(result, i, i, 1.0);
    }

    /* Normalize and eliminate along diagonal of copied original */
    // #pragma omp parallel for default(none) shared(m, n, orig, inv, mat, stderr, bad_call) private(i, j, k, res, temp, temp2) 
    for (i = 0; i < m; i++) {
        /* Find non-zero row on or below the diag */
        if(bad_call) continue;
        for (j = i; j < m; j++) {
            temp = orig->data[(j*orig->n)+i];
            if (temp != 0.0) {
                temp = 1.0 / temp;
                break;
            }
        }
        if (j == m) {
            fprintf(stderr, "Thread %d: Probably can't invert this matrix: column %d is zero below diag. Dumping the matrix to %s\n", (int)(omp_get_thread_num()), i, SINGULAR_MATRIX_DUMP_FILE);
            FILE *fp = fopen(SINGULAR_MATRIX_DUMP_FILE, "w");
            for (i = 0; i < m; i++) {
                for (j = 0; j < m; j++) {
                    fprintf(fp, "%g ", laomp_matrix_get(mat, i, j));
                }
                fprintf(fp, "\n");
            }
            fclose(fp);

            #pragma omp atomic write
            bad_call = 1;
            
        }
        if(bad_call) continue;
        /* Swap rows if entry i,i wasn't zero */
        if (j != i) {
            //#pragma omp parallel for default(none) shared(orig, inv, n, j, k) private(temp2, i)
            for (k = 0; k < n; k++) {
                temp2 = laomp_matrix_get(orig, i, k);
                
                laomp_matrix_set(orig, i, k, laomp_matrix_get(orig, j, k));
                laomp_matrix_set(orig, j, k, temp2);

                temp2 = laomp_matrix_get(inv, i, k);
                laomp_matrix_set(inv, i, k, laomp_matrix_get(inv, j, k));
                laomp_matrix_set(inv, j, k, temp2);
            }
        }

        /* Normalize row i */
        #pragma omp parallel for default(none) shared(orig, inv, n, temp, i) private(res, temp2, k, j)
        for (k = 0; k < n; k++) {
            if (k == i) {
                laomp_matrix_set(orig, i, k, 1.0);
                res = zapsmall(laomp_matrix_get(inv, i, k) * temp);
                laomp_matrix_set(inv, i, k, res);
            } else {
                res = zapsmall(laomp_matrix_get(orig, i, k) * temp);
                laomp_matrix_set(orig, i, k, res);
                res = zapsmall(laomp_matrix_get(inv, i, k) * temp);
                laomp_matrix_set(inv, i, k, res);
            }
        }

        /* Eliminate other rows in column i */
        #pragma omp parallel for default(none) shared(orig, inv, i, n, m, k) private(temp, res, temp2, j)
        for (k = 0; k < m; k++) {
            temp = laomp_matrix_get(orig, k, i);
            if (k != i && temp != 0.0) {
                #pragma omp parallel for default(none) shared(orig, inv,  temp, i, n, k) private(res, temp2, j)
                for (j = 0; j < n; j++) {
                    res = zapsmall(laomp_matrix_get(orig, k, j) - (temp * laomp_matrix_get(orig, i, j)));
                    laomp_matrix_set(orig, k, j, res);
                    res = zapsmall(laomp_matrix_get(inv, k, j) - (temp * laomp_matrix_get(inv, i, j)));
                    laomp_matrix_set(inv, k, j, res);
                }
            }
        }
    }
    if (bad_call)
        return -1;
    laomp_matrix_free(orig);

    return 0;
}

/* Extract the colums specified by the index vector idx_vec */
int
laomp_matrix_extract_vec(void *result, void *matrix, void *idx_vec)
{
    laomp_matrix_t *res;
    laomp_matrix_t *mat;
    laomp_vec_t *idx;
    int i, j, m, n;

    res = (laomp_matrix_t *)result;
    mat = (laomp_matrix_t *)matrix;
    idx = (laomp_vec_t *)idx_vec;
    
    if (res->n != idx->m || res->m != mat->m) {
        fprintf(stderr, "Bad dimensions in laomp_matrix_extract_vec\n");
        return -1;
    }

    /* Copy in the specified columns */
    #pragma omp parallel for default(none) private(i) shared(idx, mat, res)
    for (i = 0; i < idx->m; i++) {
        #pragma omp parallel for default(none) private(j) shared(idx, mat, res, i)
        for (j = 0; j < res->m; j++) {
            laomp_matrix_set(res, j, i, laomp_matrix_get(mat, j, (int)idx->data[i]));
        }
    }

    return 0;
}
 

/*
 * Functions for dealing with vectors
 */

/* Allocate a new vector with m entries */
void *
laomp_vec_alloc(int m)
{
    laomp_vec_t *new;
    
    new = (laomp_vec_t *)AlignedMalloc(sizeof(laomp_vec_t));
    if (new == NULL) {
        return NULL;
    }
    new->m = m;
    new->data = (double *)AlignedMalloc(sizeof(double) * m);
    if (new->data == NULL) {
        free(new);
        return NULL;
    }
    memset(new->data, 0, sizeof(double) * m);

    return new;
}

/* Receives a matrix, and assigns random values to all entries. */
int
laomp_mat_randomize(void *matrix, int min, int max){
    laomp_matrix_t *mat;
    mat = (laomp_matrix_t *) matrix;
    int m, n; 
    m = mat->m; 
    n = mat->n;
    uint64_t seed = ReadTSC();
    srand(seed);

    for(int i = 0; i < m; i++){
        for(int j = 0; j < n; j++){
            double num = (rand() % (max - min + 1)) + min;
            laomp_matrix_set(mat, i, j, num);
        }
    }

    return 0;
}

// void
// laomp_vec_randomize(void *vec, int min, int max){
//     return 0;
// }

/* Initialize the given vector from a normal c-style array */
int
laomp_vec_init(void *vec, double *array, int m)
{
    laomp_vec_t *vp;
    vp = (laomp_vec_t *)vec;
    
    if (vp->m != m) {
        fprintf(stderr, "Bad dimensions in laomp_vec_init\n");
        return -1;
    }

    memcpy(vp->data, array, sizeof(double) * m);

    return 0;
}

/* Free the given vector */
void
laomp_vec_free(void *vec)
{
    laomp_vec_t *vp;
    vp = (laomp_vec_t *)vec;

    free(vp->data);
    free(vp);
}

/* Return the i-th entry of the given vector */
double
laomp_vec_get(void *vec, int i)
{
    laomp_vec_t *vp;
    vp = (laomp_vec_t *)vec;

    if (i < 0 || i >= vp->m) {
        fprintf(stderr, "Bad index %d into vector of %d elements in laomp_vec_get\n",
                i, vp->m);
        return 0.0;
    }
    return vp->data[i];
}

/* Sets the i-th entry of the given vector to value */
int
laomp_vec_set(void *vec, int i, double value)
{
    laomp_vec_t *vp;
    vp = (laomp_vec_t *)vec;

    if (i < 0 || i >= vp->m) {
        fprintf(stderr, "Bad index %d into vector of %d elements in laomp_vec_set\n",
                i, vp->m);
        return 0.0;
    }
    vp->data[i] = value;
}

/* Receives a matrix, and assigns random values to all entries. */
int
laomp_vec_randomize(void *vec, int min, int max){
    laomp_vec_t *vp;
    vp = (laomp_vec_t *)vec;
    int m; 
    m = vp->m;
    uint64_t seed = ReadTSC();
    srand(seed);

    for(int i = 0; i < m; i++){
            double num = (rand() % (max - min + 1)) + min;
            laomp_vec_set(vp, i, num);
    }

    return 0;
}

/* Dumps a textual representation of the vector on the given FILE * */
void
laomp_vec_dump(FILE *fp, void *vec)
{
    laomp_vec_t *vp;
    int i;

    vp = (laomp_vec_t *)vec;

    for (i = 0; i < vp->m; i++) {
        fprintf(fp, "[%d] %f\n", i, vp->data[i]);
    }
}

/* Returns a vector which contains the elements specified by the index vector idx_vec */
int
laomp_vec_extract_vec(void *result, void *vec, void *idx_vec)
{
    laomp_vec_t *res;
    laomp_vec_t *v;
    laomp_vec_t *idx;
    int i;

    res = (laomp_vec_t *)result;
    v = (laomp_vec_t *)vec;
    idx = (laomp_vec_t *)idx_vec;

    if (res->m != idx->m) {
        fprintf(stderr, "Bad dimensions in laomp_vec_extract_vec\n");
        return -1;
    }

    /* Verify all entries of idx_vec are indices into vec */
    for (i = 0; i < idx->m; i++) {
        if (idx->data[i] < 0 || idx->data[i] >= v->m || floor(idx->data[i]) != idx->data[i]) {
            fprintf(stderr, "Bad entry in index vector in laomp_vec_extract_vec\n"); 
            return -1;
        }
    }

    /* Copy entries */
    for (i = 0; i < idx->m; i++) {
        res->data[i] = v->data[(int)idx->data[i]];
    }

    return 0;
}


/*
 * Operations
 * NOTE: these functions should all return 0 if the dimensions of their arguments make sense,
 * otherwise they should return non-zero to indicate an error.
 *
 * NOTE: result is assumed to be a matrix or vector allocated with the correct dimensions.
 */

/* Scalar value times a vector */
int
laomp_scalar_vec_mult(void *result, double value, void *vec)
{
    laomp_vec_t *vp;
    laomp_vec_t *res;
    int i;

    vp = (laomp_vec_t *)vec;
    res = (laomp_vec_t *)result;
    if (vp->m != res->m) {
        fprintf(stderr, "Bad dimensions in laomp_scalar_vec_mult\n");
        return -1;
    }

    #pragma omp parallel for default(none) private(i) shared(res, vp, value)
    for (i = 0; i < vp->m; i++) {
        res->data[i] = vp->data[i] * value;
    }
    return 0;
}

/* Element-wise subtraction */
int
laomp_vec_vec_subtract(void *result, void *vec1, void *vec2)
{
    laomp_vec_t *v1;
    laomp_vec_t *v2;
    laomp_vec_t *res;
    int i;

    v1 = (laomp_vec_t *)vec1;
    v2 = (laomp_vec_t *)vec2;
    res = (laomp_vec_t *)result;
    if (v1->m != v2->m || v2->m != res->m) {
        fprintf(stderr, "Bad dimensions in laomp_vec_vec_subtract\n");
        return -1;
    }

    #pragma omp parallel for default(none) private(i) shared(res, v1, v2)
    for (i = 0; i < v1->m; i++) {
        res->data[i] = zapsmall(v1->data[i] - v2->data[i]);
    }
    return 0;
}

/* Dot-product of two vectors */
int
laomp_vec_dot_prod(double *result, void *vec1, void *vec2)
{
    laomp_vec_t *v1;
    laomp_vec_t *v2;
    double acc;
    int i;

    v1 = (laomp_vec_t *)vec1;
    v2 = (laomp_vec_t *)vec2;
    acc = 0.0;
    if (v1->m != v2->m) {
        fprintf(stderr, "Bad dimensions in laomp_vec_dot_prod\n");
        return -1;
    }

    #pragma omp parallel for default(none) private(i) shared(v1, v2) reduction(+:acc)
    for (i = 0; i < v1->m; i++) {
        acc += v1->data[i] * v2->data[i];
    }

    *result = acc;
    return 0;
}



/* Matrix-vector multiplication */
int
laomp_matrix_vec_mult(void *result, void *matrix, void *vector)
{
    laomp_vec_t *v;
    laomp_matrix_t *mat;
    laomp_matrix_t *res;
    double acc;
    
    int i, j;

    v = (laomp_vec_t *)vector;
    mat = (laomp_matrix_t *)matrix;
    res = (laomp_matrix_t *)result;
    if (mat->n != v->m || mat->m != res->m) {
        fprintf(stderr, "Bad dimensions in laomp_matrix_vec_mult\n");
        return -1;
    }

    for (i = 0; i < mat->m; i++) {
        acc = 0;
        //#pragma omp parallel for default(none) private(j) shared(i, mat, v) reduction(+:acc)
        for (j = 0; j < mat->n; j++) {
            acc += v->data[j] * mat->data[(i*mat->n)+j];
        }
        res->data[i] = acc;
    }
    return 0;
}

/* Vector-matrix multiplication */
int
laomp_vec_matrix_mult(void *result, void *vec, void *matrix)
{
    laomp_vec_t *v;
    laomp_matrix_t *mat;
    laomp_matrix_t *res;
    double acc;
    int i, j;

    v = (laomp_vec_t *)vec;
    mat = (laomp_matrix_t *)matrix;
    res = (laomp_matrix_t *)result;
    if (mat->m != v->m || mat->n != res->m) {
        fprintf(stderr, "Bad dimensions in laomp_vec_matrix_mult\n");
        return -1;
    }

    for (i = 0; i < mat->n; i++) {
        acc = 0;
        #pragma omp parallel for default(none) private(j) shared(i, mat, v) reduction(+:acc)
        for (j = 0; j < mat->m; j++) {
            acc += v->data[j] * mat->data[(j*mat->n)+i];
        }
        res->data[i] = acc;
    }
    return 0;
}

/* Matrix-matrix multiplication */
int
laomp_matrix_matrix_mult(void *result, void *matrix1, void *matrix2)
{
    laomp_matrix_t *res;
    laomp_matrix_t *mat1;
    laomp_matrix_t *mat2;
    int i, j, k;
    double acc;

    res = (laomp_matrix_t *)result;
    mat1 = (laomp_matrix_t *)matrix1;
    mat2 = (laomp_matrix_t *)matrix2;

    if (mat1->n != mat2->m || res->m != mat1->m || res->n != mat2->n) {
        fprintf(stderr, "Bad dimensions in laomp_matrix_matrix_mult\n");
        return -1;
    }

    for (i = 0; i < mat2->n; i++) {
        for (j = 0; j < mat1->m; j++) {
            acc = 0.0;
            // #pragma omp parallel for default(none) private(j) shared(i, k, mat1, mat2, res) reduction(+:acc)
            for (k = 0; k < mat2->m; k++) {            
                // printf("i = %d, j = %d, k = %d\n", i, j, k);
                acc += laomp_matrix_get(mat1, j, k) * laomp_matrix_get(mat2, k, i);
                // acc += mat1->data[j*mat1->n, k] * mat2->data[(k*mat2->n)+i];
            }
        // res->data[(j*res->n)+i] = acc;
        laomp_matrix_set(res, j, i, acc);
        }
    }
    return 0;
}

/*
 * Extra linear algebra operations needed for revised simplex
 */

/* Matrix column sum if less than zero: each entry in result_vec is the sum of the corresponding column in matrix only taking into consideration rows where the corresponding entry in index_vec is less than zero */
int laomp_matrix_colsum_ltz(void *result_vec, void *matrix, void *index_vec)
{
    laomp_vec_t *res;
    laomp_matrix_t *mat;
    laomp_vec_t *vec;
    double sum;
    int i, j;

    res = (laomp_vec_t *)result_vec;
    mat = (laomp_matrix_t *)matrix;
    vec = (laomp_vec_t *)index_vec;

    if (mat->m != vec->m || res->m != mat->n) {
        fprintf(stderr, "Bad dimensions in laomp_matrix_colsum_ltz\n");
        return -1;
    }

    /* For each column */
    for (i = 0; i < mat->n; i++) {
        sum = 0.0;
        for (j = 0; j < mat->m; j++) {
            if (vec->data[j] < 0.0) {
                sum += laomp_matrix_get(matrix, j, i);
            }
        }
        res->data[i] = sum;
    }

    return 0;
}

/* Returns both value and index of the minimum element of the given vector. Pass null if you don't care about min value or index in particular. */
void laomp_vec_min(double *min_val, int *min_idx, void *vector)
{
    laomp_vec_t *v;
    double min;
    int min_i;
    int i;

    v = (laomp_vec_t *)vector;
    min = INFINITY;
    min_i = -1;
    for (i = 0; i < v->m; i++) {
        if (v->data[i] < min) {
            min = v->data[i];
            min_i = i;
        }
    }

    if (min_val != NULL) {
        *min_val = min;
    }
    if (min_idx != NULL) {
        *min_idx = min_i;
    }
}

/* Minimum ratio test for phase 1: return min {beta_i / alphaq_i such that (beta_i < 0 and alphaq_i < 0) or (beta_i >= 0 && alphaq_i > 0)} */
int
laomp_phase1_min_ratio(double *theta, int *p, void *beta, void *alphaq)
{
    laomp_vec_t *v1;
    laomp_vec_t *v2;
    double betai;
    double alphaqi;
    double quot;
    double min;
    int min_i;
    int i;

    v1 = (laomp_vec_t *)beta;
    v2 = (laomp_vec_t *)alphaq;
    if (v1->m != v2->m) {
        fprintf(stderr, "Bad dimensions in laomp_phase1_min_ratio\n");
        return -1;
    }
    
    min = INFINITY;
    min_i = -1;
    for (i = 0; i < v1->m; i++) {
        betai = zapsmall(v1->data[i]);
        alphaqi = zapsmall(v2->data[i]);
        if ((betai < 0 && alphaqi < 0) || (betai >= 0 && alphaqi > 0)) {
            quot = betai / alphaqi;
            if (quot < min) {
                min = quot;
                min_i = i;
            }
        }
    }
    *theta = min;
    *p = min_i;
}

/* Minimum ratio test for phase 2: return min {beta_i / alphaq_i such that alphaq_i > 0} */
int
laomp_phase2_min_ratio(double *theta, int *p, void *beta, void *alphaq)
{
    laomp_vec_t *v1;
    laomp_vec_t *v2;
    double betai;
    double alphaqi;
    double quot;
    double min;
    int min_i;
    int i;

    v1 = (laomp_vec_t *)beta;
    v2 = (laomp_vec_t *)alphaq;
    if (v1->m != v2->m) {
        fprintf(stderr, "Bad dimensions in laomp_phase1_min_ratio\n");
        return -1;
    }
    
    min = INFINITY;
    min_i = -1;
    for (i = 0; i < v1->m; i++) {
        betai = zapsmall(v1->data[i]);
        alphaqi = zapsmall(v2->data[i]);
        if (alphaqi > 0) {
            quot = betai / alphaqi;
            if (quot < min) {
                min = quot;
                min_i = i;
            }
        }
    }
    *theta = min;
    *p = min_i;
}

/* Populates the pointers in the ops struct to use these matrix and vector functions */
void
laomp_set_ops(struct la_ops *ops)
{
    ops->ensure_temps = NULL;
    
    ops->matrix_alloc = laomp_matrix_alloc;
    ops->matrix_init = laomp_matrix_init;
    ops->matrix_free = laomp_matrix_free;
    ops->matrix_get = laomp_matrix_get;
    ops->matrix_get_col = laomp_matrix_get_col;
    ops->matrix_set = laomp_matrix_set;
    ops->matrix_set_col = laomp_matrix_set_col;
    ops->matrix_inverse = laomp_matrix_inverse;
    ops->matrix_extract_vec = laomp_matrix_extract_vec;
    ops->matrix_dump = laomp_matrix_dump;
    ops->matrix_randomize = laomp_mat_randomize; 

    ops->vec_alloc = laomp_vec_alloc;
    ops->vec_init = laomp_vec_init;
    ops->vec_free = laomp_vec_free;
    ops->vec_get = laomp_vec_get;
    ops->vec_set = laomp_vec_set;
    ops->vec_dump = laomp_vec_dump;
    ops->vec_extract_vec = laomp_vec_extract_vec; 
    ops->vec_randomize = laomp_vec_randomize;

    ops->scalar_vec_mult = laomp_scalar_vec_mult;
    ops->vec_vec_subtract = laomp_vec_vec_subtract;
    ops->vec_dot_prod = laomp_vec_dot_prod;
    ops->matrix_vec_mult = laomp_matrix_vec_mult;
    ops->vec_matrix_mult = laomp_vec_matrix_mult;
    ops->matrix_matrix_mult = laomp_matrix_matrix_mult;

    ops->matrix_colsum_ltz = laomp_matrix_colsum_ltz;
    ops->vec_min = laomp_vec_min;
    ops->phase1_min_ratio = laomp_phase1_min_ratio;
    ops->phase2_min_ratio = laomp_phase2_min_ratio;
}

