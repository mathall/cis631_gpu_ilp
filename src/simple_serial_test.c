/*
 * CIS 631, Fall 2019
 *
 * Run the sanity test on the simple serial ops
 *
 * Authors: Chris Misa
 */

#include <la.h>
#include <la_simple_serial.h>
#include <la_test.h>


int
main(void)
{
    struct la_ops ops;

    lass_set_ops(&ops);

    printf("----------------------- Starting la_serial_simple sanity check --------------------------\n");
    la_test(&ops);
    printf("----------------------- Finished la_serial_simple sanity check --------------------------\n");

    return 0;
}
