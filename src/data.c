/*
 * CIS 631, Fall 2019
 *
 * Functions for managing matrix and vector data structures
 *
 * Authors: Matt Hall, Chris Misa
 */

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include <data.h>

#define MAX_COL_WIDTH 10

/*
 * Allocate single indirection matrix structure with N rows and M columns
 */
double ** mallocMatrix(int N, int M){
    double **A; 
    // A = malloc( N * sizeof( double *));
    if (( A = malloc( N*sizeof( double* ))) == NULL ){
        printf(" error in first malloc\n" );
        return NULL;
    }
    for (int i = 0; i < N; i++ ){
        if (( A[i] = calloc( M, sizeof(double) )) == NULL ){
            printf(" error in second malloc\n" );
            return NULL;
        }
    }
    return A;
}

/*
 * Free matrix with N columns
 */
void freeMatrix(double **A, int N){
    for(int i = 0; i < N; i++){
        free(A[i]);
    }
    free(A);
}

/*
 * Print matrix
 */
void printMatrix(double **A, int rows, int cols){
    int i;
    int j;
    int col_offset;
    
    for (col_offset = 0; col_offset < cols; col_offset += MAX_COL_WIDTH) {
        printf("Columns %d through %d\n",
                col_offset,
                col_offset + MAX_COL_WIDTH < cols ? col_offset + MAX_COL_WIDTH - 1 : cols - 1);
        for(i = 0; i < rows; i++){
            printf("[%d] ", i);
            for(j = col_offset; j < cols && j < col_offset + MAX_COL_WIDTH; j++){
                printf("% f ", A[i][j]);
            }
            printf("\n");
        }
    }
}

/*
 * Allocate array with N elements
 */
double * mallocArray(int N){
    double *A; 
    if (( A = calloc( N, sizeof( double ))) == NULL ){
        perror(" error in Callocing Array.\n" );
        return NULL;
    }
    return A;
}

/*
 * print array of n elements
 */
void printArray(double *array, int N)
{
    int i;
    for (i = 0; i < N; i++) {
        printf("[%d] = % f\n", i, array[i]);
    }
}




/*
 * Allocate single indirection matrix structure with M rows and N columns
 * Populates matrix with random numbers uniformly distributed within a range
 */
double ** mallocRandMatrix(int M, int N, int min, int max){
    double **A; 
    time_t t;
    srand((unsigned) time (&t)); 

    printf("mallocRandMatrix: M = %d, N = %d, min = %d, max = %d \n", M, N, min, max);

    // A = malloc( N * sizeof( double *));
    if (( A = malloc( M*sizeof( double* ))) == NULL ){
        printf(" error in first malloc\n" );
        return NULL;
    }
    printf("Malloc'd rows \n");

    for (int i = 0; i < N; i++ ){
        if (( A[i] = calloc( N, sizeof(double) )) == NULL ){
            printf(" error in second malloc\n" );
            return NULL;
        }
    }
    printf("Malloc'd columns \n");
    for( int i; i < M; i++){
        // printf("iterating rows\n");
        for( int j; j < N; j++){
            // printf("iterating columns\n");
            // A[i][j] = (rand() % (max - min + 1)) + min;
            A[i][j] = 99;
            printf("A[%d][%d] = 99;", i, j); 
        }
    }

    return A;
}

/*
 * Allocate array with N elements
 * Populates matrix with random numbers uniformly distributed within a range
 */
double * mallocRandArray(int N, int min, int max){
    time_t t;
    srand((unsigned) time (&t)); 

    double *A; 
    if (( A = calloc( N, sizeof( double ))) == NULL ){
        perror(" error in Callocing Array.\n" );
        return NULL;
    }

    for( int i; i < N; i++)
        A[i] = (rand() % (max - min + 1)) + min;

    return A;
}
