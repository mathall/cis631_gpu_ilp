/*
 * CIS 631, Fall 2019
 *
 * Run sanity test on the cuda ops
 *
 * Authors: Chris Misa
 */

#include <la.h>
#include <la_cuda.h>
#include <la_test.h>

int
main(void)
{
    struct la_ops ops;
    
    lacu_set_ops(&ops);

    printf("* * * * * * * * * * Starting la_cuda sanity check * * * * * * * * * *\n");
    la_test(&ops);
    printf("* * * * * * * * * * Finished la_cuda sanity check * * * * * * * * * *\n");
    
    return 0;
}
