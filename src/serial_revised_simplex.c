/*
 * CIS 631, Fall 2019
 *
 * Runs the revised simplex algorithm with simple serial linear algebra backend
 *
 * Authors: Matt Hall, Chris Misa
 */

#include <stdio.h>
#include <la.h>
#include <revised_simplex.h>
#include <la_simple_serial.h>
#include <read_mps.h>

/*
 * Main function for testing
 */
int
main(int argc, char **argv) {
    struct la_ops ops;
    double **A;
    double *b;
    double *c;
    double z;
    int m;
    int n;
    int i;
    enum problem_type type;
    int rv;

    if (argc != 2) {
        printf("Usage: %s <MPS file>\n", argv[0]);
        return 0;
    }

    /* Gather operation function pointers */
    // bm_set_set_ops(&ops);
    lass_set_ops(&ops);

    /* Read the problem in */
    rv = read_mps(argv[1], &A, &b, &c, &m, &n, &type);
    if (rv != 0) {
        fprintf(stderr, "Failed to read mps file at \"%s\"\n", argv[1]);
        return 1;
    }

    /* Coerce into minimization problem */
    if (type == MAXIMIZE) {
        for (i = 0; i < n; i++) {
            c[i] *= -1;
        }
    }

    /* Compute */
    rv = Revised_Simplex(&ops, A, b, c, m, n, &z);
    switch (rv) {
        case 0:
            printf("Program solved. Optimal objective value is %f\n", z);
            break;
        case 1:
            printf("Program unbounded\n");
            break;
        default:
            printf("An error occured in Revised_Simplex()\n");
            break;
    }

    return 0;
}
