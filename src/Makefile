#
# CIS 631, Fall 2019
#
# Make objects from source files
#
# Authors: Chris Misa, Matt Hall
#

# Objects shared across executables
common_objs = data.o read_mps.o la_simple_serial.o common.o revised_simplex.o la_test.o 

# Objects for CUDA implementation
cuda_objs = la_cuda.o la_cublas.o

openMP_objs = la_omp.o

# Executable names
executables = serial_simplex test_mps_reader simple_serial_test serial_revised_simplex benchmark omp_test  omp_revised_simplex

ifneq ($(NO_CUDA),1)
cuda_executables = cuda_test cuda_revised_simplex cublas_test cublas_revised_simplex
endif

.PHONY: all
all: $(addprefix ../,$(executables) $(cuda_executables))

$(BUILDDIR)/%.o: %.c $(HEADERSDIR)/*
	$(CC) -c $(CFLAGS) $(OMPFLAGS) $< -o $@

ifneq ($(NO_CUDA),1)
$(BUILDDIR)/%.o: %.cu $(HEADERSDIR)/*
	$(NVCC) -c $(CFLAGS) $(NVCCFLAGS) $< -o $@
endif

#
# Rules for making particular executables: must also be added to $(executables) above
#
# Keeping these separate since linking will be different
########################################################
../serial_simplex: $(addprefix $(BUILDDIR)/,serial_simplex.o $(common_objs))
	$(CC) $(CFLAGS) $^ -o $@ $(LIBS)

../serial_revised_simplex: $(addprefix $(BUILDDIR)/,serial_revised_simplex.o $(common_objs))
	$(CC) $(CFLAGS) $^ -o $@ $(LIBS)

../test_mps_reader: $(addprefix $(BUILDDIR)/,test_mps_reader.o $(common_objs))
	$(CC) $(CFLAGS) $^ -o $@ $(LIBS)

../simple_serial_test: $(addprefix $(BUILDDIR)/,simple_serial_test.o $(common_objs))
	$(CC) $(CFLAGS) $^ -o $@ $(LIBS)

ifneq ($(NO_CUDA),1)
../cuda_test: $(addprefix $(BUILDDIR)/,cuda_test.o $(common_objs) $(cuda_objs))
	$(CXX) $(CFLAGS) $^ -o $@ $(LIBS) $(NVCCLIBS)

../cuda_revised_simplex: $(addprefix $(BUILDDIR)/,cuda_revised_simplex.o $(common_objs) $(cuda_objs))
	$(CXX) $(CFLAGS) $^ -o $@ $(LIBS) $(NVCCLIBS)

../cublas_test: $(addprefix $(BUILDDIR)/,cublas_test.o $(common_objs) $(cuda_objs))
	$(CXX) $(CFLAGS) $^ -o $@ $(LIBS) $(NVCCLIBS)

../cublas_revised_simplex: $(addprefix $(BUILDDIR)/,cublas_revised_simplex.o $(common_objs) $(cuda_objs))
	$(CXX) $(CFLAGS) $^ -o $@ $(LIBS) $(NVCCLIBS)
endif

../omp_revised_simplex: $(addprefix $(BUILDDIR)/,omp_revised_simplex.o $(common_objs) $(openMP_objs))
	$(CXX) $(CFLAGS) $(OMPFLAGS) $^ -o $@ $(LIBS) $(OMPLIBS)

../omp_test: $(addprefix $(BUILDDIR)/,omp_test.o $(common_objs) $(openMP_objs))
	$(CC) $(CFLAGS) $(OMPFLAGS) $^ -o $@ $(LIBS) $(OMPLIBS)

../benchmark: $(addprefix $(BUILDDIR)/,benchmark.o $(common_objs) $(openMP_objs) $(cuda_objs))
	$(CXX) $(CFLAGS) $(OMPFLAGS) $^ -o $@ $(LIBS) $(OMPLIBS) $(NVCCLIBS)

../common: $(addprefix $(BUILDDIR)/,common.o $(common_objs))
	$(CC) $(CFLAGS) $^ -o $@ $(LIBS)

.PHONY: clean
clean:
	rm -f $(addprefix ../,$(executables) $(cuda_executables))
