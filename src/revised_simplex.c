/*
 * CIS 631, Fall 2019
 *
 * Revised Simplex Implementation
 *
 * Authors: Matt Hall, Chris Misa
 *
 * based on slides from: 
 * https://web.archive.org/web/20191126064616/https://www.cs.upc.edu/~erodri/webpage/cps/theory/lp/revised/slides.pdf
 *
 * The phase 1 algorithm is from Kahn et al., "New artificial-free phase 1 simplex method," in IJENS 2013.
 * 
 */
#include <stdio.h>
#include <math.h>

#include <common.h>
#include <la.h>

// #define DEBUG
// #define DEBUG_PROGRESS
// #define DEBUG_PHASE1

/*
 * Execute the revised simplex method under the following assumptions:
 *
 * 1) The problem is expressed as
 *
 *   minimize z = cx
 *   subject to  Ax >= b, x >= 0
 *      where c, x are n-element vectors, A is an m by n matrix, and b is an m-element vector
 *
 * 2) The problem is non-degenerate
 *
 * Returns 0 if an optimal solution was found, 
 *         1 if the problem is unbounded, 
 *         < 0 if any other error condition occured. 
 */
int
Revised_Simplex(struct la_ops *ops,
                double **A_in,
                double *b_in,
                double *c_in,
                int m, 
                int n,
                double *z_out)
{
    void *A;          /* The revised tableau */
    void *b;          /* The r.h.s. for the constraint matrix */
    void *c;          /* The objective function */
    void *B;          /* The current basis */
    void *B_inverse;  /* Inverse of the current basis */
    void *B_idx;      /* Indices of the columns of A in the current basis (i.e. the basic variables) */
    void *R_idx;      /* Indices of the columns of A not in the current bases (i.e. the non-basic variables) */
    double z;         /* Current cost value */
    void *beta;       /* Current basic solution */
    void *cB;         /* Entries of objective function c corresponding to columns of A currently in the basis */
    double d;         /* Reduced cost for each non-basic variable */
    double d_min;
    void *pi;         /* Simplex multiplier */
    int i, j;
    void *aj;         /* The j-th column of A */
    void *alphaq;     /* Coefficients of x_q in the tableau */
    int q, p;         /* Indices of the variable to bring in and the variable to kick out respectively */
    int q_idx;
    double betai;
    double alphaqi;
    double betai_over_alphaqi; /* Tightness of each basic variable */
    double theta;              /* Tightest bound */
    void *theta_alphaq;        /* alpha_q scalled by theta */
    int rv = -1;

    void *A_R;   /* Columns of A corresponding to non-basic variables */
    void *alpha; /* matrix of alphaq's */
    
    double temp;

    int iteration = 0;

    /* Phase 1 variables */
    void *L;       /* Set of which beta are less than zero */
    double WB;     /* Phase 1 object of each row considered */
    void *w;
    double WB_min; /* Min of phase 1 objective entries */
    int l;
    int flag;

    void *c_R;    /* Objective function coefficients coresponding to non-basic variables */

    uint64_t start;
    uint64_t end;

    /* Allocate memory for all structures needed */
    if (ops->ensure_temps != NULL) {
        ops->ensure_temps(m, n + m);
    }
    A = ops->matrix_alloc(m, n + m);
    b = ops->vec_alloc(m);
    c = ops->vec_alloc(n + m);
    B = ops->matrix_alloc(m, m);
    B_inverse = ops->matrix_alloc(m, m);
    beta = ops->vec_alloc(m);
    cB = ops->vec_alloc(m);
    L = ops->vec_alloc(m);

    A_R = ops->matrix_alloc(m, n);
    alpha = ops->matrix_alloc(m, n);
    w = ops->vec_alloc(n);
    c_R = ops->vec_alloc(n);

    /* Using index vectors to keep track of these sets...should be ints if we had them */
    B_idx = ops->vec_alloc(m);
    R_idx = ops->vec_alloc(n);
    aj = ops->vec_alloc(m);

    pi = ops->vec_alloc(m);

    alphaq = ops->vec_alloc(m);
    theta_alphaq = ops->vec_alloc(m);

    /*  
     * 1. Initialization:
     */

    /* Copy in A */
    for (i = 0; i < m; i++) {
        for (j = 0; j < n; j++) {
            ops->matrix_set(A, i, j, A_in[i][j]);
        }
    }
    /* Add slack variables */
    for (i = 0; i < m; i++) {
        ops->matrix_set(A, i, i + n, 1.0);
    }

    /* Copy in b and c */
    ops->vec_init(b, b_in, m);
    for (i = 0; i < n; i++) {
        ops->vec_set(c, i, c_in[i]);
    }

    /* Set index set B_idx to reflect initial choice of basis w.r.t. columns of A */
    for (i = 0; i < m; i++) {
        ops->vec_set(B_idx, i, i + n);
    }
    /* Set index set R_idx to reflect initial non-basic varibles */
    for (i = 0; i < n; i++) {
        ops->vec_set(R_idx, i, i);
    }

    /* Extract initial B and compute B_inverse */
    ops->matrix_extract_vec(B, A, B_idx);
    if (ops->matrix_inverse(B_inverse, B) != 0) {
        fprintf(stderr, "Failed to invert B\n");
        rv = -1;
        goto done;
    }

    /* Initial basic solution: beta = B_inverse * b, z = 0 */
    ops->matrix_vec_mult(beta, B_inverse, b);

#ifdef DEBUG_PHASE1
    printf("=============== Initial state ==============\n");
    printf("A:\n");
    ops->matrix_dump(stdout, A);
    printf("b:\n");
    ops->vec_dump(stdout, b);
    printf("c:\n");
    ops->vec_dump(stdout, c);
#endif

    printf("============= Phase 1 ================\n");

    InitTSC();
    start = ReadTSC();

    while (1) {

#ifdef DEBUG_PROGRESS
        /* Compute "cost" as sum of all b_i where beta_i < 0 */
        z = 0.0;
        for (i = 0; i < m; i++) {
            if (ops->vec_get(beta, i) < 0) {
                z += ops->vec_get(b, i);
            }
        }
        printf("============== Iteration %d (cost %g) ================\n", iteration, z);
#endif

#ifdef DEBUG_PHASE1
        printf("B:\n");
        ops->matrix_dump(stdout, B);
        printf("B inverse:\n");
        ops->matrix_dump(stdout, B_inverse);
        printf("beta:\n");
        ops->vec_dump(stdout, beta);
#endif

        /* If all beta_i are greater or equal to zero, we have a feasible solution */
        ops->vec_min(&betai, NULL, beta);
        if (betai >= 0) {

#ifdef DEBUG_PHASE1
            printf("Found feasible basis\n");
#endif
            break;
        }

        /* Calculate phase 1 objective function's min element */

        ops->matrix_extract_vec(A_R, A, R_idx);         /* Get the columns of A corresponding to current non-basic variables */
        ops->matrix_matrix_mult(alpha, B_inverse, A_R); /* Get tableau values for these column using B_inverse */
        ops->matrix_colsum_ltz(w, alpha, beta);         /* Sum columns considering only rows where corresponding entry in beta is less than zero */
        ops->vec_min(&WB_min, &q_idx, w);               /* Find the minimum */
        q = ops->vec_get(R_idx, q_idx);                 /* The index returned is the index into R_idx, so also look up the index into A using R_idx*/

        /* If none of the WB entries are negative, the problem is infeasible */
        if (WB_min >= 0) {
            printf("Problem is primal infeasible.\n");
            rv = -1;    
            goto done;
        }

#ifdef DEBUG_PROGRESS
        printf("Min entry %d in objective vector is %g\n", q, WB_min);
#endif

        /* Extract q-th column of original constraint matrix and the tableau */
        ops->matrix_get_col(aj, A, q);
        ops->matrix_get_col(alphaq, alpha, q_idx);

        /* Find basic variable to kick out */
        ops->phase1_min_ratio(&theta, &p, beta, alphaq);

        if (p == -1) {
            printf("Failed to find a basic variable to pivot out in phase 1\n");
            rv = -1;
            goto done;
        }

        /*
         * Make the pivot
         */
#ifdef DEBUG_PROGRESS
        printf("pivoting in column %d, for basis column %d\n", q, p);
#endif

        /* Swap the non-basic entry q with the p-th basic entry; make q the new p-th basic entry */
        ops->vec_set(R_idx, q_idx, ops->vec_get(B_idx, p));
        ops->vec_set(B_idx, p, q);
        
        /* Replace the p-th column in B with the q-th column in A (still stored in aj from above) */
        ops->matrix_set_col(B, p, aj);

        /* Update B inverse */
        if (ops->matrix_inverse(B_inverse, B) != 0) {
            fprintf(stderr, "Failed to invert B!\n");
            rv = -1;
            goto done;
        }

        /* Update beta */
        ops->matrix_vec_mult(beta, B_inverse, b);
        
        iteration++;
    }

    /* Compute cost of initial feasible solution */
    ops->vec_extract_vec(cB, c, B_idx);
    ops->vec_dot_prod(&z, cB, beta);

#ifdef DEBUG
    printf("=============== Initial feasible solution (cost %g) ==============\n", z);
    printf("A:\n");
    ops->matrix_dump(stdout, A);
    printf("b:\n");
    ops->vec_dump(stdout, b);
    printf("c:\n");
    ops->vec_dump(stdout, c);

    printf("B_idx:\n");
    ops->vec_dump(stdout, B_idx);

    printf("beta:\n");
    ops->vec_dump(stdout, beta);
    printf("B:\n");
    ops->matrix_dump(stdout, B);
#endif

    printf("============= Phase 2 ================\n");
    while (1) {

#ifdef DEBUG_PROGRESS
        printf("============== Iteration %d (cost %g) ================\n", iteration, z);
#endif

#ifdef DEBUG
        printf("B:\n");
        ops->matrix_dump(stdout, B);

        printf("B inverse:\n");
        ops->matrix_dump(stdout, B_inverse);
        
        printf("B indices:\n");
        ops->vec_dump(stdout, B_idx);

        printf("beta:\n");
        ops->vec_dump(stdout, beta);
#endif

        /*
         * 2. Calculate reduction in cost for non-basic variables
         */
        ops->vec_extract_vec(cB, c, B_idx);
        ops->vec_matrix_mult(pi, cB, B_inverse);

#ifdef DEBUG
        printf("cB:\n");
        ops->vec_dump(stdout, cB);
        printf("pi:\n");
        ops->vec_dump(stdout, pi);    

        printf("R indices:\n");
        ops->vec_dump(stdout, R_idx);
#endif

        /* For each non-basic variable */
        ops->matrix_extract_vec(A_R, A, R_idx);  /* Form matrix of columns of A corresponding to non-basic variables */
        ops->vec_extract_vec(c_R, c, R_idx);     /* Extract objective function entries corresponding to non-basic variables */
        ops->vec_matrix_mult(w, pi, A_R);        /* w = pi * A_R */
        ops->vec_vec_subtract(w, c_R, w);        /* w = c_r - pi * A_R i.e. compute the cost improvement for each non-basic variable */

        ops->vec_min(&d, &q_idx, w);             /* Find minimum cost improvement i.e. best non-basic variable */

        q = ops->vec_get(R_idx, q_idx);          /* Get the index of this non-basic variable in the columns of A */
        
        /* If all d_j are >= 0 and we have an optimal solution */
        if (d >= 0) {
            *z_out = z;
            rv = 0;
            break;
        }

#ifdef DEBUG_PROGRESS
        printf("q = %d, q_idx = %d, d_q = %g\n", q, q_idx, d);
#endif

        /* Calculate alphaq, the coefficients for the non-basic variable we're bringing in (note aj = aq) */
        ops->matrix_get_col(aj, A, q);
        ops->matrix_vec_mult(alphaq, B_inverse, aj);

#ifdef DEBUG
        printf("alpha_q:\n");
        ops->vec_dump(stdout, alphaq);
#endif

        /*
         * 3. Find basic variable to kick out by determining tightest bound.
         */
        ops->phase2_min_ratio(&theta, &p, beta, alphaq);

        /* If we didn't touch p, then none of the alphaq_i are > 0 so the problem is unbounded */
        if (p == -1) {
            rv = 1;
            break;
        }

#ifdef DEBUG_PROGRESS
        printf("p = %d, theta: %g\n", p, theta);
#endif

        /*
         * 4. Update
         */
        
        /* Swap the non-basic entry q with the p-th basic entry; make q the new p-th basic entry */
        ops->vec_set(R_idx, q_idx, ops->vec_get(B_idx, p));
        ops->vec_set(B_idx, p, q);

        /* Compute beta = beta - theta * alpha_q */
        ops->scalar_vec_mult(theta_alphaq, theta, alphaq);
        ops->vec_vec_subtract(beta, beta, theta_alphaq);

        /* Set beta_p = theta */
        ops->vec_set(beta, p, theta);
        
        /* Replace the p-th column in B with the q-th column in A (still stored in aj from above) */
        ops->matrix_set_col(B, p, aj);

        /* Update cost of current solution */
        z = z + theta * d;

        /* Update B inverse */
        if (ops->matrix_inverse(B_inverse, B) != 0) {
            fprintf(stderr, "Failed to invert B!\n");
            rv = -1;
            goto done;
        }

        iteration++;
    }

    end = ReadTSC();

    printf("Revised simplex execution time: %g\n", ElapsedTime(end - start));

done:
    return rv;
}
