/* 
    Benchmark suit, compaires runtime for primative operations of the 
    Revised Simplex Algorithm. 

    Specifically, it reports time to complete operations in single-threaded
    mode, OpenMP, and CUDA. 

    Operations include, vector-matrix multiplication, matrix-matrix multiplication,
    dot-product, matrix inversion, matrix-set-extraction, to name a few. 

    Witten by Matt Hall and Chris Misa.
*/ 

#include "stdio.h"
#include "stdlib.h"
#include "time.h"

#include <common.h>
#include <la.h>
#include <la_cublas.h>
#include <bm_set.h>
#include <la_simple_serial.h>
#include <la_openMP.h>
#include <la_cuda.h>
#include <data.h>

int benchmark(int m, int n, int iterations, char test_type, int debug); 

int usage(int argc, char **argv){
    if (argc > 5 || argc < 4){
        fprintf(stderr, "Usage: %s <size> <iterations> <s|o|c>\n\t<size> : matrix/vector size to test. vectors will have <size> entries\n\t\tand matrix will have <size>^2 cells.\n\t<iterations> : How many times to run each test\n\t<s|o|c>:\n\t\t s: serial\n\t\t o: openMP\n\t\t c: Cuda\n", argv[0]);
        exit(-1);
    }
    return 0;
}

/* dumps benchmark data to a timestamped file */ 
int
main(int argc, char **argv){

    int m, n, k;
    char test_type; 
    usage(argc, argv);
    m = atoi(argv[1]);
    n = m;
    k = atoi(argv[2]);
    test_type = argv[3][0];
    int debug = 0;
    if (argc == 5)
        debug = 1;

    benchmark(m, n, k, test_type, debug);
    
    return 0;
}


/*
Creates benchmark file for operations in serial, openMP, or cuda implementation. 
test_type should be 's' for serial, 'o' for OpenMP, or 'c' for Cuda, depending 
on the desired test. 
*/
int
benchmark(int m, int n, int k, char test_type, int print_to_console){

    /* initalize operation structure */ 
    FILE *fp; 
    char filename[255]; 
    struct la_ops ops; 
    switch(test_type){
        case 's':
        case 'S': // serial
            lass_set_ops(&ops);
            sprintf(filename, "benchmark_serial_%d_%lu.txt",n,ReadTSC()); 
            break; 
        case 'c':
        case 'C': // Cuda
            lacu_set_ops(&ops);
            sprintf(filename, "benchmark_cuda_%d_%lu.txt",n,ReadTSC()); 
            break;
        case 'b':
        case 'B': // Cuda
            lacb_set_ops(&ops);
            sprintf(filename, "benchmark_cublas_%d_%lu.txt",n, ReadTSC()); 
            break;
        case 'o':
        case 'O':
            laomp_set_ops(&ops);
            sprintf(filename, "benchmark_openMP_%d_%lu.txt",n,ReadTSC()); 
            break; 
        default:
            perror("Invalid benchmark test type. Expected 's', 'c', or 'o' for 'serial', 'cuda', or 'openMP'\n"); 
            return -1; 
    }

    if (ops.ensure_temps != NULL) {
        ops.ensure_temps(m, n);
    }

    /* Open the logfile */
    if (print_to_console)
        fp = stdout;
    else{
        printf("%s\n",filename);   
        fp = fopen(filename, "w");
        if (fp == NULL) {
            fprintf(stderr, "Failed to open \"%s\"\n", filename);
            return -1;
        }
    }   

    fprintf(fp, "Benchmarking serial linear algebra ops\nm_rows = %d\nn_columns= %d\n\n", m, n);
    /* range for random numbers */ 
    int min = -100; 
    int max = 100;     

    uint64_t seed = ReadTSC();
    srand(seed);

    void *mn_mat, *nm_mat, *n_vec, *m_vec;

    /* generate two random matrices */ 
    mn_mat = ops.matrix_alloc(m, n);
    nm_mat = ops.matrix_alloc(n, m);
    printf("randomizing matrix 1\n"); 
    ops.matrix_randomize(mn_mat, min, max);
    printf("randomizing matrix 2\n"); 
    ops.matrix_randomize(nm_mat, min, max);

    /* generate two random vectors */ 
    n_vec = ops.vec_alloc(m);
    m_vec = ops.vec_alloc(n);
    printf("randomizing vec 1\n"); 
    ops.vec_randomize(n_vec, min, max);
    printf("randomizing vec 2\n"); 
    ops.vec_randomize(m_vec, min, max);


    /* print randomized data, unless dims are big... then don't bother */
    if (m < 10 && n <= 10){
        printf("serial mats and vecs\n");
        printf("serial matrix 1\n");
        ops.matrix_dump(fp, mn_mat);
        printf("serial matrix 2\n");
        ops.matrix_dump(fp, nm_mat);
        printf("\nserial vector 1\n");
        ops.vec_dump(fp, n_vec);
        printf("serial vector 2\n");
        ops.vec_dump(fp, m_vec);
    }

    /* Do benchmarking stuff here! */
    // Initialize timer
    uint64_t start_t;
    uint64_t end_t;
    InitTSC();

    fprintf(fp, "matrix_get_col:\n", n);
    void *col_p;
    col_p = ops.vec_alloc(n);  
    for(int i = 0, col; i < k; i++ ){
        col = rand() % n; 
        start_t = ReadTSC();
        ops.matrix_get_col(col_p, mn_mat, col);
        end_t = ReadTSC();
        fprintf(fp, "%g, ", ElapsedTime(end_t - start_t));
    }
    fprintf(fp, "\n");
    ops.vec_free(col_p);

    fprintf(fp, "matrix_set_col:\n", n);    
    void *temp_m_by_n_matrix, *temp_m_vec; 
    temp_m_by_n_matrix = ops.matrix_alloc(m, n); 
    temp_m_vec = ops.vec_alloc(m); 
    for(int i = 0, col; i < k; i++ ){
        col = rand() % n; 
        ops.vec_randomize(temp_m_vec, min, max); 
        start_t = ReadTSC();
        ops.matrix_set_col(temp_m_by_n_matrix, col, temp_m_vec); 
        end_t = ReadTSC();
        fprintf(fp, "%g, ", ElapsedTime(end_t - start_t));
    }
    fprintf(fp, "\n");

    fprintf(fp, "matrix_extract_vec:\n");
    /* create n by m+n matrix, and n vector 'idx_vec' of
     * random numbers in [0, m+n)
     * extract vectors of matrix indicated by 'idx_vec'
     */
    void *temp_n_by_n, *rand_n_by_m_plus_n, *n_idx_vec; 
    // alloc data
    temp_n_by_n = ops.matrix_alloc(n, n); 
    rand_n_by_m_plus_n = ops.matrix_alloc(n, m+n); 
    n_idx_vec = ops.vec_alloc(n); 
    // create random (m)x(m+n) matrix
    for(int i = 0; i < k; i++){
        ops.matrix_randomize(rand_n_by_m_plus_n, min, max);
        // choose random indicies from m+n;
        ops.vec_randomize(n_idx_vec, 0, (m+n)-1);
        // time the operation.
        start_t = ReadTSC();
        ops.matrix_extract_vec(temp_n_by_n, rand_n_by_m_plus_n, n_idx_vec); 
        end_t = ReadTSC();
        fprintf(fp, "%g, ", ElapsedTime(end_t - start_t));
    }
    fprintf(fp, "\n");
    
    fprintf(fp, "matrix_inverse:\n");
    void *inverted_mat_p;
    inverted_mat_p = ops.matrix_alloc(n, n);  
    for(int i = 0, res =-1; i < k; i++, res = 0){
        do{
            if (res != 0){ // matrix inverse failed, come up with a new random matrix
                ops.matrix_randomize(temp_n_by_n, min, max); 
            }
            start_t = ReadTSC();
            res = ops.matrix_inverse(inverted_mat_p, temp_n_by_n);
            end_t = ReadTSC();
            // printf("tried to invert, res = %d\n", res); 
        }while(res != 0); 
        fprintf(fp, "%g, ", ElapsedTime(end_t - start_t));
    }
    fprintf(fp, "\n");

    fprintf(fp, "vec_get:\n");
    for(int i = 0, idx; i < k; i++){
        idx = rand() % n;
        start_t = ReadTSC();
        ops.vec_get(n_vec, idx); 
        end_t = ReadTSC();
        fprintf(fp, "%g, ", ElapsedTime(end_t - start_t));
    }
    fprintf(fp, "\n");
    
    fprintf(fp, "vec_set:\n");
    for(int i = 0, idx; i < k; i++){
        double val = rand(); 
        idx = (int) val % n; 
        start_t = ReadTSC();
        ops.vec_set(n_vec, idx, val);
        end_t = ReadTSC();
        fprintf(fp, "%g, ", ElapsedTime(end_t - start_t));
    }
    fprintf(fp, "\n");
    
    // la_vec_extract_vec_t vec_extract_vec;        /* P */
    fprintf(fp, "vec_extract_vec:\n");
    void *temp_m_plus_n_vec;  
    temp_m_plus_n_vec = ops.vec_alloc(m+n); 
    for(int i = 0; i < k; i++){
        ops.vec_randomize(temp_m_plus_n_vec, min, max); 
        ops.vec_randomize(n_idx_vec, 0, (m+n)-1);
        start_t = ReadTSC();
        ops.vec_extract_vec(n_vec, temp_m_plus_n_vec, n_idx_vec); 
        end_t = ReadTSC();
        fprintf(fp, "%g, ", ElapsedTime(end_t - start_t));
    }
    fprintf(fp, "\n");
    // la_scalar_vec_mult_t scalar_vec_mult;        /* P */
    fprintf(fp, "scalar_vec_mult:\n");
    for(int i = 0; i < k; i++){
        double d = (rand() % (max - min + 1)) + min;
        ops.vec_randomize(n_vec, min, max);
        start_t = ReadTSC();
        ops.scalar_vec_mult(n_vec, d, n_vec); 
        end_t = ReadTSC();
        fprintf(fp, "%g, ", ElapsedTime(end_t - start_t));
    }    
    fprintf(fp, "\n");
    // la_vec_vec_subtract_t vec_vec_subtract;      /* P */
    fprintf(fp, "vec_subtract:\n");
    for(int i = 0; i < k; i++){
        ops.vec_randomize(m_vec, min, max);
        ops.vec_randomize(temp_m_vec, min, max);
        start_t = ReadTSC();
        ops.vec_vec_subtract(temp_m_vec, m_vec, temp_m_vec); 
        end_t = ReadTSC();
        fprintf(fp, "%g, ", ElapsedTime(end_t - start_t));
    }
    fprintf(fp, "\n");

    fprintf(fp, "vec_dot_prod:\n");
    for(int i = 0; i < k; i++){
        ops.vec_randomize(m_vec, min, max);
        ops.vec_randomize(temp_m_vec, min, max);
        double d; 
        start_t = ReadTSC();
        ops.vec_dot_prod(&d, m_vec, temp_m_vec);
        end_t = ReadTSC();
        fprintf(fp, "%g, ", ElapsedTime(end_t - start_t));
    }
    fprintf(fp, "\n");

    // la_matrix_vec_mult_t matrix_vec_mult;        /* P */
    fprintf(fp, "matrix_vec_mult:\n");
    for(int i = 0; i < k; i++){
        ops.matrix_randomize(mn_mat, min, max);
        ops.vec_randomize(n_vec, min, max);
        start_t = ReadTSC();
        ops.matrix_vec_mult(temp_m_vec, mn_mat, n_vec);
        end_t = ReadTSC();
        fprintf(fp, "%g, ", ElapsedTime(end_t - start_t));
    }
    fprintf(fp, "\n");
    // la_vec_matrix_mult_t vec_matrix_mult;        /* P */

    fprintf(fp, "vec_matrix_mult:\n");
    for(int i = 0; i < k; i++){
        ops.matrix_randomize(mn_mat, min, max);
        ops.vec_randomize(m_vec, min, max);
        start_t = ReadTSC();
        ops.vec_matrix_mult(n_vec, m_vec, mn_mat);
        end_t = ReadTSC();
        fprintf(fp, "%g, ", ElapsedTime(end_t - start_t));
    }
    fprintf(fp, "\n");

    // la_matrix_matrix_mult_t matrix_matrix_mult;  /* P */
    fprintf(fp, "matrix_matrix_mult:\n");
        for(int i = 0; i < k; i++){
        ops.matrix_randomize(temp_m_by_n_matrix, min, max);
        ops.matrix_randomize(temp_n_by_n, min, max);
        start_t = ReadTSC();
        ops.vec_matrix_mult(mn_mat, temp_m_by_n_matrix, temp_n_by_n); 
        end_t = ReadTSC();
        fprintf(fp, "%g, ", ElapsedTime(end_t - start_t));
    }
    fprintf(fp, "\n");

    // la_matrix_colsum_ltz_t matrix_colsum_ltz;    /* P */
    fprintf(fp, "matrix_colsum_ltz:\n");
    for(int i = 0; i < k; i++){
        ops.matrix_randomize(mn_mat, min, max);
        ops.vec_randomize(n_idx_vec, 0, n-1);
        start_t = ReadTSC();
        ops.matrix_colsum_ltz(n_vec, mn_mat, n_idx_vec);
        end_t = ReadTSC();
        fprintf(fp, "%g, ", ElapsedTime(end_t - start_t));
    }
    fprintf(fp, "\n");

    // la_vec_min_t vec_min;                        /* P */
    fprintf(fp, "vec_min:\n");
    for(int i = 0; i < k; i++){
        ops.vec_randomize(n_vec, min, max);
        start_t = ReadTSC();
        ops.vec_min(NULL, NULL, n_vec);
        end_t = ReadTSC();
        fprintf(fp, "%g, ", ElapsedTime(end_t - start_t));
    }

    // la_phase1_min_ratio_t phase1_min_ratio;      /* P */
    fprintf(fp, "\n");
    fprintf(fp, "phase1_min_ratio:\n");
    for(int i = 0; i < k; i++){
        ops.vec_randomize(m_vec, min, max);
        ops.vec_randomize(temp_m_vec, min, max);
        double d; 
        int temp; 
        start_t = ReadTSC();
        ops.phase1_min_ratio(&d, &temp, m_vec, temp_m_vec);
        end_t = ReadTSC();
        fprintf(fp, "%g, ", ElapsedTime(end_t - start_t));
    }
    fprintf(fp, "\n");

    // la_phase2_min_ratio_t phase2_min_ratio;      /* P */
    fprintf(fp, "phase2_min_ratio:\n");
    for(int i = 0; i < k; i++){
        ops.vec_randomize(m_vec, min, max);
        ops.vec_randomize(temp_m_vec, min, max);
        double d; 
        int temp; 
        start_t = ReadTSC();
        ops.phase2_min_ratio(&d, &temp, m_vec, temp_m_vec);
        end_t = ReadTSC();
        fprintf(fp, "%g, ", ElapsedTime(end_t - start_t));
    }
    fprintf(fp, "\n");
    /* Clean up, Clean up; everybody do your share */ 
    ops.matrix_free(mn_mat); 
    ops.matrix_free(nm_mat);
    ops.vec_free(n_vec); 
    ops.vec_free(m_vec);
    ops.vec_free(temp_m_vec); 
    ops.vec_free(n_idx_vec); 
    // ops.matrix_free(inverted_mat_p); 
    ops.matrix_free(temp_m_by_n_matrix); 
    ops.matrix_free(temp_n_by_n); 
    ops.matrix_free(rand_n_by_m_plus_n); 
    // ops.vec_free(temp_m_plus_n_vec); 
    
    return 0; 
}
