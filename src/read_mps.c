/*
 * CIS 631, Fall 2019
 *
 * Functions to read MPS formatted files
 *
 * Authors: Chris Misa
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* Local include */
#include <read_mps.h>
#include <data.h>
#include <common.h>

/*
 * Defines for the labels expected in MPS files
 */
#define READ_MPS_INDICATOR_NAME "NAME"
#define READ_MPS_INDICATOR_NAME_LEN 4
#define READ_MPS_INDICATOR_ROWS "ROWS"
#define READ_MPS_INDICATOR_ROWS_LEN 4
#define READ_MPS_INDICATOR_COLUMNS "COLUMNS"
#define READ_MPS_INDICATOR_COLUMNS_LEN 7
#define READ_MPS_INDICATOR_RHS "RHS"
#define READ_MPS_INDICATOR_RHS_LEN 3
#define READ_MPS_INDICATOR_RANGES "RANGES"
#define READ_MPS_INDICATOR_RANGES_LEN 6
#define READ_MPS_INDICATOR_BOUNDS "BOUNDS"
#define READ_MPS_INDICATOR_BOUNDS_LEN 6
#define READ_MPS_INDICATOR_ENDATA "ENDATA"
#define READ_MPS_INDICATOR_ENDATA_LEN 6
#define READ_MPS_INDICATOR_OBJSENSE "OBJSENSE"
#define READ_MPS_INDICATOR_OBJSENSE_LEN 8

#define READ_MPS_PROB_TYPE_MIN "MIN"
#define READ_MPS_PROB_TYPE_MAX "MAX"

/*
 * Advances the given pointer to the next, non-space character
 */
static void
skip_white_space(const char **ch) {
    const char *tmp;

    tmp = *ch;
    while (*tmp == ' ' && *tmp != '\0') {
        tmp++;
    }
    *ch = tmp;
}

/*
 * Returns a copy of the next space-separated word
 * Advances the pointer to the next space (or null) character
 *
 * If something fails, returns NULL and the pointer is not moved
 */
static char *
copy_word(const char **ch)
{
    const char *tmp;
    int word_size = 0;
    char *res;

    tmp = *ch;
    while (tmp[word_size] != ' '
            && tmp[word_size] != '\n'
            && tmp[word_size] != '\0') {
        word_size++;
    }

    res = (char *)malloc(word_size * sizeof(char) + 1);
    if (res == NULL) {
        return NULL;
    }
    
    memcpy(res, tmp, word_size * sizeof(char));
    res[word_size] = '\0';

    *ch = tmp + word_size;
    
    return res;
}

/*
 * Reads a single row line and pushes a new struct onto rows
 */
static int
read_mps_row(const char *row_str, read_mps_row_t **rows)
{
    read_mps_row_t *new_row;

    new_row = (read_mps_row_t *)malloc(sizeof(read_mps_row_t));
    if (new_row == NULL) {
        fprintf(stderr, "No memory for new row!\n");
        goto error_out;
    }

    /* Skip leading white space */
    skip_white_space(&row_str);

    /* Deal with row sense */
    switch (*row_str) {
        case 'N':
            new_row->op = READ_MPS_ROW_OP_OBJ;
            break;
        case 'G':
            new_row->op = READ_MPS_ROW_OP_GREATER;
            break;
        case 'L':
            new_row->op = READ_MPS_ROW_OP_LESS;
            break;
        case 'E':
            new_row->op = READ_MPS_ROW_OP_EQUAL;
            break;
        default:
            fprintf(stderr, "Unknown row sense character: %c\n", *row_str);
            goto error_out;
    }

    /* Move to and copy row name */
    row_str++;
    skip_white_space(&row_str);
    new_row->name = copy_word(&row_str);
    if (new_row->name == NULL) {
        fprintf(stderr, "Failed to read row name\n");
        goto error_out;
    }

    /* Push onto list */
    new_row->next = *rows;
    *rows = new_row;

    return 0;

error_out:
    if (new_row != NULL) {
        free(new_row);
    }
    return -1;
}

static void
read_mps_dump_rows(read_mps_row_t *rows)
{
    while (rows != NULL) {
        DEBUG_PRINT("Row name: \"%s\", operation: %d\n", 
                rows->name, rows->op);
        rows = rows->next;
    }
}

/*
 * Reads a single col line and pushed a new struct onto cols or appends to last_col's entries
 */
static int
read_mps_col(const char *col_str, read_mps_col_t **cols)
{
    char *col_name = NULL;
    read_mps_col_t *new_col = NULL;
    read_mps_row_t *new_row = NULL;

    /* Parse the column name */
    skip_white_space(&col_str);
    col_name = copy_word(&col_str);

    /* If same name as last col, append these entries */
    if (*cols != NULL && strcmp(col_name, (*cols)->name) == 0) {
        new_col = *cols;

        /* Don't need this any more */
        free(col_name);
        col_name = NULL;

    /* Otherwise, start new column */
    } else {
        new_col = (read_mps_col_t *)malloc(sizeof(read_mps_col_t));
        if (new_col == NULL) {
            fprintf(stderr, "No memory for new column\n");
            goto error_out;
        }
        new_col->name = col_name;
        new_col->entries = NULL;

        /* Push onto cols list */
        new_col->next = *cols;
        *cols = new_col;
    }

    /* Parse first entry */
    new_row = (read_mps_row_t *)malloc(sizeof(read_mps_row_t));
    if (new_row == NULL) {
        fprintf(stderr, "No memory for new row while parsing column\n");
        goto error_out;
    }
    skip_white_space(&col_str);
    new_row->name = copy_word(&col_str);
    new_row->value = strtod(col_str, (char **)&col_str);
    
    /* Push it onto entries */
    new_row->next = new_col->entries;
    new_col->entries = new_row;
    
    /* Possibly parse second entry */
    skip_white_space(&col_str);
    if (*col_str != '\0') {
        new_row = (read_mps_row_t *)malloc(sizeof(read_mps_row_t));
        if (new_row == NULL) {
            fprintf(stderr, "No memory for new row while parsing column\n");
            goto error_out;
        }
        new_row->name = copy_word(&col_str);
        new_row->value = strtod(col_str, (char **)&col_str);
        
        /* Push it onto entries */
        new_row->next = new_col->entries;
        new_col->entries = new_row;
    }

    return 0;

error_out:
    if (col_name != NULL) {
        free(col_name);
    }
    if (new_row != NULL) {
        free(new_row);
    }
    return -1;
}

static void
read_mps_dump_cols(read_mps_col_t *cols)
{
    read_mps_row_t *row_ptr;
    while (cols != NULL) {
        DEBUG_PRINT("Column name: \"%s\"\n", cols->name);
        row_ptr = cols->entries;
        while (row_ptr != NULL) {
            DEBUG_PRINT("  row: \"%s\", value: %g\n",
                    row_ptr->name, row_ptr->value);
            row_ptr = row_ptr->next;
        }
        cols = cols->next;
    }
}

/*
 * Search list of rows for the given name and return its index
 *
 * Returns -2 if not found since we use -1 to indicate the objective function row
 */
static int
get_row_index_for_name(read_mps_row_t *rows, char *name)
{
    while (rows != NULL) {
        if (strcmp(rows->name, name) == 0) {
            return rows->index;
        }
        rows = rows->next;
    }
    return -2;
}

/*
 * Update the matrix A so that row r is removed
 */
void
read_mps_remove_row(double **A, double *b, int *m_in, int *n_in, int r)
{
    int i, j;
    int m, n;

    m = (*m_in) - 1;
    n = *n_in;

    for (i = r; i < m; i++) {
        for (j = 0; j < n; j++) {
            A[i][j] = A[i+1][j];
        }
        b[i] = b[i+1];
    }

    *m_in = m;
}

/*
 * Update the matrix A so that column col is removed
 */
void
read_mps_remove_col(double **A, double *c, int *m_in, int *n_in, int col)
{
    int i, j;
    int m, n;

    m = *m_in;
    n = (*n_in) - 1;
    
    for (j = col; j < n; j++) {
        for (i = 0; i < m; i++) {
            A[i][j] = A[i][j+1];
        }
        c[j] = c[j+1];
    }

    *n_in = n;
}

/*
 * Checks the problem for certain feasibility requirements.
 *
 * Eliminates zero rows and column and hence might change the dimensions
 *
 * Returns 0 of the problem is ok, non-zero if the problem cannot be sovled.
 */
int
read_mps_check_problem(double **A, double *b, double *c, int *m_in, int *n_in)
{
    int i, j;
    int m, n;

    m = *m_in;
    n = *n_in;

    /* Check for zero rows */
    for (i = 0; i < m; i++) {
        for (j = 0; j < n; j++) {
            if (A[i][j] != 0) {
                break;
            }
        }
        if (j == n) {
            if (b[i] == 0) {
                printf("Found zero row %d, removing\n", i);
                read_mps_remove_row(A, b, m_in, n_in, i);
                
                /* Update loop bounds and re-check current index */
                m = *m_in;
                i--;

            } else {
                printf("Non-zero RHS for zero row %d: problem infeasible!\n", i);
                return -1;
            }
        }
    }

    /* Check for zero columns */
    for (j = 0; j < n; j++) {
        for (i = 0; i < m; i++) {
            if (A[i][j] != 0) {
                break;
            }
        }
        if (i == m) {
            printf("Found zero column %d, removing\n", j);
            read_mps_remove_col(A, c, m_in, n_in, j);

            /* Update bounds, re-check current column */
            n = *n_in;
            j--;
        }
    }

    return 0;
}

/*
 * Reads an MPS file at the given file_path.
 * Returns by reference:
 *   A : The constraint matrix
 *   c : The right-hand-side of the constraints
 *   b : Coefficients for the objective function
 *
 * Supports the OBJSENSE extension to MPS to determine the problem type (default to MIN)
 *
 * If the problem type is MAX, all equalities are normalized to less than,
 * if the problem type is MIN, all equalities are normalized to greater than.
 *
 * Since the BOUNDS and RANGES sections are not supported, we assume all variables
 * are non-negative as suggested here: http://lpsolve.sourceforge.net/5.0/mps-format.htm
 *
 * The actual return value is 0 on success, otherwise a number indicating the type of error.
 */
int
read_mps(const char *file_path,
         double ***A,
         double **b,
         double **c,
         int *m,
         int *n,
         enum problem_type *prob_type)
{
    int rv = 0;
    FILE *fp;
    read_mps_row_t *rows = NULL;
    read_mps_row_t *row_ptr;
    read_mps_col_t *cols = NULL;
    read_mps_col_t *col_ptr;
    read_mps_col_t *rhss = NULL;
    char *line_ptr = NULL;
    char *tmp_ptr;
    size_t line_buf_len;
    ssize_t line_len;
    int state = -1;

    enum problem_type new_prob_type = MINIMIZE;
    int new_m = 0;
    int new_n = 0;
    double **new_A = NULL;
    double *new_b = NULL;
    double *new_c = NULL;
    
    int row_index;
    int i;

    /* Open the file */
    fp = fopen(file_path, "r");
    if (fp == NULL) {
        fprintf(stderr, "Failed to open \"%s\"\n",
                file_path);
        rv = -1;
        goto error_out;
    }
    
    /* Read records into temporary lists */
    while ((line_len = getline(&line_ptr, &line_buf_len, fp)) != -1) {

        /* Void-out terminal newline */
        if (line_len < 1) {
            DEBUG_PRINT("Read empy line?\n");
            continue;
        }
        line_ptr[line_len - 1] = '\0';

        /* Handle comments */
        if (line_ptr[0] == '*') {
            continue;
            
        /* Handle inner-section lines */
        } else if (line_ptr[0] == ' ') {
            if (state == -1) {
                fprintf(stderr, "File does not start with valid indicator\n");
                rv = -1;
                goto error_out;
            }
            switch (state) {

                case 0: /* Do nothing state for skipping sections we don't yet support */
                    break;

                case 1:
                    rv = read_mps_row(line_ptr, &rows);
                    break;

                case 2:
                    rv = read_mps_col(line_ptr, &cols);
                    break;

                case 3:
                    rv = read_mps_col(line_ptr, &rhss);
                    break;

                default:
                    fprintf(stderr, "Unknown internal state (%d) in read_mps()\n", state);
                    rv = -1;
            }
            if (rv != 0) {
                goto error_out;
            }

        /* Handle transition between sections */
        } else {

            if (strncmp(line_ptr, READ_MPS_INDICATOR_NAME, READ_MPS_INDICATOR_NAME_LEN) == 0) {
                tmp_ptr = line_ptr + READ_MPS_INDICATOR_NAME_LEN;
                skip_white_space((const char **)&tmp_ptr);
                DEBUG_PRINT("Read MPS name: \"%s\"\n", tmp_ptr);
                state = 0;

            } else if (strncmp(line_ptr, READ_MPS_INDICATOR_OBJSENSE, READ_MPS_INDICATOR_OBJSENSE_LEN) == 0) {
                tmp_ptr = line_ptr + READ_MPS_INDICATOR_OBJSENSE_LEN;
                skip_white_space((const char **)&tmp_ptr);
                DEBUG_PRINT("Problem type: \"%s\"\n", tmp_ptr);
                if (strcmp(tmp_ptr, READ_MPS_PROB_TYPE_MIN) == 0) {
                    new_prob_type = MINIMIZE;
                } else if (strcmp(tmp_ptr, READ_MPS_PROB_TYPE_MAX) == 0) {
                    new_prob_type = MAXIMIZE;
                } else {
                    fprintf(stderr, "Unknown value for OBJSENSE field: \"%s\"\n", tmp_ptr);
                    rv = -1;
                    goto error_out;
                }
                state = 0;

            } else if (strncmp(line_ptr, READ_MPS_INDICATOR_ROWS, READ_MPS_INDICATOR_ROWS_LEN) == 0) {
                state = 1;

            } else if (strncmp(line_ptr, READ_MPS_INDICATOR_COLUMNS, READ_MPS_INDICATOR_COLUMNS_LEN) == 0) {
                state = 2;
                
            } else if (strncmp(line_ptr, READ_MPS_INDICATOR_RHS, READ_MPS_INDICATOR_RHS_LEN) == 0) {
                state = 3;
                
            } else if (strncmp(line_ptr, READ_MPS_INDICATOR_RANGES, READ_MPS_INDICATOR_RANGES_LEN) == 0) {
                fprintf(stderr, "WARNING: Ranges sections not supported yet. . .\n");
                state = 0;
                continue;
            } else if (strncmp(line_ptr, READ_MPS_INDICATOR_BOUNDS, READ_MPS_INDICATOR_BOUNDS_LEN) == 0) {
                fprintf(stderr, "WARNING: Bounds sections not supported yet! Assuming all variables are non-negative!\n");
                state = 0;
                continue;
            } else if (strncmp(line_ptr, READ_MPS_INDICATOR_ENDATA, READ_MPS_INDICATOR_ENDATA_LEN) == 0) {
                break;
            }
        }
    }

    /* Count number of rows, set row indices, and find objective function */
    new_m = 0;
    row_ptr = rows;
    while (row_ptr != NULL) {
    
        /* Mark objective function with index -1 */
        if (row_ptr->op == READ_MPS_ROW_OP_OBJ) {
            row_ptr->index = -1;

        /* Equality constraints will break into two inequality constraints: save space for them here */
        } else if (row_ptr->op == READ_MPS_ROW_OP_EQUAL) {
            row_ptr->index = new_m++;
            new_m++;

        } else {
            row_ptr->index = new_m++;
        }
        
        row_ptr = row_ptr->next;
    }

    /* Count number of columns and set column indices */
    new_n = 0;
    col_ptr = cols;
    while (col_ptr != NULL) {
        col_ptr->index = new_n++;
        col_ptr = col_ptr->next;
    }

    /* Allocate result structures */
    new_A = mallocMatrix(new_m, new_n);
    new_b = mallocArray(new_m);
    new_c = mallocArray(new_n);
    if (new_A == NULL || new_b == NULL || new_c == NULL) {
        fprintf(stderr, "Allocation for results failed\n");
        rv = -1;
        goto error_out;
    }

    /* Populate constraints and objective function */
    col_ptr = cols;
    while (col_ptr != NULL) {
        
        row_ptr = col_ptr->entries;
        while (row_ptr != NULL) {

            /* Search rows to get the index */
            row_index = get_row_index_for_name(rows, row_ptr->name);
            if (row_index == -2) {
                fprintf(stderr, "Failed to find definition of row \"%s\" referenced in column \"%s\"\n",
                        row_ptr->name, col_ptr->name);
                rv = -1;
                goto error_out;
            }
        
            /* Write value into constraints or objective function */
            if (row_index == -1) {
                if (new_prob_type == MAXIMIZE) {
                    new_c[col_ptr->index] = -1 * row_ptr->value;
                } else {
                    new_c[col_ptr->index] = row_ptr->value;
                }
            } else {
                new_A[row_index][col_ptr->index] = row_ptr->value;
            }

            row_ptr = row_ptr->next;
        }

        col_ptr = col_ptr->next;
    }
    
    if (new_prob_type == MAXIMIZE) {
        printf("Warning: switched minimize to maximize by negating objective functions: objective value will be -1 * actual object value\n");
        new_prob_type = MINIMIZE;
    }

    /* Populate RHS vector */
    row_ptr = rhss->entries;
    while (row_ptr != NULL) {

        /* Search rows to get the index */
        row_index = get_row_index_for_name(rows, row_ptr->name);
        if (row_index == -2) {
            fprintf(stderr, "Failed to find definition of row \"%s\" of RHS vector\n",
                    row_ptr->name);
            rv = -1;
            goto error_out;
        }

        /* Write the value into the RHS vector */
        new_b[row_index] = row_ptr->value;

        row_ptr = row_ptr->next;
    }

    /* Normalize the direction of in-equalities and expand equalities based on problem type*/
    row_ptr = rows;
    while (row_ptr != NULL) {

        /* Skip the objective function row */
        if (row_ptr->index == -1) {
            row_ptr = row_ptr->next;
            continue;
        }

        /* Flip inequality */
        if (row_ptr->op == READ_MPS_ROW_OP_GREATER) {
            for (i = 0; i < new_n; i++) {
                new_A[row_ptr->index][i] *= -1;
            }
            new_b[row_ptr->index] *= -1;
            row_ptr->op = READ_MPS_ROW_OP_LESS;

        /* Duplicate row in extra space and flip */
        } else if (row_ptr->op == READ_MPS_ROW_OP_EQUAL) {
            for (i = 0; i < new_n; i++) {
                new_A[row_ptr->index + 1][i] = -1 * new_A[row_ptr->index][i];
            }
            new_b[row_ptr->index + 1] = -1 * new_b[row_ptr->index];
            row_ptr->op = READ_MPS_ROW_OP_LESS;
        }
        row_ptr = row_ptr->next;
    }

    /* Make sure the problem is ok to try solving */
    if (read_mps_check_problem(new_A, new_b, new_c, &new_m, &new_n) != 0) {
        goto error_out;
    }

    /* Write return values */
    *A = new_A;
    *b = new_b;
    *c = new_c;
    *m = new_m;
    *n = new_n;
    *prob_type = new_prob_type;

    /* Close the file and return */
    if (fp != NULL) {
        fclose(fp);
    }
    return rv;

    /* Clean up anything that might have been allocated or opened */
error_out:
    if (fp != NULL) {
        fclose(fp);
    }
    if (new_A != NULL) {
        freeMatrix(new_A, new_n);
    }
    if (new_b != NULL) {
        free(new_b);
    }
    if (new_c != NULL) {
        free(new_c);
    }
    return rv;
}
