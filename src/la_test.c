/*
 * CIS 631, Fall 2019
 *
 * Generic sanity tests of linear algebra (and index set) functions
 *
 * Skips over sets of operations if the functions pointers are set to NULL for incremental testing.
 *
 * Authors: Chris Misa
 */

#include <stdio.h>
#include <stdlib.h>

#include <la.h>
#include <data.h>

void
la_test(struct la_ops *ops)
{
    void *set1;
    void *set2;
    int i, j;

    void *matrix1;
    void *matrix2;
    void *matrix3;
    double **temp_matrix;
    void *vec1;
    void *vec2;
    void *vec3;
    double temp_vec[4] = {0.1, 0.2, 1.3, 4.5};
    double temp_d;
    double temp_d2;
    double temp_d3;
    int temp_i;

    if (ops->ensure_temps != NULL) {
        ops->ensure_temps(100, 100);
    }

    printf("================== Matrix operations ===============\n");

    if (ops->matrix_alloc != NULL
            && ops->matrix_set != NULL
            && ops->matrix_get != NULL
            && ops->matrix_dump != NULL
            && ops->matrix_init != NULL
            && ops->matrix_free != NULL) {

        matrix1 = ops->matrix_alloc(4, 5);
        ops->matrix_set(matrix1, 0, 1, 23.45);
        ops->matrix_set(matrix1, 1, 4, 12.2);
        printf("Set some values\n");
        ops->matrix_dump(stdout, matrix1);

        printf("Entry at 1, 4 is %f\n", ops->matrix_get(matrix1, 1, 4));
        printf("Entry at 2, 1 is %f\n", ops->matrix_get(matrix1, 2, 1));

        temp_matrix = mallocMatrix(4, 3);
        temp_matrix[0][1] = 34.5;
        temp_matrix[0][2] = 12.3;
        temp_matrix[3][1] = 1.02;
        matrix2 = ops->matrix_alloc(4, 3);
        ops->matrix_init(matrix2, temp_matrix, 4, 3);
        printf("Loaded matrix from c-style 2-d array\n");
        ops->matrix_dump(stdout, matrix2);

        ops->matrix_free(matrix1);
        ops->matrix_free(matrix2);
    } else {
        printf(". . . unimplemented\n");
    }

    printf("================ Vector operations ================\n");
    
    if (ops->vec_alloc != NULL
            && ops->vec_set != NULL
            && ops->vec_dump != NULL
            && ops->vec_get != NULL
            && ops->vec_init != NULL
            && ops->vec_dot_prod != NULL
            && ops->scalar_vec_mult != NULL
            && ops->vec_vec_subtract != NULL
            && ops->vec_free != NULL) {

        vec1 = ops->vec_alloc(13);
        vec2 = ops->vec_alloc(13);
        ops->vec_set(vec1, 0, 0.34);
        ops->vec_set(vec1, 4, 2.2);
        ops->vec_set(vec1, 6, 1.4);
        ops->vec_set(vec1, 9, 5.9);
        printf("Set some values (vec1)\n");
        ops->vec_dump(stdout, vec1);

        printf("Entry at 5 is %f\n", ops->vec_get(vec1, 5));
        printf("Entry at 4 is %f\n", ops->vec_get(vec1, 4));

        ops->scalar_vec_mult(vec2, 2.0, vec1);
        printf("vec1 * 2.0 = \n");
        ops->vec_dump(stdout, vec2);

        vec3 = ops->vec_alloc(13);
        ops->vec_vec_subtract(vec3, vec2, vec1);
        printf("vec2 - vec1 = \n");
        ops->vec_dump(stdout, vec3);
        
        ops->vec_free(vec3);

        vec2 = ops->vec_alloc(4);
        ops->vec_init(vec2, temp_vec, 4);
        printf("Loaded vector from c-style array\n");
        ops->vec_dump(stdout, vec2);

        vec3 = ops->vec_alloc(4);
        ops->vec_set(vec3, 0, 0.0);
        ops->vec_set(vec3, 1, 1.0);
        ops->vec_set(vec3, 2, 0.5);
        ops->vec_set(vec3, 3, 0.0);
        ops->vec_dot_prod(&temp_d, vec2, vec3);

        printf("vec2 is:\n");
        ops->vec_dump(stdout, vec2);
        printf("vec3 is:\n");
        ops->vec_dump(stdout, vec3);
        printf("Dot product between vec2 and vec3 is %f\n", temp_d);

        ops->vec_free(vec1);
        ops->vec_free(vec2);

        vec1 = ops->vec_alloc(700);
        vec2 = ops->vec_alloc(700);
        temp_d = 0.0;
        for (i = 0; i < 700; i++) {
            temp_d2 = (double)rand() / (double)RAND_MAX;
            temp_d3 = (double)rand() / (double)RAND_MAX;
            temp_d += temp_d2 * temp_d3;
            ops->vec_set(vec1, i, temp_d2);
            ops->vec_set(vec2, i, temp_d3);
        }
        printf("Dot product test on 700 elements should give %g\n", temp_d);
        ops->vec_dot_prod(&temp_d, vec1, vec2);
        printf("Result: %g\n", temp_d);
        
    } else {
        printf(". . . unimplemented\n");
    }

    printf("================ Matrix, Vector operations ================\n");
    
    if (ops->vec_alloc != NULL
            && ops->matrix_alloc != NULL
            && ops->vec_set != NULL
            && ops->matrix_set != NULL
            && ops->matrix_vec_mult != NULL
            && ops->vec_dump != NULL
            && ops->matrix_dump != NULL
            && ops->matrix_free != NULL
            && ops->vec_free != NULL) {
        vec1 = ops->vec_alloc(2);
        matrix1 = ops->matrix_alloc(3, 2);
        vec2 = ops->vec_alloc(3);

        ops->vec_set(vec1, 0, 1);
        ops->vec_set(vec1, 1, 0.5);

        ops->matrix_set(matrix1, 0, 0, 2);
        ops->matrix_set(matrix1, 1, 0, 3);
        ops->matrix_set(matrix1, 2, 0, 4);
        ops->matrix_set(matrix1, 0, 1, 1);
        ops->matrix_set(matrix1, 1, 1, 0.5);
        ops->matrix_set(matrix1, 2, 1, 0.25);

        ops->matrix_vec_mult(vec2, matrix1, vec1);

        printf("vec1:\n");
        ops->vec_dump(stdout, vec1);
        printf("matrix1:\n");
        ops->matrix_dump(stdout, matrix1);
        printf("matrix1 * vec1:\n");
        ops->vec_dump(stdout, vec2);

        ops->vec_free(vec1);
        ops->vec_free(vec2);
        ops->matrix_free(matrix1);
    } else {
        printf(". . . unimplemented\n");
    }

    if (ops->vec_alloc != NULL
            && ops->matrix_alloc != NULL
            && ops->vec_set != NULL
            && ops->matrix_set != NULL
            && ops->vec_dump != NULL
            && ops->matrix_dump != NULL
            && ops->vec_matrix_mult != NULL
            && ops->matrix_free != NULL
            && ops->vec_free != NULL) {

        vec1 = ops->vec_alloc(2);
        matrix1 = ops->matrix_alloc(3, 2);
        vec2 = ops->vec_alloc(3);

        ops->vec_set(vec1, 0, 1);
        ops->vec_set(vec1, 1, 0.5);

        ops->matrix_set(matrix1, 0, 0, 2);
        ops->matrix_set(matrix1, 1, 0, 3);
        ops->matrix_set(matrix1, 2, 0, 4);
        ops->matrix_set(matrix1, 0, 1, 1);
        ops->matrix_set(matrix1, 1, 1, 0.5);
        ops->matrix_set(matrix1, 2, 1, 0.25);
        ops->vec_set(vec2, 0, 1);
        ops->vec_set(vec2, 1, 2);
        ops->vec_set(vec2, 2, 1.5);
        
        ops->vec_matrix_mult(vec1, vec2, matrix1);

        printf("vec2:\n");
        ops->vec_dump(stdout, vec2);
        printf("matrix1:\n");
        ops->matrix_dump(stdout, matrix1);
        printf("vec2 * matrix1:\n");
        ops->vec_dump(stdout, vec1);

        ops->vec_free(vec1);
        ops->vec_free(vec2);
        ops->matrix_free(matrix1);

    } else {
        printf(". . . unimplemented\n");
    }

    if (ops->vec_alloc != NULL
            && ops->matrix_alloc != NULL
            && ops->vec_set != NULL
            && ops->matrix_set != NULL
            && ops->vec_dump != NULL
            && ops->matrix_dump != NULL
            && ops->matrix_set_col != NULL
            && ops->matrix_free != NULL
            && ops->vec_free != NULL) {

        vec1 = ops->vec_alloc(2);
        matrix1 = ops->matrix_alloc(3, 2);
        vec2 = ops->vec_alloc(3);

        ops->vec_set(vec1, 0, 1);
        ops->vec_set(vec1, 1, 0.5);

        ops->matrix_set(matrix1, 0, 0, 2);
        ops->matrix_set(matrix1, 1, 0, 3);
        ops->matrix_set(matrix1, 2, 0, 4);
        ops->matrix_set(matrix1, 0, 1, 1);
        ops->matrix_set(matrix1, 1, 1, 0.5);
        ops->matrix_set(matrix1, 2, 1, 0.25);
        ops->vec_set(vec2, 0, 1);
        ops->vec_set(vec2, 1, 2);
        ops->vec_set(vec2, 2, 1.5);

        printf("Set column 1 of matrix1 to vec2:\n");
        ops->matrix_set_col(matrix1, 1, vec2);
        ops->matrix_dump(stdout, matrix1);

        ops->matrix_free(matrix1);
        ops->vec_free(vec1);
        ops->vec_free(vec2);
    } else {
        printf(". . . unimplemented\n");
    }

    if (ops->matrix_alloc != NULL
            && ops->matrix_set != NULL
            && ops->matrix_randomize != NULL
            && ops->matrix_matrix_mult != NULL
            && ops->matrix_dump != NULL
            && ops->matrix_free != NULL) {
        puts("alloc mat");
        matrix1 = ops->matrix_alloc(3, 4);
        puts("alloc mat");
        matrix2 = ops->matrix_alloc(4, 5);
        puts("alloc mat");
        matrix3 = ops->matrix_alloc(3, 5);

        puts("set mat1");
        ops->matrix_set(matrix1, 0, 0, 0.5);
        puts("set mat2");
        ops->matrix_set(matrix1, 1, 1, 2);
        puts("randomize mat2");
        ops->matrix_randomize(matrix2, -100, 100);
        puts("matrix mult");
        ops->matrix_matrix_mult(matrix3, matrix1, matrix2);
    
        printf("matrix1:\n");
        ops->matrix_dump(stdout, matrix1);
        printf("matrix2:\n");
        ops->matrix_dump(stdout, matrix2);
        printf("matrix1 X matrix2:\n");
        ops->matrix_dump(stdout, matrix3);

        ops->matrix_free(matrix1);
        ops->matrix_free(matrix2);
        ops->matrix_free(matrix3);
        
    } else {
        printf(". . . unimplemented\n");
    }

    printf("=================== Matrix inversion ===================\n");

    if (ops->matrix_alloc != NULL
            && ops->matrix_set != NULL
            && ops->matrix_inverse != NULL
            && ops->matrix_matrix_mult != NULL
            && ops->matrix_dump != NULL) {
    

        matrix1 = ops->matrix_alloc(3, 3);
        matrix2 = ops->matrix_alloc(3, 3);
        matrix3 = ops->matrix_alloc(3, 3);
        
        ops->matrix_set(matrix1, 0, 0, 0.0);
        ops->matrix_set(matrix1, 1, 0, -1.0);
        ops->matrix_set(matrix1, 2, 0, 0.0);
        ops->matrix_set(matrix1, 0, 1, 1.0);
        ops->matrix_set(matrix1, 1, 1, 4.0);
        ops->matrix_set(matrix1, 2, 1, 1.0);
        ops->matrix_set(matrix1, 0, 2, 0.0);
        ops->matrix_set(matrix1, 1, 2, 0.0);
        ops->matrix_set(matrix1, 2, 2, 1.0);

        ops->matrix_inverse(matrix2, matrix1);
        ops->matrix_matrix_mult(matrix3, matrix2, matrix1);

        printf("Original matrix:\n");
        ops->matrix_dump(stdout, matrix1);
        printf("Computed inverse:\n");
        ops->matrix_dump(stdout, matrix2);
        printf("Test by multiplication:\n");
        ops->matrix_dump(stdout, matrix3);

        ops->matrix_set(matrix1, 0, 1, 2.0);
        ops->matrix_set(matrix1, 1, 1, 0.0);
        ops->matrix_set(matrix1, 2, 1, 0.0);
        ops->matrix_set(matrix1, 0, 0, 5.0);
        ops->matrix_set(matrix1, 1, 0, 3.0);
        ops->matrix_set(matrix1, 2, 0, 6.0);
        ops->matrix_set(matrix1, 0, 2, 1.0);
        ops->matrix_set(matrix1, 1, 2, 5.0);
        ops->matrix_set(matrix1, 2, 2, 7.0);

        ops->matrix_inverse(matrix2, matrix1);
        ops->matrix_matrix_mult(matrix3, matrix2, matrix1);

        printf("Original matrix:\n");
        ops->matrix_dump(stdout, matrix1);
        printf("Computed inverse:\n");
        ops->matrix_dump(stdout, matrix2);
        printf("Test by multiplication:\n");
        ops->matrix_dump(stdout, matrix3);

        ops->matrix_set(matrix1, 0, 0, 2.0);
        ops->matrix_set(matrix1, 1, 0, 1.0);
        ops->matrix_set(matrix1, 2, 0, 1.0);
        ops->matrix_set(matrix1, 0, 1, 5.0);
        ops->matrix_set(matrix1, 1, 1, 0.0);
        ops->matrix_set(matrix1, 2, 1, 0.0);
        ops->matrix_set(matrix1, 0, 2, 4.0);
        ops->matrix_set(matrix1, 1, 2, 5.0);
        ops->matrix_set(matrix1, 2, 2, 2.0);

        ops->matrix_inverse(matrix2, matrix1);
        ops->matrix_matrix_mult(matrix3, matrix2, matrix1);

        printf("Original matrix:\n");
        ops->matrix_dump(stdout, matrix1);
        printf("Computed inverse:\n");
        ops->matrix_dump(stdout, matrix2);
        printf("Test by multiplication:\n");
        ops->matrix_dump(stdout, matrix3);

    } else {
        printf(". . . unimplemented\n");
    }


    printf("=============== Extract vector operations ===================\n");
    
    if (ops->matrix_alloc != NULL
            && ops->vec_alloc != NULL
            && ops->matrix_set != NULL
            && ops->matrix_dump != NULL
            && ops->vec_set != NULL
            && ops->matrix_extract_vec != NULL
            && ops->matrix_free != NULL
            && ops->vec_free != NULL) {
        matrix1 = ops->matrix_alloc(3, 10);
        matrix2 = ops->matrix_alloc(3, 3);
        vec1 = ops->vec_alloc(3);

        for (i = 0; i < 3; i++) {
            for (j = 0; j < 10; j++) {
                ops->matrix_set(matrix1, i, j, (double)rand() / (double)RAND_MAX);
            }
        }
        printf("matrix1:\n");
        ops->matrix_dump(stdout, matrix1);
        
        ops->vec_set(vec1, 0, 5);
        ops->vec_set(vec1, 1, 1);
        ops->vec_set(vec1, 2, 8);
        ops->matrix_extract_vec(matrix2, matrix1, vec1);
        printf("Extracted columns 5, 1, 8:\n");
        ops->matrix_dump(stdout, matrix2);

        ops->matrix_free(matrix1);
        ops->matrix_free(matrix2);
        ops->vec_free(vec1);
    } else {
        printf(". . . unimplemented\n");
    }

    if (ops->vec_alloc != NULL
            && ops->vec_dump != NULL
            && ops->vec_set != NULL
            && ops->vec_extract_vec != NULL) {

        vec1 = ops->vec_alloc(10);
        for (i = 0; i < 10; i++) {
            ops->vec_set(vec1, i, (double)rand() / (double)RAND_MAX);
        }
        printf("vec1:\n");
        ops->vec_dump(stdout, vec1);

        vec2 = ops->vec_alloc(4);
        vec3 = ops->vec_alloc(4);

        ops->vec_set(vec2, 0, 1);
        ops->vec_set(vec2, 1, 2);
        ops->vec_set(vec2, 2, 7);
        ops->vec_set(vec2, 3, 4);

        ops->vec_extract_vec(vec3, vec1, vec2);
        printf("Extracted elements 1, 2, 7, 4:\n");
        ops->vec_dump(stdout, vec3);
    } else {
        printf(". . . unimplemented\n");
    }

    printf("================= Extended operation ======================\n");

    if (ops->matrix_alloc != NULL
            && ops->vec_alloc != NULL
            && ops->matrix_set != NULL
            && ops->vec_set != NULL
            && ops->matrix_colsum_ltz != NULL
            && ops->matrix_dump != NULL
            && ops->vec_dump != NULL
            && ops->matrix_free != NULL
            && ops->vec_free != NULL) {
        matrix1 = ops->matrix_alloc(4, 5);
        vec1 = ops->vec_alloc(4);
        vec2 = ops->vec_alloc(5);

        ops->matrix_set(matrix1, 0, 0, 10);
        ops->matrix_set(matrix1, 1, 0, 3);
        ops->matrix_set(matrix1, 3, 1, 4);
        ops->matrix_set(matrix1, 0, 1, 3);
        ops->matrix_set(matrix1, 2, 2, 1.5);
        ops->matrix_set(matrix1, 3, 3, 20);
        ops->matrix_set(matrix1, 0, 4, 1);
        ops->matrix_set(matrix1, 1, 4, 2);
        ops->matrix_set(matrix1, 2, 4, 0.5);

        ops->vec_set(vec1, 0, 10);
        ops->vec_set(vec1, 1, -10);
        ops->vec_set(vec1, 2, -10);
        ops->vec_set(vec1, 3, 10);

        ops->matrix_colsum_ltz(vec2, matrix1, vec1);

        printf("matrix1:\n");
        ops->matrix_dump(stdout, matrix1);
        printf("vec1:\n");
        ops->vec_dump(stdout, vec1);
        printf("colsum ltz returns:\n");
        ops->vec_dump(stdout, vec2);
        
        ops->matrix_free(matrix1);
        ops->vec_free(vec1);
        ops->vec_free(vec2);
    

    } else {
        printf(". . . unimplemented\n");
    }

    if (ops->matrix_alloc != NULL
            && ops->vec_alloc != NULL
            && ops->matrix_set != NULL
            && ops->vec_set != NULL
            && ops->matrix_colsum_ltz != NULL
            && ops->matrix_dump != NULL
            && ops->vec_dump != NULL
            && ops->matrix_free != NULL
            && ops->vec_free != NULL
            && ops->vec_min != NULL) {

        /*********************************************/

        vec1 = ops->vec_alloc(10);
        ops->vec_set(vec1, 0, (double)rand() / (double)RAND_MAX);
        ops->vec_set(vec1, 1, (double)rand() / (double)RAND_MAX);
        ops->vec_set(vec1, 2, (double)rand() / (double)RAND_MAX);
        ops->vec_set(vec1, 3, (double)rand() / (double)RAND_MAX);
        ops->vec_set(vec1, 4, (double)rand() / (double)RAND_MAX);
        ops->vec_set(vec1, 5, (double)rand() / (double)RAND_MAX);
        ops->vec_set(vec1, 6, (double)rand() / (double)RAND_MAX);
        ops->vec_set(vec1, 7, (double)rand() / (double)RAND_MAX);
        ops->vec_set(vec1, 8, (double)rand() / (double)RAND_MAX);
        ops->vec_set(vec1, 9, (double)rand() / (double)RAND_MAX);
        ops->vec_min(&temp_d, &temp_i, vec1);

        printf("vec1:\n");
        ops->vec_dump(stdout, vec1);
        printf("Min element is %g with index %d\n", temp_d, temp_i);

        ops->vec_free(vec1);
    } else {
        printf(". . . unimplemented\n");
    }
}
