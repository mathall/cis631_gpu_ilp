*
* A Test case with an equality
*
NAME TEST3
OBJSENSE MAX
ROWS
 N  OBJ
 L  L1
 L  L2
 E  E1
COLUMNS
    X1      OBJ     2     E1     1
    X1      L1      1     L2     0
    Y1      OBJ     1     E1     1
    Y1      L1      0     L2     1
RHS
    RHS1    E1      5     L1     2
    RHS1    L2      4
ENDATA
