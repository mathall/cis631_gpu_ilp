*
* A Test case with an equality and a useless constraint.
* L2 does not constrain the objective
*
NAME TEST3
OBJSENSE MAX
ROWS
 N  OBJ
 E  E1
 L  L1
 L  L2
COLUMNS
    X1      OBJ     2     E1     1
    X1      L1      1     L2     0
    Y1      OBJ     1     E1     1
    Y1      L1      0     L2     1
RHS
    RHS1    E1      5     L1     2
    RHS1    L2      10
ENDATA
