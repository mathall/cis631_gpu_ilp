NAME        KAHN1
OBJSENSE MAX
ROWS
 N  COST
 G  C5
 G  C3
 G  C4
 G  C2
 L  C1
COLUMNS
    X2     COST    5     C2    1
    X2     C3      1     C4    2
    X2     C5      4
    X1     COST    3     C1    1
    X1     C3      1     C4    3
    X1     C5      5
RHS
    RHS1   C1      4     C2    6
    RHS1   C3      8     C4    18
    RHS1   C5      32
ENDATA
