NAME TEST5
OBJSENSE MAX
ROWS
 N  OBJ
 L  L1
 L  L2
 L  L3
 L  JUNK
COLUMNS
    X1      OBJ   1     L1     1
    X1      L2    1     L3     0
    Y1      OBJ   2     L1     1
    Y1      L2   -4     L3     1
    JUN     OBJ   0
RHS
    RHS1    L1    5     L2    -3
    RHS1    L3    4
ENDATA
