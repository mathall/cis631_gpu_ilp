* maximize 3*x1 + x2 + 2*x3
* subject to 
* x1 + x2 + 3*x3 <= 30
* 2*x1 + 2*x2 + 5*x3 <= 24
* 4*x1 + x2 + 2*x3 <= 36
*
* solution x1 = 8, x2 = 4, x3 = 0 full slack form: (8,4,0,18,0,0)
* optimal value = 28
*
NAME          CORMEN1
OBJSENSE MAX
ROWS
 N  OBJ
 L  C1
 L  C2
 L  C3
COLUMNS
    X2        OBJ                  1   C1                   6
    X2        C2                   2   C3                   0
    X3        OBJ                  2   C1                   1
    X3        C2                   1   C3                   2
RHS
    RHS1      C1                   4   C2                  2
    RHS1      C3                   3
ENDATA
