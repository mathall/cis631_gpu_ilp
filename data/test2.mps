NAME TEST2
OBJSENSE MIN
ROWS
 N  OBJ
 L  L1
 L  L2
 L  L3
COLUMNS
    X1      OBJ        -1  L1       1
    X1      L2          1  L3       0
    Y1      OBJ        -2  L1       1
    Y1      L2          0  L3       1
RHS
    RHS1    L1          3  L2       2
    RHS1    L3          2
ENDATA
