/*
 * CIS 631, Fall 2019
 *
 * Linear algebra function and data type abstractions
 *
 * Authors: Chris Misa, Matt Hall
 *
 * Some assumptions are made across implementations:
 *   Matrix and vector entries are doubles
 *   Matrix and vectors should be handled as void * . . . the programmer must be real careful with types. . .
 *
 * Allocation functions may return NULL if there's not enough memory.
 * All indexes are zero-based.
 */

#ifndef LA_H
#define LA_H

#include <stdio.h>

/*
 * Function to possibly initialize memory buffers given max dimentions
 */
typedef int (*la_ensure_temps_t)(int max_m, int max_n);

/*
 * Functions for dealing with matrices
 */

/* Allocate a new matrix with m rows and n columns. Sets all entries to 0.0 */
typedef void * (*la_matrix_alloc_t)(int m, int n);

/* Initialize the given matrix with values from a normal c-style two-dimensional array */
typedef int (*la_matrix_init_t)(void *matrix, double **array, int m, int n);

/* Free memory held by matrix */
typedef void (*la_matrix_free_t)(void *matrix);

/* Returns the entry at row i, column j */
typedef double (*la_matrix_get_t)(void *matrix, int i, int j);

/* Returns a vector representation of column j owned by the caller, assuming result is an already-allocated vector of the correct length */
typedef int (*la_matrix_get_col_t)(void *result, void *matrix, int j);

/* Sets the value at row i, column j to value */
typedef int (*la_matrix_set_t)(void *matrix, int i, int j, double value);

/* Sets the entries in the column j to the values given in vector */
typedef int (*la_matrix_set_col_t)(void *matrix, int j, void *vector);

/* Computes the inverse matrix and returns as a new matrix */
typedef int (*la_matrix_inverse_t)(void *result, void *matrix);

/* Extract the colums specified by the index vector idx_vec */
typedef int (*la_matrix_extract_vec_t)(void *result, void *matrix, void *idx_vec);

/* dumps a textual representation of the matrix on the given file * */
typedef void (*la_matrix_dump_t)(FILE *fp, void *matrix);

/* Receives a matrix, and assigns random values to all entries. */
typedef int (*la_matrix_randomize)(void *matrix, int min, int max);

/*
 * Functions for dealing with vectors
 */

/* Allocate a new vector with m entries */
typedef void * (*la_vec_alloc_t)(int m);

/* Initialize the given vector from a normal c-style array */
typedef int (*la_vec_init_t)(void *vec, double *array, int m);

/* Free the given vector */
typedef void (*la_vec_free_t)(void *vec);

/* Return the i-th entry of the given vector */
typedef double (*la_vec_get_t)(void *vec, int i);

/* Sets the i-th entry of the given vector to value */
typedef int (*la_vec_set_t)(void *vec, int i, double value);

/* Dumps a textual representation of the vector on the given FILE * */
typedef void (*la_vec_dump_t)(FILE *fp, void *vec);

/* Returns a vector which contains the elements specified by the index vector idx_vec */
typedef int (*la_vec_extract_vec_t)(void *result, void *vec, void *idx_vec);

/* Receives a vector, and assigns random values to all entries. */
typedef int (*la_vec_randomize)(void *vec, int min, int max);



/*
 * Operations
 * NOTE: these functions should all return 0 if the dimensions of their arguments make sense,
 * otherwise they should return non-zero to indicate an error.
 *
 * NOTE: result is assumed to be a matrix or vector allocated with the correct dimensions.
 */

/* Scalar value times a vector */
typedef int (*la_scalar_vec_mult_t)(void *result, double value, void *vec);

/* Element-wise subtraction */
typedef int (*la_vec_vec_subtract_t)(void *result, void *vec1, void *vec2);

/* Dot-product of two vectors */
typedef int (*la_vec_dot_prod_t)(double *result, void *vec1, void *vec2);

/* Matrix-vector multiplication */
typedef int (*la_matrix_vec_mult_t)(void *result, void *matrix, void *vector);

/* Vector-matrix multiplication */
typedef int (*la_vec_matrix_mult_t)(void *result, void *vec, void *matrix);

/* Matrix-matrix multiplication */
typedef int (*la_matrix_matrix_mult_t)(void *result, void *matrix1, void *matrix2);

/*
 * Extra linear algebra operations needed for revised simplex
 */

/* Matrix column sum if less than zero: each entry in result_vec is the sum of the corresponding column in matrix only taking into consideration rows where index_vec is less than zero */
typedef int (*la_matrix_colsum_ltz_t)(void *result_vec, void *matrix, void *index_vec);

/* Returns both value and index of the minimum element of the given vector */
typedef void (*la_vec_min_t)(double *min_val, int *min_idx, void *vector);

/* Minimum ratio test for phase 1: return min {beta_i / alphaq_i such that (beta_i < 0 and alphaq_i < 0) or (beta_i >= 0 && alphaq_i > 0)} */
typedef int (*la_phase1_min_ratio_t)(double *theta, int *p, void *beta, void *alphaq);

/* Minimum ratio test for phase 2: return min {beta_i / alphaq_i such that alphaq_i > 0} */
typedef int (*la_phase2_min_ratio_t)(double *theta, int *p, void *beta, void *alphaq);

/*
 * Encapsulation of functions for handing to algorithms
 *
 * Performance sensitive operations are noted with P in the margin
 */
struct la_ops {

    la_ensure_temps_t ensure_temps;

    la_matrix_alloc_t matrix_alloc;
    la_matrix_init_t matrix_init;
    la_matrix_free_t matrix_free;
    la_matrix_get_t matrix_get;
    la_matrix_get_col_t matrix_get_col;          /* P */
    la_matrix_set_t matrix_set;
    la_matrix_set_col_t matrix_set_col;          /* P */
    la_matrix_inverse_t matrix_inverse;          /* P */
    la_matrix_extract_vec_t matrix_extract_vec;  /* P */
    la_matrix_dump_t matrix_dump;
    la_matrix_randomize matrix_randomize; 

    la_vec_alloc_t vec_alloc;
    la_vec_init_t vec_init;
    la_vec_free_t vec_free;
    la_vec_get_t vec_get;                        /* P */
    la_vec_set_t vec_set;                        /* P */
    la_vec_dump_t vec_dump;
    la_vec_extract_vec_t vec_extract_vec;        /* P */
    la_vec_randomize vec_randomize; 

    la_scalar_vec_mult_t scalar_vec_mult;        /* P */
    la_vec_vec_subtract_t vec_vec_subtract;      /* P */
    la_vec_dot_prod_t vec_dot_prod;              /* P */
    la_matrix_vec_mult_t matrix_vec_mult;        /* P */
    la_vec_matrix_mult_t vec_matrix_mult;        /* P */
    la_matrix_matrix_mult_t matrix_matrix_mult;  /* P */

    la_matrix_colsum_ltz_t matrix_colsum_ltz;    /* P */
    la_vec_min_t vec_min;                        /* P */
    la_phase1_min_ratio_t phase1_min_ratio;      /* P */
    la_phase2_min_ratio_t phase2_min_ratio;      /* P */
};

#endif
