/*
 * CIS 631, Fall 2019
 *
 * Functions for managing matrix and vector data structures
 *
 * Authors: Matt Hall, Chris Misa
 */

#ifndef DATA_H
#define DATA_H

/*
 * Allocate single indirection matrix structure with M rows and N columns
 */
double ** mallocMatrix(int N, int M);

/*
 * Free matrix with N columns
 */
void freeMatrix(double **A, int N);

/*
 * Print matrix
 */
void printMatrix(double **A, int rows, int cols);

/*
 * Allocate array with N elements
 */
double * mallocArray(int N);

/*
 * print array of n elements
 */
void printArray(double *array, int n);


/*
 * Allocate single indirection matrix structure with M rows and N columns
 * Populates matrix with random numbers uniformly distributed within a range
 */
double ** mallocRandMatrix(int N, int M, int min, int max);

/*
 * Allocate array with N elements
 * Populates matrix with random numbers uniformly distributed within a range
 */
double * mallocRandArray(int N, int min, int max);

#endif
