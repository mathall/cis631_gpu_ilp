/*
 * CIS 631, Fall 2019
 *
 * Generic sanity tests of linear algebra (and index set) functions
 *
 * Skips over sets of operations if the functions pointers are set to NULL for incremental testing.
 *
 * Authors: Chris Misa
 */

#ifndef LA_TEST_H
#define LA_TEST_H

#include <la.h>

void la_test(struct la_ops *ops);

#endif
