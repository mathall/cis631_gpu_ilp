/*
 * CIS 631, Fall 2019
 *
 * Simple, serial linear algebra operations using C-style storage
 *
 * Authors: Chris Misa, Matt Hall
 */

#ifndef LA_OMP_H
#define LA_OMP_H

#include <la.h>

typedef struct laomp_matrix_s {
    double *data;
    int m;
    int n;
} laomp_matrix_t;

typedef struct laomp_vec_s {
    double *data;
    int m;
} laomp_vec_t;

/* Populates the pointers in the ops struct to use these matrix and vector functions */
void laomp_set_ops(struct la_ops *ops);

#endif
