/*
 * CIS 631, Fall 2019
 *
 * Implementation of set-management functions on bit maps.
 *
 * Authors: Chris Misa
 *
 */

#ifndef BM_SET_H
#define BM_SET_H

#include <la.h>

typedef struct bm_set_t {
    unsigned char *data;
    int size;
    int num_elems;
}bm_set_t;

/* Populates the required pointers in the ops struct to use these set functions */
void bm_set_set_ops(struct la_ops *ops);

/* Allocate a set to keep track of m distinct indices */
void * bm_set_alloc(int m);

/* Free a set */
void bm_set_free(void *set);

/* Adds the i-th index to the set */
void bm_set_add(void *set, int i);

/* Removes the i-th index from the set */
void bm_set_remove(void *set, int i);

/* Returns non-zero if the i-th elemet is in the set, 0 otherwise */
int bm_set_test(void *set, int i);

/* Returns the index of the i-th element in the set */
int bm_set_get(void *set, int i);

/* returns the inverse of the given set, assuming result is an already-allocated set of the same length */
void bm_set_inverse(void *result, void *set);

/* Dumps a textual representation of the set on the given FILE * */
void bm_set_dump(FILE *fp, void *set);

#endif
