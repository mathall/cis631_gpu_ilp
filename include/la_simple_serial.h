/*
 * CIS 631, Fall 2019
 *
 * Simple, serial linear algebra operations using C-style storage
 *
 * Authors: Chris Misa
 */

#ifndef LA_SIMPLE_SERIAL_H
#define LA_SIMPLE_SERIAL_H

#include <la.h>

typedef struct lass_matrix_s {
    double *data;
    int m;
    int n;
} lass_matrix_t;

typedef struct lass_vec_s {
    double *data;
    int m;
} lass_vec_t;

/* Populates the pointers in the ops struct to use these matrix and vector functions */
void lass_set_ops(struct la_ops *ops);

#endif
