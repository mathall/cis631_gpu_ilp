/*
 * CIS 631, Fall 2019
 *
 * Functions to read MPS formatted files
 *
 * Authors: Chris Misa
 */

#include <common.h>

#ifndef READ_MPS_H
#define READ_MPS_H

typedef enum {
    READ_MPS_ROW_OP_OBJ,
    READ_MPS_ROW_OP_LESS,
    READ_MPS_ROW_OP_GREATER,
    READ_MPS_ROW_OP_EQUAL
} read_mps_row_op_t;

typedef struct read_mps_row_s read_mps_row_t;
struct read_mps_row_s {
    char *name;
    read_mps_row_op_t op;
    double value;
    int index;
    read_mps_row_t *next;
};


typedef struct read_mps_col_s read_mps_col_t;
struct read_mps_col_s {
    char *name;
    read_mps_row_t *entries;
    int index;
    read_mps_col_t *next;
};

/*
 * Reads an MPS file at the given file_path.
 * Returns by reference:
 *   A : The constraint matrix
 *   c : The right-hand-side of the constraints
 *   b : Coefficients for the objective function
 *
 * Supports the OBJSENSE extension to MPS to determine the problem type (default to MIN)
 *
 * If the problem type is MAX, all equalities are normalized to less than,
 * if the problem type is MIN, all equalities are normalized to greater than.
 *
 * Since the BOUNDS and RANGES sections are not supported, we assume all variables
 * are non-negative as suggested here: http://lpsolve.sourceforge.net/5.0/mps-format.htm
 *
 * The actual return value is 0 on success, otherwise a number indicating the type of error.
 */
int
read_mps(const char *file_path, double ***A, double **b, double **c, int *m, int *n, enum problem_type *prob_type);

#endif
