/*
 * CIS 631, Fall 2019
 *
 * Revised Simplex Implementation
 *
 * Authors: Matt Hall, Chris Misa
 *
 * based on slides from: 
 * https://web.archive.org/web/20191126064616/https://www.cs.upc.edu/~erodri/webpage/cps/theory/lp/revised/slides.pdf
 *
 * The phase 1 algorithm is from Kahn et al., "New artificial-free phase 1 simplex method," in IJENS 2013.
 * 
 */

#ifndef REVISED_SIMPLEX_H
#define REVISED_SIMPLEX_H

#include <la.h>

/*
 * Execute the revised simplex method under the following assumptions:
 *
 * 1) The problem is expressed as
 *
 *   minimize z = cx
 *   subject to  Ax >= b, x >= 0
 *      where c, x are n-element vectors, A is an m by n matrix, and b is an m-element vector
 *
 * 2) The problem is non-degenerate
 *
 * Returns 0 if an optimal solution was found, 
 *         1 if the problem is unbounded, 
 *         < 0 if any other error condition occured. 
 */
int
Revised_Simplex(struct la_ops *ops,     /* Structure with function pointers to use for linear algebra operations */
                double **A_in,          /* Row-major 2-D C array containing constraint matrix A */
                double *b_in,           /* C array containing right hand side of constraints b */
                double *c_in,           /* C array containing coefficients of objective function c */
                int m,                  /* Number of constraints */
                int n,                  /* Number of variables */
                double *z_out);         /* Returned optimal value of objectice function */

#endif
