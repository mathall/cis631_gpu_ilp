/*
 * CIS 631, Fall 2019
 *
 * Naive CUDA implementation of linear algebra operations and data structures
 *
 * Bounds checking is removed for speed in many functions, assuming the underlying algorithm has already been verified through lass_* or equiv
 *
 * Authors: Chris Misa
 */

#include <la.h>

#ifdef __cplusplus
extern "C"
{
#endif

/* Populates the pointers in the ops struct to use these matrix and vector functions */
void lacb_set_ops(struct la_ops *ops);

#ifdef __cplusplus
} // extern "C"
#endif
