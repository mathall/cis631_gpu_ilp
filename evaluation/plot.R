#
# Generate simple box plot from stql_send_test module output
#
library(RColorBrewer)

#
# Argument should be directory holding raw data
#
args <- commandArgs(trailingOnly=T)
if (length(args) != 1) {
  stop("Need path to data")
}

options(digits=22)

headRoom <- 1.1

#
# Color pallete
#
mColors <- function(n) { brewer.pal(n, "Paired") }

#
# Work around to draw intervals around
# points in graph
#
drawArrows <- function(xs, ys, sds, color) {
  arrows(xs, ys,
         xs, ys + sds,
         length=0.01, angle=90, code=3, col=color)
}

data <- read.table(args[[1]], header=T)

# remove time=0 rows
data <- data[data$time != 0,]

means <- aggregate(.~problem+backend, data=data, FUN=mean)
sds <- aggregate(.~problem+backend, data=data, FUN=sd)


dput(sds, file="summary.data")

#
# MEANS BARPLOT
#

bpd <- matrix(nrow=length(levels(means$backend)))
bpd_sd <- matrix(nrow=length(levels(sds$backend)))
for (l in levels(means$problem)) {

    bpd <- cbind(bpd, means[means$problem == l,]$time)
    bpd_sd <- cbind(bpd_sd, sds[sds$problem == l,]$time)
    
}

# get rid of extra first col
bpd <- bpd[,-1]
bpd_sd <- bpd_sd[,-1]

rownames(bpd) <- levels(means$backend)
colnames(bpd) <- levels(means$problem)

# swap serial to first row
bpd <- bpd[c(3,2,1),]
bpd_sd <- bpd_sd[c(3,2,1),]

ybnds <- c(0, headRoom * max(bpd[1,] + bpd_sd[1,]))

cairo_pdf(file="lp_eval_mean.pdf")
par(cex=1.0)
barCenters <- barplot(bpd,
                      ylab=expression(paste("Mean Execution Time (s)", sep="")),
                      ylim=ybnds,
                      col=mColors(3),
                      beside=T,
                      las=2)

drawArrows(barCenters[seq(1,length(barCenters), 3)], bpd[1,], bpd_sd[1,], "black")
drawArrows(barCenters[seq(2,length(barCenters), 3)], bpd[2,], bpd_sd[2,], "black")
drawArrows(barCenters[seq(3,length(barCenters), 3)], bpd[3,], bpd_sd[3,], "black")

legend("topleft", 
       horiz=F,
       legend=rownames(bpd),
       fill=mColors(3))
dev.off()

speedups <- bpd
speedups[2,] <- speedups[1,] / speedups[2,]
speedups[3,] <- speedups[1,] / speedups[3,]
speedups <- speedups[-1,]

speedups_sds <- bpd_sd[-1,]

ybnds <- c(0, headRoom * max(speedups[2,] + speedups_sds[2,]))

cairo_pdf(file="lp_eval_mean_speedup.pdf")
par(cex=1.0)
barCenters <- barplot(speedups,
                      ylab=expression(paste("Mean Speedup", sep="")),
                      ylim=ybnds,
                      beside=T,
                      col=mColors(3)[c(2,3)],
                      las=2)

drawArrows(barCenters[seq(1,length(barCenters), 2)], speedups[1,], speedups_sds[1,], "black")
drawArrows(barCenters[seq(2,length(barCenters), 2)], speedups[2,], speedups_sds[2,], "black")

legend("topleft", 
       horiz=F,
       fill=mColors(3)[c(2,3)],
       legend=rownames(speedups))
dev.off()
