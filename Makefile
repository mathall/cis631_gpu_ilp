#
# CIS 631, Fall 2019
#
# Main make script
#
# Use `export NO_CUDA=1` if you don't want to build CUDA versions
#
# Authors: Chris Misa
#

export BUILDDIR=$(shell pwd)/build
export HEADERSDIR=$(shell pwd)/include

export CC = gcc
export CXX = g++
export CFLAGS = -g -I$(HEADERSDIR)
export LIBS = -lm 
export OMPLIBS = -lgomp
export OMPFLAGS += -fopenmp #-D_POSIX_C_SOURCE=200112L
#export CUDA_INSTALL_PATH = /opt/cuda-10.0
export CUDA_INSTALL_PATH = /packages/cuda/9.2
#export CUDA_INSTALL_PATH = /packages/cuda/10.1
export NVCC = $(CUDA_INSTALL_PATH)/bin/nvcc
export NVCCFLAGS = -I$(CUDA_INSTALL_PATH)/include -gencode arch=compute_37,code=sm_37 --default-stream per-thread
#export NVCCFLAGS = -I$(CUDA_INSTALL_PATH)/include --default-stream per-thread
export NVCCLIBS = -L$(CUDA_INSTALL_PATH)/lib64 -lcudart -lcublas -lcusolver

# Add sub-directories for recursive make here
SUBDIRS = src

.PHONY: subdirs $(SUBDIRS)
subdirs: $(SUBDIRS)

$(SUBDIRS): $(BUILDDIR)
	$(MAKE) -C $@

$(BUILDDIR):
	mkdir -p $(BUILDDIR)

.PHONY: clean
clean:
	for dir in $(SUBDIRS); do make -C $$dir clean; done
	rm -rf $(BUILDDIR)
